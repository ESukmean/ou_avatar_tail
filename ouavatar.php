<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	error_reporting(0);
	date_default_timezone_set("Asia/Seoul");
	/*
	if(is_numeric($_GET['mn']) == false){
		echo '회원번호 없음';
		exit;
	}*/
	// php 설치 후 설치 항목 메모
	// 설치하지 않으면 작동하지 않습니다.
	// 빠진 항목 있다면 추가 바랍니다.
	// php5-gd 			이미지 표시에 필요
	// php5-xcache		xcache 서버 캐시에 필요
	// php5-curl		웹 페이지 가져오기에 필요


	// 오유 도메인인지 확인. 오유 도메인일 때에만 xcache 작동시킴.
	if (preg_match("/todayhumor\.co\.kr/i", $_SERVER["HTTP_REFERER"])){
		$is_todayhumor_domain = true;
	} else {
		$is_todayhumor_domain = false;
	}
	

	// 자료창고인지 확인 
	if (preg_match("/table\=(databox|databoxold|singo)\&/i", $_SERVER["HTTP_REFERER"])
		|| 	preg_match("/table\=bestofbest\&no\=155685/i", $_SERVER["HTTP_REFERER"])
		|| 	preg_match("/board\/list\.php/i", $_SERVER["HTTP_REFERER"])
		
		){
		$is_testboard = true;
	} else {
		$is_testboard = false;
	}
	
		

	header('Content-Type: image/png');
	header('Cache-Control: max-age=600');   


	// 이미지의 가로와 세로
	$im_width = 806;
	$im_height = 200;

	// 아이콘(SCV님이 API에 아이콘 넣어주면 지움)
	$nick_icon = $_GET['nick_icon'];

	// 중복함수를 인클루드시킨다.
	$file_server_path = realpath(__FILE__);
	$server_path = str_replace(basename(__FILE__), "", $file_server_path);

	include_once "lib/php/ouavatar_lang.php";		// 다국어 지원
	include_once "lib/php/ouavatar_if.php";		// if 변수 모아둔것
	include_once "lib/php/ouavatar_fwrite.php";	// fwrite 변수 모아둔것 
	include_once "lib/php/ouavatar_rtltoltr.php";	// RTL 지원 
	include_once "lib/php/ouavatar_mysql.php";	// RTL 지원 
	$query = '';
	
	$avatar_mysqli = new ouavatar_mysqli();
	
	// 한 쪽에만 인클루드 시켜야 할 경우 여기로 빼둔다.
	if((isset($_GET['oumark_gloss_opacity']) == false || $_GET['oumark_gloss_opacity'] == "" || isset($_GET['isold']) == false) || (isset($_GET['oumark_gloss_opacity']) && $_GET['oumark_gloss_opacity'] == 0))
		$_GET['oumark_gloss_opacity'] = 1;

	function curl_page($str){
		if(xcache_isset('ou|page|'.$str)){
			return xcache_get('ou|page|'.$str);
		}
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $str);
		curl_setopt($ch, CURLOPT_USERAGENT,'OU_Tail_Maker (ActiveXXX, irobota, humorbest_862108)');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		xcache_set('ou|page|'.$str, str_replace(array("\t","\n","\r"),'',curl_exec($ch)), 600);
		curl_close($ch);
		return xcache_get('ou|page|'.$str);
	}
	 
	$nickname = $text_nickname_null;
	$joindate = $text_joindate_null;
	$times = 0;
	$replytimes = 0;
	$bobtimes = 0;
	$besttimes = 0;
	$totaltimes = 0;
	$level = 0;

	$_GET['about1'] = str_replace("&apos;", "'", $_GET['about1']);
	$_GET['about1'] = str_replace("&quot;", '"', $_GET['about1']);

	$_GET['about2'] = str_replace("&apos;", "'", $_GET['about2']);
	$_GET['about2'] = str_replace("&quot;", '"', $_GET['about2']);
	
	

	include_once "lib/php/ouavatar_user_agent_write.php";	// SQL
	
	
	// xcache가 설치되지 않으면 오류를 일으킨다.
	if(xcache_isset('ou|minfo|'.$_GET['mn'])){
		
		$array = xcache_get('ou|minfo|'.$_GET['mn']);
		$nickname = $array[0];
		$joindate = $array[1];
		$times = $array[2];
		$level = $array[7];
		$totaltimes = $array[6];
		xcache_inc("ouavatar_total_view",1,0);
		xcache_inc("ouavatar_total_view_".date("Y-m-d"),1,0);
		// 아바타 메뉴 미리보기용 랜덤 캐시 
		if (intval($times) > 0 && $times != "" && xcache_isset('ouavatar_view_'.$_GET['mn']) 
		&& $is_todayhumor_domain == true && $is_testboard == false){
			$whole_getval = "";
			foreach($_GET as $key => $value)
			{
				 $whole_getval .= $key."=".urlencode($value)."&";
			}
			$preview_array = array($whole_getval,$_GET['mn'],$nickname,$_GET['about1'],$_GET['about2'],$level,$nick_icon,$_GET['hyperlink'],$_GET['title']);
			
			

			$insert_query .= "
			CREATE TABLE IF NOT EXISTS ouavatar_member_db
						(
						ou_mn			INT NOT NULL PRIMARY KEY, 
						ou_nick			TINYTEXT NOT NULL,
						ou_level		SMALLINT NOT NULL,
						ou_getval		TEXT NOT NULL,
						ou_hits			INT DEFAULT '0' NOT NULL,
						ou_cdate		DATETIME DEFAULT '0000-00-00 00:00:00'
						)CHARACTER SET utf8 COLLATE utf8_general_ci;

			
		
			insert into ouavatar_member_db
						(
						ou_mn,
						ou_nick,
						ou_level,
						ou_getval,
						ou_cdate
						)
						values (
						'".mysql_escape_string($_GET['mn'])."',
						'".mysql_escape_string($nickname)."',
						'".mysql_escape_string($level)."',
						'".mysql_escape_string($whole_getval)."',
						'".mysql_escape_string(date("Y-m-d H:i:s"))."'
						)	
						on duplicate key update 
						ou_mn='".mysql_escape_string($_GET['mn']). "', 
						ou_hits = ou_hits+1,
						ou_nick = '".mysql_escape_string($nickname)."',
						ou_level = '".mysql_escape_string($level)."',
						ou_getval = '".mysql_escape_string($whole_getval)."'
						 
						 ;	
						";

			
			
		}	// if (intval($times) > 0 && $times != "" && xcache_isset('ouavatar_view_'.$_GET['mn']) && $is_todayhumor_domain == true && $is_testboard == false){	오유 내에서 자료창고가 아닌 모든 게시판
		
		// 아바타 메뉴 미리보기용 랜덤 캐시 끝
	}else{
		$avatar_mysqli->refresh();
		$avatar_mysqli->active_users($_GET['mn']);
		
		$ouapi = curl_page('http://www.todayhumor.co.kr/board/list.php?kind=member&mn='.$_GET['mn'], true);
		//echo htmlspecialchars($ouapi);
	 	$tmp;
		preg_match("/<strong>(.*)<\/strong>님의/", $ouapi, $tmp);
		$nickname =  $tmp[1];
		preg_match("/회원가입 : ((\d\d-)(\d\d-)(\d\d))/", $ouapi, $tmp);
		$joindate = '20'.$tmp[1];   // 가입날짜
		preg_match("/방문횟수 : (\d*)/", $ouapi, $tmp);
		$times = $tmp[1]; // 방문횟수
		preg_match_all("/>([0-9]+)<\/a>/", $ouapi, $tmp);
		$totaltimes = $tmp[1][0]; // 전체글 수
		$level = floor((($times*2) + ($totaltimes*5)) / 70);
		 
		$blacklist = array(528773, 536840); // 강등리스트
		
		
		if(empty($_SERVER["HTTP_REFERER"])){
			$_SERVER["HTTP_REFERER"] = '';
		}
		 
		 
		// 오유 도메인 및 자료창고가 아닌 게시판 내에서 실행될 때에만 
		if( $is_todayhumor_domain == true && $is_testboard == false){
			
			$minfo_array = array($nickname, $joindate, $times, $replytimes, $bobtimes, $besttimes, $totaltimes, $level, $_SERVER['HTTP_REFERER'], $nick_icon);
			
			// 임시 xcache
			if(isset($totaltimes) == false || empty($totaltimes) || is_numeric($totaltimes) == false)
				xcache_set('ou|minfo|'.$_GET['mn'], $minfo_array , 10);
			else
				xcache_set('ou|minfo|'.$_GET['mn'], $minfo_array , 600);
			
			// 영구 xcache(임시 xcache 갱신시 같이 갱신)
			fwxcache_set('ou|minfo_inf|'.$_GET['mn'], 	$minfo_array);
		
			
		
			// 갱신 횟수 저장
			/*$insert_query .= "
			CREATE TABLE IF NOT EXISTS ouavatar_refresh
						(
						ou_datestamp	int not null primary key,
						ou_refresh		INT NOT NULL
						);
			insert into ouavatar_refresh
						(ou_datestamp) values ('".$datestamp."')
						on duplicate key update 
						ou_refresh = ou_refresh + 1;";

			*/
		}
	}





	$image = imagecreatetruecolor($im_width, $im_height);
	imagealphablending($image, 0);
	imagesavealpha($image, 1);
	$color = imagecolorallocatehex($image, '#'.mysql_escape_string($_GET['text_color']));
	   
	if ($times == ''){
		$nickname = $text_nickname_null;
		$joindate = $text_joindate_null;
		$times = 0;
		$replytimes = 0;
		$bobtimes = 0;
		$besttimes = 0;
		$totaltimes = 0;
		$level = 0;
	}


	imagefilledrectangle($image, 0, 0, 805, 199, imagecolorallocatehex($image,'#'.mysql_escape_string($_GET['background_color'])));
	imagealphablending($image, 1);
	 
	 
	// 가장 최근에 가입한(번호가 가장 높은) 회원번호를 구한다. 
	if (intval($times) > 0 && $times != "" && intval(fwxcache_get('ouavatar_max_mn')) < $_GET['mn']){
		//ouavatar_fwrite('ouavatar_max_mn');
		xcache_set('ouavatar_max_mn',$_GET['mn'],0);
	}

	if (intval($times) > 0 && $times != ""){
		xcache_inc('ouavatar_view_'.$_GET['mn'],1,0);
	}



	// 총 사용자 수 계산
	// 오유 도메인 내 및 자료창고가 아닐 때 실행시에만 가능.
	if( $is_todayhumor_domain == true && $is_testboard == false){
		

		$ouavatar_uarr_get = array();
		// 너무 길게 하면 안된다.
		// uarr은 user_array의 약자.
		if (xcache_isset('ou|minfo|'.$_GET['mn'])){
			// 활성 사용자 수 
			
			$ouavatar_uarr_get = xcache_get('ouavatar_uarr_'.date("Y-m-d"));
			array_push($ouavatar_uarr_get,$_GET['mn']);
			$ouavatar_uarr_get = array_unique($ouavatar_uarr_get);	// 중복 배열 제거
			xcache_set('ouavatar_uarr_'.date("Y-m-d"),$ouavatar_uarr_get,0);
			
			

			/*
			$insert_query .= "
			CREATE TABLE IF NOT EXISTS ouavatar_active_users
						(
						ou_datestamp	int not null primary key,
						ou_users		INT NOT NULL
						);
			insert into ouavatar_active_users
						(ou_datestamp) values ('".$datestamp."')
						on duplicate key update 
						ou_users = '".count($ouavatar_uarr_get)."';";
			*/
		}
		
		// 조회수
		/*
		$insert_query .= "
		CREATE TABLE IF NOT EXISTS ouavatar_hits
					(
					ou_datestamp	int not null primary key,
					ou_hits		INT NOT NULL
					);
		insert into ouavatar_hits
					(ou_datestamp) values ('".$datestamp."')
					on duplicate key update 
					ou_hits = ou_hits+1;";
					
		*/
		
		
	} //if( $is_todayhumor_domain == true){



	// 폰트 경로 설정
	$directory = $_SERVER['DOCUMENT_ROOT'] . "/ouavatar/";
	$font = $directory."hangangm.ttf";


	// 폰트 설정 끝


	// imagettftext ( resource $image , float $size , float $angle , int $x , int $y , int $color , string $fontfile , string $text )
	// imagettftext(이미지, 폰트크기, 회전, X위치, Y위치, 색상, 폰트, 텍스트)

	// 언어
		if ($_GET['language'] == "zh_cn"){
			$join_text = '会员加入 : '.$joindate.'  访问次数 : '.$times.'次';
			$gen_text = '布告个数:'.$totaltimes;	
		} else if ($_GET['language'] == "zh_tw"){
			$join_text = '會員加入 : '.$joindate.'  訪問次數 : '.$times.'次';
			$gen_text = '布告個數:'.$totaltimes;
		} else if ($_GET['language'] == "ja_jp"){
			$join_text = '会員加入 : '.$joindate.'  訪問回数 : '.$times.'回';
			$gen_text = '全体スレッド:'.$totaltimes;		
		} else if ($_GET['language'] == "en_us"){
			if ($joindate != $text_joindate_null){
				$joindate = date("F j, Y",strtotime($joindate));
			}
			
			$join_text = 'Join : '.$joindate.'  Visit : '.$times.'';
			$gen_text = 'Posts: '.$totaltimes;		
		} else if ($_GET['language'] == "en_uk"){
			if ($joindate != $text_joindate_null){
				$joindate = date("j F Y",strtotime($joindate));
			}
			
			$join_text = 'Join : '.$joindate.'  Visit : '.$times.'';
			$gen_text = '  Posts: '.$totaltimes;		
		} else if ($_GET['language'] == "es_es"){
			if ($joindate != $text_joindate_null){
				$joindate = month_lang(date("j \d\\e F, Y",strtotime($joindate)));
			}
			
			$join_text = 'Registro : '.$joindate.'  Visita : '.$times.'';
			$gen_text = 'Poste:'.$totaltimes;
		} else if ($_GET['language'] == "eo_eo"){
			if ($joindate != $text_joindate_null){
				$joindate = month_lang(date("j \d\\e F, Y",strtotime($joindate)));
			}
			
			$join_text = 'Enskribis : '.$joindate.'  Visitoj : '.$times.'';
			$gen_text = 'Poŝtoj:'.$totaltimes;
		} else if ($_GET['language'] == "ru_ru"){
			if ($joindate != $text_joindate_null){
				$joindate = month_lang(date("d F Y г.",strtotime($joindate)));
			}
			
			$join_text = 'Вступать : '.$joindate.'  Визиты : '.$times.'';
			$gen_text = 'Сообщений:'.$totaltimes;
		} else if ($_GET['language'] == "fr_fr"){
			if ($joindate != $text_joindate_null){
				$joindate = month_lang(date("j F Y",strtotime($joindate)));
			}
			
			$join_text = 'Inscription : '.$joindate.' Visites : '.$times.'';
			$gen_text = 'Entiers:'.$totaltimes;


		} else {
			$join_text = '회원가입 : '.$joindate.'  방문횟수 : '.$times.'회';
			switch (mt_rand(0,5)) {
				case 1:
					$gen_text = '전체글 : '.$totaltimes.'  ※ 숨겨진 게시판, 독도 게시판 [table=dokdo]';		
					break;

				case 2:
				case 3:
					$gen_text = '전체글 : '.$totaltimes.'  ※ [클유사태 1주기] 클유분들 수고 많으셨습니다.';
					break;

				default:
					$gen_text = '전체글 : '.$totaltimes.'  ※공식 API의 문제로 정보가 불안정 및 미비합니다.';
					break;
			}
			//$gen_text = (mt_rand(0, 5) != 1) ? '전체글 : '.$totaltimes.'  ※공식 API의 문제로 정보가 불안정 및 미비합니다.' : (mt_rand(0, 2) == 1) ? '전체글 : '.$totaltimes.'  ※ 숨겨진 게시판, 독도 게시판 [table=dokdo]' : '전체글 : '.$totaltimes.'  ※ 클유사태 1주기] 클유분들 수고 많으셨습니다.';
		}
	// 언어 끝




	$nbsp_md5 = md5("&nbsp;");
	$nickname_text = str_replace($nbsp_md5," ",trim(rtl_ltr(str_replace(" ",$nbsp_md5,$nickname))));
	$about1_text = str_replace($nbsp_md5," ",trim(rtl_ltr(str_replace(" ",$nbsp_md5,$_GET['about1']))));
	$about2_text = str_replace($nbsp_md5," ",trim(rtl_ltr(str_replace(" ",$nbsp_md5,$_GET['about2']))));

	if ($_GET['text_direction']=="rtl"){
		$_GET['avatar_x'] = 806-200;
		$star_x = rtl_text(70,$font,'★',806-210);
		$nickname_x = rtl_text(40,$font,$nickname_text,806-290);
		$lv_text_x = rtl_text(24,$font,"Lv.",806-240);
		$level_x = rtl_text(24,$font,$level,806-220);
		$join_text_x = rtl_text(18,$font,$join_text,806-220);
		$gen_text_x = rtl_text(15,$font,$gen_text,806-220);
		$about1_text_x = rtl_text(14,$font,$about1_text,806-220);
		$about2_text_x = rtl_text(14,$font,$about2_text,806-220);
	} else {
		$_GET['avatar_x'] = 0;
		$star_x = 210;
		$nickname_x = 290;
		$lv_text_x = 210;
		$level_x = rtl_text(14,$font,$level,260);
		$join_text_x = 220;
		$gen_text_x = 220;
		$about1_text_x = 220;
		$about2_text_x = 220;
	}




	// 단색 아이콘(star/ribbon)
	avatar_image($directory.'icon/icon_'.$_GET['nick_icon'].'.png',$image,$_GET['star_color'],"n",100,"icon",$star_x,5);




	// 폰트를 굵게 하기 위해 반복한다.
	for ($i=0; $i<2; $i++){
		// $star = 별표
		//imagettftext($image, 70, 0, $star_x, 68, $star_color, 	$font, '★' );
		
		// $nickname = 회원 닉네임
		imagettftext($image, 40, 0, $nickname_x, 58, $color, $font, $nickname_text);
		 
		// $lv_text = 회원 레벨 Lv
		imagettftext($image, 24, 0, $lv_text_x, 35, $color, $font, 'Lv.');
		 
		// $level = 회원 레벨 숫자 (레벨숫자 오른쪽 정렬을 위해)
		imagettftext($image, 24, 0, $level_x, 65, $color, $font, $level);

		// $join_text = 회원가입
		imagettftext($image, 18, 0, $join_text_x, 100, $color, $font, $join_text);

		// $gen_text = 댓글수, 베오베, 베스트, 전체글 
		imagettftext($image, 15, 0, $gen_text_x, 127, $color, $font, $gen_text); 
		 
		// $about1 = 자기소개 첫번째 줄	
		imagettftext($image, 14, 0, $about1_text_x, 155, imagecolorallocatehex($image,'#'.mysql_escape_string($_GET['about_color'])), $font, $about1_text );
		 
		// $about2 = 자기소개 두번째 줄
		imagettftext($image, 14, 0, $about2_text_x, 180, imagecolorallocatehex($image,'#'.mysql_escape_string($_GET['about_color'])), $font, $about2_text );
	}






	//$about1 = rtl_text(14,$font,$_GET['about1'],780);

	//RTL 정렬 시 필요한 함수
	function rtl_text($size, $font, $text, $x){
		$dimensions = imagettfbbox($size, 0, $font, $text);
		$textWidth = abs($dimensions[4] - $dimensions[0]);
		return $x+(imagesx($im) - $textWidth);
	}




	// 모든 아바타 요소는 200x200 픽셀 투명+검은색 PNG 이어야 한다.
	// 좌측 아바타 레이어 겹치기 기능. 아래로 갈수록 레이어는 위로 올라온다.
	// 그림자, 입, 혀, 치아 모두 따로 PNG로 만들어두어야 한다.
	 
	 //echo $server_path;
	include_once $server_path."ouavatar_layer.php";
	include_once "ouavatar_layer.php";
	 


	// 리퍼러 확인(개발자용)
	if ($_COOKIE['ouavatar_debug'] == "true"){
		$debugfont = $directory."unifont.ttf";
		$debug_fontsize = 11;
		
		// true와 false를 화면에 표시하기 위해서는 이 과정이 필수.
		$boolarray = Array(false => 'false', true => 'true');

		$bboxtext = "\$_SERVER[\"HTTP_REFERER\"] = ".$_SERVER["HTTP_REFERER"]."\n".
		"\$is_todayhumor_domain (오유 도메인) = ".$boolarray[$is_todayhumor_domain]."\n".
		"\$is_testboard (테스트 게시판) = ".$boolarray[$is_testboard]."\n".
		"업데이트 시간 = ".date("Y-m-d H:i:s")."\n".
		"\$_COOKIE['ouavatar_debug'] (디버그 화면 켜기) = ".$_COOKIE["ouavatar_debug"]."\n".

		"디버그 화면을 해제하시려면 오유아바타 메뉴 페이지에서 개발자 옵션을 해제하시고 브라우저 캐시를 삭제하십시오.\n(쿠키가 아닌 캐시만 삭제해 주시기 바랍니다.)";
		
		$bbox1 =	imagettfbbox ( $debug_fontsize , 0 , $font , $bboxtext );
		
		
		$transparent = imagecolorallocatealpha($image, 255,255,255,10);
		imagefilledrectangle($image, $bbox1[6], $bbox1[7], $bbox1[2], $bbox1[3]+$debug_fontsize, $transparent);
		imagettftext($image, $debug_fontsize, 0, 0, $debug_fontsize, imagecolorallocatehex($image,'#000000'), $font, $bboxtext);
		
		if ($is_todayhumor_domain == false){
			$bboxtext = "오유 외 도메인에서 사용되어 통계 및 순위에 집계되지 않았습니다.";
			$debug_color='#FF0000';
		} else {
			if ($is_testboard == true){
				$bboxtext = "테스트 전용 게시판에서 사용되어 통계 및 순위에 집계되지 않았습니다.";
				$debug_color='#FF0000';
			} else {
				$bboxtext = "통계 및 순위에 집계되었습니다.";
				$debug_color='#0000FF';	
			}
		}
		
		// imagefilledrectangle ( $image , int $x1 , int $y1 , int $x2 , int $y2 , int $color )
		$bbox2 =	imagettfbbox ( $debug_fontsize , 0 , $font , $bboxtext );
		imagefilledrectangle($image, $bbox2[6], $bbox1[3]+$debug_fontsize+1, $bbox2[2], $bbox1[3]+($debug_fontsize*3)+1, $transparent);
		imagettftext($image, $debug_fontsize, 0, 0, $bbox1[3]+$debug_fontsize*2+7, imagecolorallocatehex($image,$debug_color), $font, $bboxtext);
	} // 개발자 모드 끝


	// 아바타 레이어를 그리는 함수
	function avatar_image($layer,$image,$color_hex,$isshadow,$opacity,$type,$x_pos,$y_pos,$iscolor){
		$layer_png = imagecreatefrompng($layer); 
		
		// 이 두 줄의 코드가 없으면 윤곽선 현상이 생긴다.
		imagealphablending($layer_png, false);
		imagesavealpha($layer_png, false);
		
		if ($iscolor != true){
			// 이미지 컬러라이징 시작
			imagefilter($layer_png, IMG_FILTER_GRAYSCALE);
			 
			// 그림자 png 레이어를 위한 기능.
			// 그림자가 n이 아니면, RGB에 50을 마이너스한 다음 만약 음수가 된다면 양수로 치환하여 그림자 효과를 구현한다.
			if ($isshadow == 'n')
			{
				$colorize_red = intval(hexdec(substr($color_hex,0,2)));
				$colorize_green = intval(hexdec(substr($color_hex,2,2)));
				$colorize_blue = intval(hexdec(substr($color_hex,4,2)));
			} else {
				$colorize_red = abs(intval(hexdec(substr($color_hex,0,2)))-50);
				$colorize_green = abs(intval(hexdec(substr($color_hex,2,2)))-50);
				$colorize_blue = abs(intval(hexdec(substr($color_hex,4,2)))-50);
			} 
			imagefilter($layer_png, IMG_FILTER_COLORIZE, $colorize_red,$colorize_green,$colorize_blue);
			// 이미지 컬러라이징 끝
		}
		
		if ($opacity == null || $opacity > 100)
			$opacity = 100;
		 
		if ($type != "icon"){
			$x_pos = $_GET['avatar_x'];
			$y_pos = 0;
			$avatar_width = 199;
			$avatar_height = 200;
			$avatar_crop = $_GET['crop'];
			$avatar_rotate = $_GET['rotate'];
		} else {
			$avatar_width = 72;
			$avatar_height = 72;
			$avatar_crop = 0;
			$avatar_rotate = 0;
		}
		
		 
		 
		
		imagecopymerge_alpha(
		$image,         // 원본 이미지 
		imagerotate($layer_png,intval($avatar_rotate),imagecolorallocate($image,192,192,192)),             // 위에 겹칠 이미지
		$x_pos,$y_pos,            // 겹칠 이미지의 위치
		1+intval($avatar_crop),1+intval($avatar_crop),        // 겹칠 이미지 위치
		$avatar_width, $avatar_height,       // 겹칠 이미지 자르기. 겹칠 이미지의 캔버스 크기만큼 권장한다.
		$opacity                // 투명도. 0이 투명, 100이 불투명
		);
	}
	 
	function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
		$src_w -= 3;
		$src_h -= 3;
		$cut = imagecreatetruecolor($src_w, $src_h);
		imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
		imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
		imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
	}
	 
	// 이미지 컬러를 헥스로
	function imagecolorallocatehex($im,$s){
		if ($s[0]=='#') $s=substr($s,1);
		$bg_dec=hexdec($s);
		return imagecolorallocate($im,
					($bg_dec & 0xFF0000) >> 16,
					($bg_dec & 0x00FF00) >>  8,
					($bg_dec & 0x0000FF)
					);
	}

	// 탈퇴회원 또는 정보 없음 
	if ($times == ''){
		imagefilter($image, IMG_FILTER_GRAYSCALE);
	}
	 
	// SQL 쿼리 작동
	if ($insert_query != ""){
		$mysqli->multi_query($insert_query); 
	}

	//최종 이미지 생성
	imagepng($image);
	imagedestroy($image);

	$avatar_mysqli->hit();
	$avatar_mysqli->query();
	?>
