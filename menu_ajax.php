<?php
	if(isset($_GET['type']) == false) $_GET['type'] = '';
	include_once('lib/php/ouavatar_max_avatar_num.php');

	function get_to_input(){
		foreach($_GET as $key => $val){
			if($key == 'type') continue;

			?><input type="hidden" name="<?php echo htmlspecialchars($key); ?>" value="<?php echo htmlspecialchars($val); ?>"><?php
		}
	}
	function avatar_preview($name,$num,$oumark_light)
	{
		$result = "<div class='ouavatar_preview panel panel-info' id='".$name."_preview'><div class='panel-heading'><b>미리보기</b> (미리보기 이미지 클릭시 바로 적용됩니다.)</div>";
		
		if ($oumark_light == true){
			for ($i=0 ; $i<=100 ; $i=$i+25)
			{
				// OU마크 발광 선택 메뉴 전용
				$result .= "<span style='display:inline-block;'>".$i." <img class='ouavatar_preview_img' alt='".$i."' src='lib/php/ouavatar_preview.php?body_num=1&body_color=1E90FF&oumark_color=FFFFFF&oumark_gloss_opacity=".$i."&end=true&isold&.png' onclick='$(\"#oumark_gloss_opacity\").val(\"".$i."\")'></span>";
			}
		} else {
			for ($i=1;$i<=$num;$i++)
			{
				$result .= "<span style='display:inline-block;'>".$i." <img class='ouavatar_preview_img' alt='".$i."' src='lib/php/ouavatar_preview.php?".$name."_num=".$i."' onclick='$(\"#".$name."_num\").val(\"".$i."\")'></span>";
			}
		}
		$result .= "</div>";
		echo $result;
	}

	function step_progress($percent){ 
		if(isset($_COOKIE['setting'])) $_COOKIE['setting'] = ''; ?>
		<script>var setting = "<?php echo addslashes($_COOKIE['setting']); ?>"</script>
		<ol class="breadcrumb" style="margin-bottom:0">
			<li<?php if($percent == 25) echo ' class="active"'; ?>><a href="?type=step_1">회원정보 및 자기소개. 그리고 색상 지정</a></li>
			<li<?php if($percent == 50) echo ' class="active"'; ?>><a href="?type=step_2">아바타 모습 설정</a></li>
			<li<?php if($percent == 75) echo ' class="active"'; ?>><a href="?type=step_3">기타 잡다한 설정</a></li>
			<li<?php if($percent == 100) echo ' class="active"'; ?>><a href="?type=end">끝 ~</a></li>
		</ol>
		<div class="progress">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent; ?>%">
				<span class="sr-only"><?php echo $percent; ?>% 진행중..</span>
			</div>
		</div><?php
	}

	switch ($_GET['type']) {
		case '':
			?><div class="alert alert-info">
				이 아바타 꼬릿말은 <b>오픈소스</b>입니다. 누구나 <b>개발에 참여</b>하실 수 있습니다. (<a href="https://www.dropbox.com/s/xith7uviwxuvue1/ouavatar.zip?dl=0" target="_blank" class="alert-link">소스 다운로드</a>)<br />현제 버전은 E_Sukmean 포크 버전입니다 (<a href="https://bitbucket.org/ESukmean/ou_avatar_tail" target="_blank" class="alert-link">소스 다운로드</a>)
			</div>
			<div class="alert alert-info">꼬릿말 서비스에 대한 설명은 <a href="//www.todayhumor.co.kr/board/view.php?table=humorbest&no=862108" target="_blank" class="alert-link">오유 게시글 humorbest_862108</a>에서 볼 수 있습니다.</div>
			<div class="row">
				<a href="?type=make_intro" class="col-sm-6"><button class="btn btn-primary">꼬릿말 만들기</button></a>
				<a href="?type=summary" class="col-sm-6"><button class="btn btn-info">통계정보 보기</button></a>
			</div><?php
		break;
		case 'make_intro': ?>
			<div class="alert alert-info">
				이 아바타 꼬릿말은 <b>오픈소스</b>입니다. 누구나 <b>개발에 참여</b>하실 수 있습니다. (<a href="https://www.dropbox.com/s/xith7uviwxuvue1/ouavatar.zip?dl=0" target="_blank" class="alert-link">소스 다운로드</a>)<br />현제 버전은 E_Sukmean 포크 버전입니다 (<a href="https://bitbucket.org/ESukmean/ou_avatar_tail" target="_blank" class="alert-link">소스 다운로드</a>)
			</div>
			<div class="alert alert-info">
				<h3>이 프로그램을 만드시는데 <b>기여</b>해 주신분</h3>
				<ul>
					<li><b>ActiveXXX</b> - 프로그램 개발및 실질적인 운영</li>
					<li><a class="alert-link" href="//www.todayhumor.co.kr/board/list.php?kind=member&mn=310980" target="_blank">i로보타</a> - 캐릭터 디자인 및 사용 허락. <a href="http://todayhumor.com/?humorbest_778763" target="_blank">오유 벼룩시장 캐릭터 일러스트레이터</a></li>
					<li><a class="alert-link" href="//www.todayhumor.co.kr/board/list.php?kind=member&mn=293448" target="_blank">E_Sukme</a><a class="alert-link" href="//www.todayhumor.co.kr/board/list.php?kind=member&mn=351662" target="_blank">an</a> - 서버 제공</li>
				</ul>
			</div>
			<div class="alert alert-warning">
				<h3>통계및 원활한 아바타 표시를 위하여 다음의 정보가 수집됩니다.</h3>
				<ul>
					<li>회원번호 및 닉네임</li>
					<li>가입날짜</li>
					<li>글의 갯수</li>
					<li>아바타 정보 및 자기소개</li>
					<li>조회수</li>
				</ul>
				<br />
				<b>오유에서 공식 제공한 API 내의 정보만 수집되며, 아이디, 이메일, 비밀번호 등의 개인정보는 절대 수집되지 않습니다.<br />오유 도메인 내에서만 정보가 수집되며, 테스트(자료창고) 및 신고 게시판에서는 정보가 수집되지 않습니다.</b>
			</div>
			<div class="alert alert-danger">
				<h3>주의사항</h3>
				<p>본 꼬릿말 서비스 제작자는 오유 운영팀과 관련이 없습니다.<br />이 프로그램 소스 및 캐릭터 디자인은 오유 외에서는 절대 사용하실 수 없으며 오유 내에서만 자유롭게 수정 재업이 가능합니다. 단, 수정 소스도 소스코드를 알기 쉽게 하여 공개해야 합니다.<br />혹시 오유가 아닌 외부 사이트에서 해당 캐릭터 및 디자인이 악용되어 사용되는 걸 보시면 신고 ( <a href="mailto:irobota@daum.net" target="_blank" class="alert-link">irobota@daum.net</a> / <a href="mailto:oumarket.seoul@gmail.com" target="_blank" class="alert-link">oumarket.seoul@gmail.com</a> )부탁드리겠습니다~~!</p>
			</div>
			<a href="?type=step_1" style="display:block"><button class="btn btn-primary" style="display:block;width:100%; font-size:18px">동의및 다음 단계로</button></a>
			<?php
		break;

		case 'step_1':
			step_progress(25);
			?><style>.ouavatar_preview{
				background-color:white; 
				border:1px solid silver; 
				border-radius:3px; 
				padding:4px; 
				display:none;
				font-size:18pt;
				word-break:break-all;
				z-index:10;
			}
			.ouavatar_preview_img{
				max-width:150px;
				
				vertical-align:text-top;
			}
			#ouavatar_result_img{
				max-width:100%;
			}
			.ouavatar_char_select_close_button{
				background-color:#000000; 
				color:white; 
				padding:4px;
				cursor:pointer; 
				font-size:12pt;
				display:inline-block;
			}	
			.ouavatar_char_select_category_button{
				background-color:#1E90FF; 
				color:white; 
				padding:4px;
				padding-left:12px;
				padding-right:12px;
				margin-left:2px;
				margin-right:2px;
				cursor:pointer; 
				font-size:12pt;
				display:inline-block;
			}
			.ouavatar_emoticon_button, .ouavatar_emoticon_button_complex{
				padding:8px;
				font-size:12pt;
				cursor:pointer;
				display:inline-block;
				white-space:nowrap;
			}
			.ouavatar_emoticon_button_complex{
				color:#BFBFBF;
			}

			.ouavatar_emoticon_button:hover{
				background-color:#E6E6FA;
				
			}
			</style>
			<script src="http://activexxx.mooo.com/ouavatar/lib/colpick/js/colpick.js" type="text/javascript"></script>
			<link rel="stylesheet" href="http://activexxx.mooo.com/ouavatar/lib/colpick/css/colpick.css" type="text/css">
			<form id="form1" action="?type=step_2" method="get">
				<label class="input-group">
					<span class="input-group-addon">언어: </span>
					<select name="language" class="form-control">
						<option value="ko_kr">한국어</option>
						<option value="en_us">English(US)</option>
						<option value="en_uk">English(UK)</option>
						<option value="es_es">Español</option>
						<option value="eo_eo">Esperanto</option>
						<option value="fr_fr">Français</option>
						<option value="ja_jp">日本語</option>
						<option value="zh_cn">简体中文</option>
						<option value="zh_tw">繁體中文</option>
						<option value="ru_ru">Русский</option>
					</select>
				</label>
				<label class="input-group"><span class="input-group-addon">텍스트 정렬</span><label class="form-control"><input type="radio" name="rtl">왼쪽</label><label class="form-control"><input type="radio" name="rtl">오른쪽</label></label>
				<label class="input-group"><span class="input-group-addon">회원번호</span><input type="number" name="mn" class="form-control" min="0" id="mn_help" required></label>
				<div class="alert alert-info" id="mn_help_alert" style="display:none">
					<h3>회원번호란?</h3>
					<p>오유 내에서 회원을 구분하기 위해 부여되는 번호입니다. 이 번호는 <b>자신의 개인페이지의 주소를 통해 알 수 있습니다.</b><blockquote>http://www.todayhumor.co.kr/board/list.php?kind=member&mn=<b>[여기의 숫자]</b></blockquote>이 번호는 회원가입 순서대로 쭉 부여된것이기에, 이 번호를 보면 오유에서 내가 몇번째로 가입했는지도 알 수 있습니다.<br />오유 영자님이 회원번호가 1이 아닌 36인 이유는 <a href="http://www.todayhumor.co.kr/board/view.php?table=animation&no=99048" target="_blank" class="alert-link">첫번째로 가입하시고 “영원한 1번은 없다” 라는 말을 남긴체 탈퇴 하신후, 재가입하시는걸 까먹으시고(···) 35명이 더 가입한후 회원가입을 하셔서 그렇다는 전설 (클릭시 넘어감)</a>, <a href="http://www.todayhumor.co.kr/board/view.php?table=humorbest&no=868967" target="_blank" class="alert-link" title="통제구역님 댓글 참고">(참고 링크#2)</a>이 있습니다.</p>
				</div>
				<label class="input-group"><span class="input-group-addon">자기소개 첫번째 줄</span><input type="text" name="about1" id="about1" class="form-control charpad"><div class="ouavatar_preview" id="ouavatar_char_about1"></div></label>
				<label class="input-group"><span class="input-group-addon">자기소개 두번째 줄</span><input type="text" name="about2" id="about2" class="form-control charpad"><div class="ouavatar_preview" id="ouavatar_char_about2"></div></label>
				<hr />
				<div style="margin-top:15px">
					<fieldset style="padding-bottom:20px">
						<legend>기본 제공 태마</legend>
						<div class="row">
							<div class="col-sm-6"><input type="button" class="btn btn-warning" value="낮" style="display:block;width:100%;font-size:24px" onclick="ouavatar_preset('day')" /></div>
							<div class="col-sm-6"><input type="button" class="btn btn-primary" value="밤" style="display:block;width:100%;font-size:24px" onclick="ouavatar_preset('night')" /></div>
						</div>
					</fieldset>
					<label class="input-group"><span class="input-group-addon">아이콘</span><label class="form-control"><input type="radio" name="nick_icon" value="star"><span style="color:#FFA500;">★</span> (별)</label><label class="form-control"><input type="radio" name="nick_icon" value="ribbon"><img src="http://activexxx.mooo.com/ouavatar/icon/icon_ribbon_small.png" alt="">(리본)</label><label class="form-control"><input type="radio" name="nick_icon" value="heart"><span style="color:#FFA500;">♥</span> (하트)</label></label>
					<label class="input-group"><span class="input-group-addon">아이콘 색상</span><input type="text" name="star_color" class="picker form-control" id="star_color"></label>
					<label class="input-group"><span class="input-group-addon">배경 색상</span><input type="text" name="background_color" class="picker form-control" id="bg_color"></label>
					<label class="input-group"><span class="input-group-addon">기본 글자색상 </span><input type="text" name="text_color" class="picker form-control" id="text_color"></label>
					<label class="input-group"><span class="input-group-addon">자기소개 글자색상</span><input type="text" name="about_color" class="picker form-control" id="about_color"></label>
				</div>
				<button class="btn btn-primary" style="display:block;width:100%; font-size:18px">다음 단계로</button>
				<input type="hidden" name="type" value="step_2">
				<?php get_to_input(); ?>
			</form>
			<script>
				$('#mn_help').click(function(){
					$('#mn_help_alert').slideDown();
				});

				$('input.charpad').click(function(){
					$('#ouavatar_char_'+$(this).attr('name')).slideDown(200);
				});

				function colpick(id, default_color){
					if ($(id).val() == "")
					$(id).val(default_color)
					else
					default_color = $(id).val();
					 

					 
					$(id).css('border-color',"#"+default_color)
					$(id).colpick({
						layout:"hex",
						submit:0,
						colorScheme:'dark',
						onChange:function(hsb,hex,rgb,fromSetColor) {
							if(!fromSetColor){
								$(id).val(hex).css('border-color','#'+hex);
							}
						}
					})
					.keyup(function(){
						$(this).colpickSetColor(this.value);
					});
					 
					// 문자를 넣을 때마다 바뀌도록 설정
					$(id).change(function(){
						$(id).css('border-color','#'+$(id).val())
					})
				}

				function charpad_change(tmp_class, id){
					$('#ouavatar_char_'+id+' [class^=ouavatar_charpad]').hide(); 
					$('#ouavatar_char_'+id+' '+tmp_class).show(); 
					$('#'+id).focus(); 
					$('#ouavatar_char_'+id).show(); 
				}
					
				function charpad_set(id){
					$.get("lib/php/ouavatar_charpad.php", { id:id } )
					.done(function( data ) {
						$("#ouavatar_char_"+id).html(data);
					});
				}
				function ouavatar_char_click(me, id){
					$("#"+id).val($("#"+id).val()+$(me).attr("char"))
				}
				

				$(document).ready(function() {
					colpick('#about_color',"0E76DC");
					colpick('#text_color',"000000");
					colpick('#star_color',"FFA500");
					colpick('#bg_color',"ADD8E6");  
					
					charpad_set("about1");
					charpad_set("about2");
				})
				function ouavatar_preset(mode){
					if (mode == "day"){
						$("#star_color").val("FFA500").css('border-color',"#FFA500");
						$("#bg_color").val("ADD8E6").css('border-color',"#ADD8E6");
						$("#text_color").val("000000").css('border-color',"#000000");
						$("#about_color").val("0E76DC").css('border-color',"#0E76DC");
					}
					else if (mode == "night"){
						$("#star_color").val("0056ab").css('border-color',"#0056ab");
						$("#bg_color").val("00023b").css('border-color',"#00023b");
						$("#text_color").val("ffffff").css('border-color',"#ffffff");
						$("#about_color").val("94d2ff").css('border-color',"#94d2ff");
					}
				}
			</script>
			<?php
		break;

		case 'step_2':
			step_progress(50);
			?>
			<div class="panel panel-default">
				<div class="panel-heading">첫번째 설정 결과물</div>
				<img src="new_ouavatar.php?<?php echo $_SERVER['QUERY_STRING']; ?>" alt="" style="margin:0 auto;display:block">
			</div>
			<script src="http://activexxx.mooo.com/ouavatar/lib/colpick/js/.js" type="text/javascript"></script>
			<script src="http://activexxx.mooo.com/ouavatar/lib/colpick/js/colpick.js" type="text/javascript"></script>
			<link rel="stylesheet" href="http://activexxx.mooo.com/ouavatar/lib/colpick/css/colpick.css" type="text/css">
			<form action="?type=step_2" method="get">
				<fieldset>
					<legend>아바타 머리/몸통</legend>
					<div>
						<label class="input-group"><span class="input-group-addon">포즈</span><input type="number" name="body_num" class="form-control preview" id="body_num" data-preview="body"></label>
						<?php echo avatar_preview("body",$max_body_num); ?>
						<label class="input-group"><span class="input-group-addon">상의 색상</span><input type="text" name="body_color" class="picker form-control" id="body_color"></label>
						<label class="input-group"><span class="input-group-addon">OU 마크 색상</span><input type="text" name="oumark_color" class="picker form-control" id="oumark_color"></label>
						<label class="input-group"><span class="input-group-addon">OU 마크 발광 강도 (0 ~ 100)</span><input type="number" name="oumark_gloss_opacity"  class="form-control preview" id="oumark_gloss_opacity" data-preview="oumark"></label>
						<?php echo avatar_preview("oumark",100,true); ?>
						<label class="input-group"><span class="input-group-addon">오징어머리</span><input type="number" name="squid_num"  class="form-control preview" id="squid_num" data-preview="squid"></label>
						<?php echo avatar_preview("squid",$max_squid_num); ?>
						<label class="input-group"><span class="input-group-addon">왼쪽 오징어머리 방향</span><label class="form-control"><input type="radio" name="squid_left_direction" value="up">위</label><label class="form-control"><input type="radio" name="squid_left_direction" value="down">아래</label></label>
						<label class="input-group"><span class="input-group-addon">오른쪽 오징어머리 방향</span><label class="form-control"><input type="radio" name="squid_right_direction" value="up">위</label><label class="form-control"><input type="radio" name="squid_right_direction" value="down">아래</label></label>
					</div>
				</fieldset>
				<hr />
				<fieldset>
					<legend>아바타 얼굴</legend>
					<div>
						<label class="input-group"><span class="input-group-addon">피부색</span><input type="text" name="skin_color" class="picker form-control" id="skin_color"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">헤어스타일</span><input type="number" name="hair_num"  class="form-control preview" id="hair_num" data-preview="hair"></label>
						<?php echo avatar_preview("hair",$max_hair_num); ?>
						<label class="input-group"><span class="input-group-addon">헤어 색상</span><input type="text" name="hair_color" class="picker form-control" id="hair_color"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">볼(blush)</span><input type="number" name="blush_num"  class="form-control preview" id="blush_num" data-preview="blush"></label>
						<?php echo avatar_preview("blush",$max_blush_num);  ?>
						<label class="input-group"><span class="input-group-addon">볼 색상</span><input type="text" name="blush_color" class="picker form-control" id="blush_color"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">눈썹</span><input type="number" name="brow_num"  class="form-control preview" id="brow_num" data-preview="brow"></label>
						<?php echo avatar_preview("brow",$max_brow_num);  ?>
						<label class="input-group"><span class="input-group-addon">눈썹 색상</span><input type="text" name="brow_color" class="picker form-control" id="brow_color"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">눈/눈동자</span><input type="number" name="eyes_num"  class="form-control preview" id="eyes_num" data-preview="eyes"></label>
						<?php echo avatar_preview("eyes",$max_eyes_num);  ?>
						<label class="input-group"><span class="input-group-addon">눈 색상</span><input type="text" name="eyes_color" class="picker form-control" id="eyes_color"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">안경</span><input type="number" name="glasses_num"  class="form-control preview" id="glasses_num" data-preview="glasses"></label>
						<?php echo avatar_preview("glasses",$max_glasses_num);  ?>
						<label class="input-group"><span class="input-group-addon">안경 테 색상</span><input type="text" name="glasses_border_color" class="picker form-control" id="glasses_border_color"></label>
						<label class="input-group"><span class="input-group-addon">안경 렌즈 색상</span><input type="text" name="glasses_lens_color" class="picker form-control" id="glasses_lens_color"></label>
						<label class="input-group"><span class="input-group-addon">안경 렌즈 투명도 (0~100)</span><input type="text" name="glasses_opacity" class="form-control" id="glasses_opacity"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">코</span><input type="number" name="nose_num"  class="form-control preview" id="nose_num" data-preview="nose"></label>
						<?php echo avatar_preview("nose",$max_nose_num);  ?>
						<label class="input-group"><span class="input-group-addon">코 색상</span><input type="text" name="nose_color" class="picker form-control" id="nose_color"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">입/혀/치아</span><input type="number" name="mouth_num"  class="form-control preview" id="mouth_num" data-preview="mouth"></label>
						<?php echo avatar_preview("mouth",$max_mouth_num);  ?>
						<label class="input-group"><span class="input-group-addon">입 색상</span><input type="text" name="mouth_color" class="picker form-control" id="mouth_color"></label>
						<label class="input-group"><span class="input-group-addon">혀 색상</span><input type="text" name="mouth_color" class="picker form-control" id="mouth_color"></label>
						<label class="input-group"><span class="input-group-addon">치아 색상</span><input type="text" name="tongue_color" class="picker form-control" id="tongue_color"></label>
						<hr />
						<label class="input-group"><span class="input-group-addon">하체</span><input type="number" name="bottom_num"  class="form-control preview" id="bottom_num" data-preview="bottom"></label>
						<?php echo avatar_preview("bottom",$max_bottom_num);  ?>
						<label class="input-group"><span class="input-group-addon">하의 색상</span><input type="text" name="bottom_color" class="picker form-control" id="bottom_color"></label>
						<hr />
					</div>
				</fieldset>
				<?php get_to_input(); ?>
				<input type="hidden" name="type" value="step_3">
				<button class="btn btn-primary" style="display:block;width:100%; font-size:18px">다음 단계로</button>
			</form>
			<script>
			function colpick(id, default_color){
					if ($(id).val() == "")
					$(id).val(default_color)
					else
					default_color = $(id).val();
					 

					 
					$(id).css('border-color',"#"+default_color)
					$(id).colpick({
						layout:"hex",
						submit:0,
						colorScheme:'dark',
						onChange:function(hsb,hex,rgb,fromSetColor) {
							if(!fromSetColor){
								$(id).val(hex).css('border-color','#'+hex);
							}
						}
					})
					.keyup(function(){
						$(this).colpickSetColor(this.value);
					});
					 
					// 문자를 넣을 때마다 바뀌도록 설정
					$(id).change(function(){
						$(id).css('border-color','#'+$(id).val())
					})
				}

				// color picker
				$(document).ready(function() {
					colpick('#body_color',"A52A2A");    
					colpick('#oumark_color',"a4c4db");  
					colpick('#skin_color',"ffb273");    
					colpick('#hair_color',"450000");    
					colpick('#blush_color',"FF8973");   
					colpick('#eyes_color',"FFFFFF");    
					colpick('#pupil_color',"450000");
					colpick('#brow_color',"450000");
					colpick('#nose_color',"450000");
					colpick('#mouth_color',"7B220B");
					colpick('#tongue_color',"FF6B70");
					colpick('#teeth_color',"FFFFFF");
					colpick('#bottom_color',"FF6B70");
					 
					colpick('#glasses_border_color',"000000");  
					colpick('#glasses_lens_color',"ADD8E6");
				});

				$('.form-control.preview').on('focus', function(){
					$('#'+$(this).data('preview')+'_preview').slideDown();
				});
				$('.form-control.preview').on('blur',function(){
					$('#'+$(this).data('preview')+'_preview').slideUp();
				});
			</script>
			<?php 
			break;
		case 'step_3': 
			step_progress(75); ?>
			<div class="panel panel-default">
				<div class="panel-heading">두번째 설정 결과물</div>
				<img src="new_ouavatar.php?<?php echo $_SERVER['QUERY_STRING']; ?>" alt="" style="margin:0 auto;display:block">
			</div>
			<form action="?type=step_4" method="get">
				<fieldset>
					<legend>아바타 회전</legend>
					<div class="alert alert-danger">※아직 완전하지 않습니다.<br />직각이 아닌 회전시 화질이 열화되는 현상이 있습니다.</div>
					<label class="input-group"><span class="input-group-addon">회전 각도</span><input type="number" name="rotate" class="form-control"></label>
					<label class="input-group"><span class="input-group-addon">회전 오프셋</span><input type="number" name="crop" class="form-control"></label>
				</fieldset>
				<hr />
				<fieldset>
					<legend>하이퍼링크 및 말풍선</legend>
					<label class="input-group"><span class="input-group-addon">하이퍼링크 URL</span><input type="url" name="hyperlink" class="form-control" value="http://activexxx.mooo.com/ouavatar/ouavatar_menu.php"></label>
					<div class="alert alert-info">꼬릿말 클릭시 이동할 주소입니다. (기본값: 꼬릿말 생성 프로그램 주소)<br />자신의 블로그 주소등을 적어두면 꼬릿말 클릭시 해당 사이트로 이동시킬수 있습니다.</div>
					<label class="input-group"><span class="input-group-addon">말풍선</span><input type="text" name="title" class="form-control" value="오늘의유머 아바타 꼬릿말 서비스 by ActiveXXX"></label>
					<div class="alert alert-info">이미지에는 보이지 않지만, 마우스를 올렸을 시 나올 말풍선을 설정합니다.</div>
				</fieldset>
				<input type="hidden" name="step_1" value="<?php echo htmlspecialchars($_GET['step_2']); ?>" />
				<input type="hidden" name="type" value="end">
				<?php get_to_input(); ?>
				<button class="btn btn-primary" style="display:block;width:100%; font-size:18px">다음 단계로</button>
			</form>
			<?php
			break;
		case 'end': 
			step_progress(100);
			include_once('lib/php/ouavatar_if.php'); ?>
			<div class="panel panel-primary">
				<h3 class="panel-heading panel-title">최종 결과물</h3>
				<?php if(isset($_GET['hyperlink']) && empty($_GET['hyperlink']) == false){ ?>
				<a href="<?php echo htmlspecialchars($_GET{'hyperlink'}); ?>" target="_blank" <?php if(isset($_GET['title']) && empty($_GET['title']) == false){ ?> title="<?php echo $_GET['title']; ?>"<?php } ?>>
				<?php } ?>
					<img src="http://activexxx.mooo.com/ouavatar/new_ouavatar.php?start=true&<?php echo $_SERVER['QUERY_STRING']; ?>&end=true&isold&.png" alt="<?php if(isset($_GET['title']) && empty($_GET['title']) == false){ echo $_GET['title']; } ?>" style="margin:0 auto;display:block">
				<?php if(isset($_GET['hyperlink']) && empty($_GET['hyperlink']) == false){ ?>
				</a>
				<?php } ?>
			</div>
			<div class="panel panel-info">
				<h3 class="panel-heading panel-title">HTML 코드</h3>
				<textarea style="width:100%; min-height:200px">	<?php if(isset($_GET['hyperlink']) && empty($_GET['hyperlink']) == false){ ?><a href="<?php echo htmlspecialchars($_GET{'hyperlink'}); ?>" target="_blank" <?php if(isset($_GET['title']) && empty($_GET['title']) == false){ ?> title="<?php echo $_GET['title']; ?>"<?php } ?>><?php } ?><img src="http://activexxx.mooo.com/ouavatar/new_ouavatar.php?start=true&<?php echo $_SERVER['QUERY_STRING']; ?>&end=true&isold&.png" alt="<?php if(isset($_GET['title']) && empty($_GET['title']) == false){ echo $_GET['title']; } ?>" style="margin:0 auto;display:block">	<?php if(isset($_GET['hyperlink']) && empty($_GET['hyperlink']) == false){ ?></a><?php } ?>
				</textarea>
			</div>
			<div class="panel panel-info">
				<h3 class="panel-heading panel-title">이미지 주소</h3>
				<textarea style="width:100%; min-height:200px">http://activexxx.mooo.com/ouavatar/new_ouavatar.php?start=true&<?php echo $_SERVER['QUERY_STRING']; ?>&end=true&isold&.png</textarea>
			</div>
			<div class="panel panel-info">
				<h3 class="panel-heading panel-title">꼬릿말 적용 방법</h3>
				<div style="padding:10px">
					<ol>
						<li>PC버전 오유에서 로그인 하고 로그인 칸 옆에 있는 “<a href="http://www.todayhumor.co.kr/member/modify.php" target="_blank">정보변경</a>” 을 클릭</li>
						<li>페이지 중후반 쯤에 있는 “꼬릿말” 부분에 “HTML 사용” 을 체크하고 아래 칸에 이 꼬릿말 생성 페이지에 있는 HTML 코드를 붙여넣음. 아래와 같이 될것임.<br /><img src="//cdn.esukmean.com/ou/ou_tail_preview.png" /></li>
						<li>확인 버튼을 누름</li>
						<li>잘 되는지 테스트.<br />게시판 이용 테스트용 게시판인 <a href="http://www.todayhumor.co.kr/board/list.php?table=databox" target="_blank">자료창고</a> 게시판이나, 자유게시판에 뻘글과 함께 은근슬쩍 테스트 하시면 됩니다. <b>자게의 경우 꼭 뻘글 등을 같이 써 주세요. 달랑 꼬릿말 하나 올리는건 좀 아닌것 같습니다.</b></li>
					</ol>
				</div>
			</div>
			<?php
			break;
		case 'summary': ?>
			<script src="lib/js/ouavatar_statistics_json.js"></script>
			<fieldset>
				<legend>
					<h2>
						<span>통계 및 순위</span>
						<button style="margin-left:3px" class="btn btn-primary" id="refresh">새로고침</button>
						<button style="margin-left:3px" class="btn btn-info" title="통계에 대한 정보" onclick="$('#header_info').slideDown();">?</button>
					</h2>
				</legend>
				<div class="alert alert-info" style="display:none" id="header_info">
					<span>통계및 원활한 아바타 표시를 위하여 다음의 정보가 수집됩니다.</span>
					<ul>
						<li>회원번호 및 닉네임</li>
						<li>가입날짜</li>
						<li>글의 갯수</li>
						<li>아바타 정보 및 자기소개</li>
						<li>조회수</li>
						<li>OS 및 브라우저 정보(정확한 브라우저명을 알지 못 할 때만 전체 User-agent 수집)</li>
					</ul>
					<br />
					<b>오유에서 공식 제공한 API 내의 정보만 수집되며, 아이디, 이메일, 비밀번호 등의 개인정보는 절대 수집되지 않습니다.<br />오유 도메인 내에서만 정보가 수집되며, 테스트(자료창고) 및 신고 게시판에서는 정보가 수집되지 않습니다.</b>
				</div>
				<div class="row">
					<fieldset class="col-sm-6">
						<legend>사용자수, 표시수···</legend>
						<div id="statis_info"></div>
					</fieldset>
					<fieldset class="col-sm-6">
						<legend>브라우저, OS 정보</legend>
						<div id="statis_ua"></div>
					</fieldset>
				</div>
			</fieldset>
			<script>
				function get_statistics(){
					var total_active_users = 0;
					var total_hits = 0;
					var total_refresh = 0;

					$.getJSON('lib/php/new_ouavatar_statistics_json.php',function(data){
						var i = 0;
						var to_append = '';

						var si = $('#statis_info').html('<div data-range="week"><button class="btn btn-primary" style="width:100%;margin:5px 0">이번주 보기/숨기기</button><div></div></div>');
						si = si.children('div[data-range="week"]').children('div');

						for (; i < 7; i++) {
							to_append = '<table style="width:100%">';
							to_append = to_append + '<thead>';
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td colspan=2 style="background-color:#BBEFFF;">';
							to_append = to_append + data[i]['date'];
							to_append = to_append + '</td>';
							to_append = to_append + '</tr></thead><tbody>';
							
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td>';
							to_append = to_append + "활성 사용자 수";
							to_append = to_append + '</td>';
							to_append = to_append + '<td style="text-align:right">';
							to_append = to_append + '<span style="color:red;">'+ data[i]['users_num'] +"</span> 명 ";
							total_active_users = total_active_users + parseInt(data[i]['users_num']);
							if (data[i]['users_arrow'] == "up"){
								to_append = to_append + "<span style='color:green;'>▲</span>";
							} else if (data[i]['users_arrow'] == "down"){
								to_append = to_append + "<span style='color:red;'>▼</span>";
							}
							to_append = to_append + '</td>';
							to_append = to_append + '</tr>';
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td>';
							to_append = to_append + "갱신된 꼬릿말";
							to_append = to_append + '</td>';
							to_append = to_append + '<td style="text-align:right">';
							to_append = to_append + '<span style="color:red;">'+ data[i]['refresh_num'] +"</span> 개 ";
							total_refresh = total_refresh + parseInt(data[i]['refresh_num']);
							if (data[i]['refresh_arrow'] == "up"){
								to_append = to_append + "<span style='color:green;'>▲</span>";
							} else if (data[i]['refresh_arrow'] == "down"){
								to_append = to_append + "<span style='color:red;'>▼</span>";
							}
							to_append = to_append + '</td>';
							to_append = to_append + '</tr>';
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td>';
							to_append = to_append + '조회된 꼬릿말';
							to_append = to_append + '</td>';
							to_append = to_append + '<td style="text-align:right">';
							to_append = to_append + '<span style="color:red;">'+ data[i]['view_num'] +"</span> 개 ";
							total_hits = total_hits + parseInt(data[i]['view_num']);
							if (data[i]['view_arrow'] == "up"){
								to_append = to_append + "<span style='color:green;'>▲</span>";
							} else if (data[i]['view_arrow'] == "down"){
								to_append = to_append + "<span style='color:red;'>▼</span>";
							}
							to_append = to_append + '</td>';
							to_append = to_append + '</tr>';
							
							to_append = to_append + '</tbody>';
							to_append = to_append + '</table>';
							si.append(to_append);
						};

						var si = $('#statis_info').append('<div data-range="month"><button class="btn btn-primary" style="width:100%;margin:5px 0">이번달 보기/숨기기</button><div style="display:none"></div></div>');
						si = si.children('div[data-range="month"]').children('div');

						for (; i < data.length; i++) {
							to_append = '<table style="width:100%">';
							to_append = to_append + '<thead>';
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td colspan=2 style="background-color:#BBEFFF;">';
							to_append = to_append + data[i]['date'];
							to_append = to_append + '</td>';
							to_append = to_append + '</tr></thead><tbody>';
							
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td>';
							to_append = to_append + "활성 사용자 수";
							to_append = to_append + '</td>';
							to_append = to_append + '<td style="text-align:right">';
							to_append = to_append + '<span style="color:red;">'+ data[i]['users_num'] +"</span> 명 ";
							total_active_users = total_active_users + parseInt(data[i]['users_num']);
							if (data[i]['users_arrow'] == "up"){
								to_append = to_append + "<span style='color:green;'>▲</span>";
							} else if (data[i]['users_arrow'] == "down"){
								to_append = to_append + "<span style='color:red;'>▼</span>";
							}
							to_append = to_append + '</td>';
							to_append = to_append + '</tr>';
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td>';
							to_append = to_append + "갱신된 꼬릿말";
							to_append = to_append + '</td>';
							to_append = to_append + '<td style="text-align:right">';
							to_append = to_append + '<span style="color:red;">'+ data[i]['refresh_num'] +"</span> 개 ";
							total_refresh = total_refresh + parseInt(data[i]['refresh_num']);
							if (data[i]['refresh_arrow'] == "up"){
								to_append = to_append + "<span style='color:green;'>▲</span>";
							} else if (data[i]['refresh_arrow'] == "down"){
								to_append = to_append + "<span style='color:red;'>▼</span>";
							}
							to_append = to_append + '</td>';
							to_append = to_append + '</tr>';
							
							to_append = to_append + '<tr>';
							to_append = to_append + '<td>';
							to_append = to_append + '조회된 꼬릿말';
							to_append = to_append + '</td>';
							to_append = to_append + '<td style="text-align:right">';
							to_append = to_append + '<span style="color:red;">'+ data[i]['view_num'] +"</span> 개 ";
							total_hits = total_hits + parseInt(data[i]['view_num']);
							if (data[i]['view_arrow'] == "up"){
								to_append = to_append + "<span style='color:green;'>▲</span>";
							} else if (data[i]['view_arrow'] == "down"){
								to_append = to_append + "<span style='color:red;'>▼</span>";
							}
							to_append = to_append + '</td>';
							to_append = to_append + '</tr>';
							
							to_append = to_append + '</tbody>';
							to_append = to_append + '</table>';
							si.append(to_append);
						};

						$('#statis_info button').click(function(){
							if($(this).parent().children('div').css('display') == 'none') $(this).parent().children('div').slideDown(800);
							else $(this).parent().children('div').slideUp();
						});
					});

					$.getJSON('lib/php/ouavatar_user_agent_json.php',function(data){
						var sua = $('#statis_ua').slideUp(500).slideDown(300).html('');
						var to_append = '';
						for (var i = 0; i < data.length; i++) {
							to_append = '<div class="well well-sm" style="margin-bottom:3px"><span>'+data[i]['agent']+'</span><span class="badge" style="float:right">'+data[i]['num']+'</span></div>';
							sua.append(to_append);
						};
					});
				}

				$('#refresh').click(function(){ $('#statis_info').slideUp(500); get_statistics(); $('#statis_info').slideDown(300); });
				get_statistics();
			</script>
			<?php
			break;
		
		default:
			# code...
			break;
	}