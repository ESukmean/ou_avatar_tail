ouavatar 바깥 경로에 
만약 var/www/html/ouavatar/ 라면

파일경로는 ouavatar 경로 밖에 있는
var/www/html/activexxx_mysql_config.php
이렇게 정해주십시오.
(ouavatar 경로의 ouavatar_config.php에서 사용자 경로를 설정 가능합니다)
 
소스 재패키징 및 공유 시 안전하게 하기 위한 조치이므로
불편하시더라도 양해 바랍니다.

다음은 activexxx_mysql_config.php의 내용물입니다.

<?php
$mysqli_host = '호스트명';
$mysqli_id = 'MySQL 아이디';
$mysqli_pw = 'MySQL 비밀번호';
$mysqli_db = 'MySQL DB명';
?>


