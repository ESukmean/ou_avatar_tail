/* IE6/Mozilla Support */
$( window ).resize(function() {
	ouavatar_style_response()
})
						
function ouavatar_style_response(){
	if($.browser.msie&&($.browser.version == "6.0")&&!$.support.style){  
		$(".ouavatar_menu_vertical_container, .ouavatar_logo_line").each(function(){  
			if($(this)[0].scrollWidth>800) { 
				$(".ouavatar_menu_vertical_container").css({"width":"830px"});
				$(".ouavatar_logo_line").css({"width":"800px"});
				$("#ouavatar_result_img").css({"width":""});
			} else {
				$(".ouavatar_menu_vertical_container").css({"width":"100%"});
				$(".ouavatar_logo_line").css({"width":"100%"});
				$("#ouavatar_result_img").css({"width":"100%"});
			}
		});
	} else if ($.browser.mozilla){
		if($(window).width() > 800) { 
			$("#ouavatar_result_img").css({"width":""});
		} else {
			$("#ouavatar_result_img").css({"width":"100%"});
		}
	}
}

$(document).ready(function(){
	ouavatar_style_response();
})
