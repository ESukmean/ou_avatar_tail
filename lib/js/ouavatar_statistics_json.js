function ouavatar_statistics_refresh(){
	$("#ouavatar_statistics_ajax").html('<span style="width:100%; display:inline-block; text-align:center; color:#BFBFBF;">통계 정보를 로딩중입니다...</span>');
	
	$.getJSON(current_path+"/lib/php/ouavatar_statistics_json.php",function(data){
		json_data ='<input type="button" onclick=\'$("#ouavatar_statistics_week_table").slideToggle()\' value="일주일 간 통계 보기" style="width:100%; height:32px;">';
		
		var total_active_users = 0;
		var total_hits = 0;
		var total_refresh = 0;
		
		$.each(data, function(key,value) {
				if (this['days'] == 0){
					json_data = json_data + '<div style="display:block; padding-top:4px; padding-bottom:4px; background-color:#1E90FF; color:white; text-align:center; font-weight:bold;">';
					json_data = json_data + '오늘</div> ';
				} else if (this['days'] == 7){
					json_data = json_data + '<div style="display:block; padding-top:4px; padding-bottom:4px; background-color:#1E90FF; color:white; text-align:center; font-weight:bold;">';
					json_data = json_data + '일주일 전</div> ';
				} else if (this['days'] == 30){
					json_data = json_data + '<div id="ouavatar_statistics_week_table">';
					json_data = json_data + '<input type="button" onclick=\'$("#ouavatar_statistics_month_table").slideToggle()\' value="한 달 간 통계 보기" style="width:100%; height:32px;"><div id="ouavatar_statistics_month_table">'
					json_data = json_data + '<div style="display:block; padding-top:4px; padding-bottom:4px; background-color:#1E90FF; color:white; text-align:center; font-weight:bold;">';
					json_data = json_data + '1달 전</div> ';
				}
				json_data = json_data + '<table>';
				json_data = json_data + '<thead>';
				
				json_data = json_data + '<tr>';
				json_data = json_data + '<td colspan=2 style="background-color:#BBEFFF;">';
				json_data = json_data + this['date'];
				json_data = json_data + '</td>';
				json_data = json_data + '</tr></thead><tbody>';
				
				
				json_data = json_data + '<tr>';
				json_data = json_data + '<td>';
				json_data = json_data + "활성 사용자 수";
				json_data = json_data + '</td>';
				json_data = json_data + '<td style="text-align:right">';
				json_data = json_data + '<span style="color:red;">'+ this['users_num'] +"</span> 명 ";
				total_active_users = total_active_users + parseInt(this['users_num']);
				if (this['users_arrow'] == "up"){
					json_data = json_data + "<span style='color:green;'>▲</span>";
				} else if (this['users_arrow'] == "down"){
					json_data = json_data + "<span style='color:red;'>▼</span>";
				}
				json_data = json_data + '</td>';
				json_data = json_data + '</tr>';
				
				json_data = json_data + '<tr>';
				json_data = json_data + '<td>';
				json_data = json_data + "갱신된 꼬릿말";
				json_data = json_data + '</td>';
				json_data = json_data + '<td style="text-align:right">';
				json_data = json_data + '<span style="color:red;">'+ this['refresh_num'] +"</span> 개 ";
				total_refresh = total_refresh + parseInt(this['refresh_num']);
				if (this['refresh_arrow'] == "up"){
					json_data = json_data + "<span style='color:green;'>▲</span>";
				} else if (this['refresh_arrow'] == "down"){
					json_data = json_data + "<span style='color:red;'>▼</span>";
				}
				json_data = json_data + '</td>';
				json_data = json_data + '</tr>';
				
				json_data = json_data + '<tr>';
				json_data = json_data + '<td>';
				json_data = json_data + '조회된 꼬릿말';
				json_data = json_data + '</td>';
				json_data = json_data + '<td style="text-align:right">';
				json_data = json_data + '<span style="color:red;">'+ this['view_num'] +"</span> 개 ";
				total_hits = total_hits + parseInt(this['view_num']);
				if (this['view_arrow'] == "up"){
					json_data = json_data + "<span style='color:green;'>▲</span>";
				} else if (this['view_arrow'] == "down"){
					json_data = json_data + "<span style='color:red;'>▼</span>";
				}
				json_data = json_data + '</td>';
				json_data = json_data + '</tr>';
				
				json_data = json_data + '</tbody>';
				json_data = json_data + '</table>';
				if (this['days'] == 1){
					json_data = json_data + '</div> ';
				} else if (this['days'] == 8){
					json_data = json_data + '</div> ';
				}
		})
		/*console.log(json_data);*/
		
		
		json_data = json_data + '<table>';
		json_data = json_data + '<thead>';
				
		json_data = json_data + '<tr>';
		json_data = json_data + '<td colspan=2 style="background-color:#BBEFFF;">';
		json_data = json_data + '합계';
		json_data = json_data + '</td>';
		json_data = json_data + '</tr></thead><tbody>';
		json_data = json_data + '<tr>';
		json_data = json_data + '<td>';
		json_data = json_data + '갱신된 꼬릿말';
		json_data = json_data + '</td>';
		json_data = json_data + '<td style="text-align:right">';
		json_data = json_data + '<span style="color:red;">'+ total_refresh +"</span> 개 ";
		json_data = json_data + '</td>';
		json_data = json_data + '</tr>';
				
		json_data = json_data + '<tr>';
		json_data = json_data + '<td>';
		json_data = json_data + '조회된 꼬릿말';
		json_data = json_data + '</td>';
		json_data = json_data + '<td style="text-align:right">';
		json_data = json_data + '<span style="color:red;">'+ total_hits +"</span> 개 ";
		json_data = json_data + '</td>';
		json_data = json_data + '</tr>';
				
		json_data = json_data + '</tbody>';
		json_data = json_data + '</table>';
		
		$("#ouavatar_statistics_ajax").html(json_data);
	})
}
