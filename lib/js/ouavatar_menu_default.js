// 반응형웹을 위한 폭
responsible_width = 800;

// 모바일 같이 폭이 좁은 환경에서는 팔레트가 왼쪽으로 치우치는 버그가 있다.
// 타이머를 설정하여 0px 이하 마이너스의 값이 감지될 경우 치우치지 않게 한다.
setInterval(function(){
	$(".colpick.colpick_hex.colpick_hex_ns.colpick_dark").each(function(){
		if ($(this).position().left < 0 ){
			$(this).css({"left":"16px"})
		} 
	})
},100);



// 아바타 생성하기 
function ouavatar(){
	
	// enable vibration support
	navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;
		 
	if (navigator.vibrate) {
		// 만약 진동을 지원한다면 
		navigator.vibrate(500);
	}
	
	if(isNaN(document.getElementById("mn").value) || document.getElementById("mn").value == ''){
		alert("회원번호를 입력해 주세요.");
		document.getElementById('mn').focus();
		return;
	}
	url=current_path+"/ouavatar."+extension+"?"+$("#ouavatar_form").serialize().replace(/\'/g,"%27").replace(/\"/g,"%22") + "&isold&.png";
	//$('#load_avatar').val().replace("'",encodeURIComponent("'"));
	 
	 // 짧은주소 구글 API 시작
	 $.get(current_path+"/lib/php/ouavatar_shorturl.php",{url:url},function(response){
		$("#ouavatar_shorturl_hidden, #ouavatar_shorturl_link").val(response);
		$("#ouavatar_shorturl_link").attr({"href":response});
	 })
	// 짧은주소 구글 API 끝
	
	$("#result").html("<img id='ouavatar_result_img' src='"+url+"' onload='document.body.scrollTop = document.body.scrollHeight;' alt='"+ouavatar_title_default+"' title='"+$("#title").val()+"'><br>엑박이 뜨는 경우 밑의 URL에서 start=true부터 end=true까지 그대로 복사하여 다시 불러와 주십시오.<br>저장 후 업로드로 사용하시면 데이터가 갱신되지 않으므로 밑의 주소를 복사하는 것으로 사용하시기 바랍니다.<div style='text-align:center; '><input type='button' value='URL 표시하기' onclick='showurl(\"url\")' style='width:49%; height:48px; font-size:12pt;'><input type='button' value='&lt;img src&gt; 표시하기' onclick=showurl(\"imgsrc\") style='width:49%; height:48px; font-size:12pt;'></div><div id='showurldiv' style='padding:3px; word-break:break-all; background-color:#00FF00; width:100%; display:none;'></div><div id='showhelpdiv' style='display:block; margin-top:16px; display:none;'></div><input type='textbox' id='ouavatar_shorturl_hidden' style='display:none;'>")
	 
	$('#load_avatar').val($("#ouavatar_form").serialize().replace(/\'/g,"%27").replace(/\"/g,"%22"));
	 
	// document.body.scrollTop = document.body.scrollHeight;
	// IE에서 작동 불능.
	
	//$level = floor((($times*1) + ($replytimes*2) + ($totaltimes*3) + ($besttimes*5) + ($bobtimes*10))/200);
	// 레벨계산기준 = ((방문횟수*1) + (댓글수*2) + (전체글*3) + (베스트*5) + (베오베*10)) / 200 
	
	window.scrollTo(0,document.body.scrollHeight);
	// IE에서 작동 가능하게 
	// window.scrollTo(Xpos,Ypos)
	
} // function ouavatar() 아바타 설정 로드 끝 

function showurl(type){
	if (type == "url")	// URL 표시하기 클릭시 
	{
		$("#showurldiv").show();
		$("#showurldiv").html(url);
		
		$("#showhelpdiv").show();
		$("#showhelpdiv").html("꼬릿말로 사용하시려면 <input type='button' value='&lt;img src&gt; 표시하기' onclick=showurl(\"imgsrc\")> 버튼을 클릭하십시오.<hr>(짧은주소 사용시 아바타 정보 불러오기를 지원하지 않습니다.)<br><div style='padding:4px; margin-top:4px; background-color:#90EE90; font-size:12pt;' >짧은주소: <a href='"+$("#ouavatar_shorturl_hidden").val()+"' id='ouavatar_shorturl_link' target='_blank' class='ouavatar_shorturl_span' style='font-size:12pt;'>"+$("#ouavatar_shorturl_hidden").val()+"</a></div>");
		//document.body.scrollTop = document.body.scrollHeight;
		// IE에서 작동 불능.
		
	}
	else if (type =="imgsrc") // <img src> 클릭시
	{
		
		function html_attr_escape(val){
			return val.replace(/'/g, '&amp;#39;').replace(/\"/g, '&amp;quot;').replace(/\\/g, '&amp;#92;').replace(/\&/g,"&amp;");
		}
		
		
		
		
		
		// 하이퍼링크
		if ($("#hyperlink").val() == ""){
			ouavatar_href_start = "";
			ouavatar_href_end = "";
		} else {
			ouavatar_href_start = "&lt;a href='"+html_attr_escape($("#hyperlink").val())+"' target='_blank'&gt;";
			ouavatar_href_end = "&lt;/a&gt;";
		}
		
		$("#showurldiv").show();
		$("#showurldiv").html(
			ouavatar_href_start+"&lt;img src='"+url+"' alt='"+html_attr_escape(ouavatar_title_default)+"' title='"+html_attr_escape($("#title").val())+"' &gt;"+ouavatar_href_end);
		
		
		
		$("#showhelpdiv").show();
		$("#showhelpdiv").html("위의 꼬릿말 소스를 복사하신 후, <input type='button' value='꼬릿말 설정 페이지' onclick='window.open(\"http://www.todayhumor.co.kr/member/modify.php\")'>에서 반드시 <span style='color:red'>HTML 사용</span>에 체크하신 후 붙여넣으십시오.");
		//document.body.scrollTop = document.body.scrollHeight;
		// IE에서 작동 불능.
	}
	
	
	window.scrollTo(0,document.body.scrollHeight);
	// IE에서 작동 가능하게 
	// window.scrollTo(Xpos,Ypos)
}
   
 
function colpick(id, default_color){
	if ($(id).val() == "")
	$(id).val(default_color)
	else
	default_color = $(id).val();
	 

	 
	$(id).css('border-color',"#"+default_color)
	$(id).colpick({
		layout:"hex",
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,fromSetColor) {
			if(!fromSetColor){
				$(id).val(hex).css('border-color','#'+hex);
			}
		}
	})
	.keyup(function(){
		$(this).colpickSetColor(this.value);
	});
	 
	// 문자를 넣을 때마다 바뀌도록 설정
	$(id).change(function(){
		$(id).css('border-color','#'+$(id).val())
	})
}

// color picker
$(document).ready(function() {
	colpick('#about_color',"0E76DC");
	colpick('#text_color',"000000");
	colpick('#star_color',"FFA500");
	colpick('#bg_color',"ADD8E6");  
	colpick('#body_color',"A52A2A");    
	colpick('#oumark_color',"a4c4db");  
	colpick('#skin_color',"ffb273");    
	colpick('#hair_color',"450000");    
	colpick('#blush_color',"FF8973");   
	colpick('#eyes_color',"FFFFFF");    
	colpick('#pupil_color',"450000");
	colpick('#brow_color',"450000");
	colpick('#nose_color',"450000");
	colpick('#mouth_color',"7B220B");
	colpick('#tongue_color',"FF6B70");
	colpick('#teeth_color',"FFFFFF");
	colpick('#bottom_color',"FF6B70");
	 
	colpick('#glasses_border_color',"000000");  
	colpick('#glasses_lens_color',"ADD8E6");
	
	charpad_set("about1");
	charpad_set("about2");
})
 
function avatar_load(){
	location.href = current_page+"?"+$('#load_avatar').val().replace(/%27/g,"%26apos%3B").replace(/%22/g,"%26quot%3B").replace(/\'/g,"%26apos%3B").replace(/\"/g,"%26quot%3B").replace(/http:\/\/[^>]+\.(php|esm)\?/i,"");
}
 
 
$( document ).ready(function() {
	// 아바타 정보를 로드하였을 때
	$('#load_avatar').val($("#ouavatar_form").serialize().replace(/%27/g,"%26apos%3B").replace(/%22/g,"%26quot%3B").replace(/\'/g,"%26apos%3B").replace(/\"/g,"%26quot%3B"));
})
 
// 낮/밤 설정
function ouavatar_preset(mode){
	if (mode == "day"){
		$("#star_color").val("FFA500").css('border-color',"#FFA500");
		$("#bg_color").val("ADD8E6").css('border-color',"#ADD8E6");
		$("#text_color").val("000000").css('border-color',"#000000");
		$("#about_color").val("0E76DC").css('border-color',"#0E76DC");
	}
	else if (mode == "night"){
		$("#star_color").val("0056ab").css('border-color',"#0056ab");
		$("#bg_color").val("00023b").css('border-color',"#00023b");
		$("#text_color").val("ffffff").css('border-color',"#ffffff");
		$("#about_color").val("94d2ff").css('border-color',"#94d2ff");
	}
}

// 이모티콘 클릭시
function ouavatar_char_click(me, id){
	$("#"+id).val($("#"+id).val()+$(me).attr("char"))
}
function charpad_change(tmp_class, id){
	$('[class^=ouavatar_charpad]').hide(); 
	$(tmp_class).show(); 
	$('#'+id).focus(); 
	$('#ouavatar_char_'+id).show(); 
}
	
function charpad_set(id){
	$.get(current_path+"/lib/php/ouavatar_charpad.php", { id:id } )
	.done(function( data ) {
		$("#ouavatar_char_"+id).html(data);
	});
}

function ouavatar_source_download(){
	if (confirm("개발에 참여하시겠습니까?") == true){
		$("#ouavatar_source_develop_count").html("파일이 곧 다운로드됩니다.");
		$("#ouavatar_source_develop_count").slideDown();
		
		$("#ouavatar_download_iframe").attr("src","https://dl.dropboxusercontent.com/u/209802803/ouavatar.zip");
		
		$.get(current_path + "/lib/php/ouavatar_source_download.php",function(html){
			$("#ouavatar_source_develop_count").html(html);
			$("#ouavatar_source_develop_count").slideDown();
			// 새 창을 띄우지 않고 받기 위해 iframe 사용
		})
	}
}

function quickload(){
	$("#ouavatar_quickload_info").hide();
	$("#ouavatar_quickload_info").html("로딩중입니다...");
	$("#ouavatar_quickload_info").css({"background-color":"#90EE90"});
	$("#ouavatar_quickload_info").slideDown("fast")



	$.getJSON(current_path+"/lib/php/ouavatar_preview_json.php?mn="+$("#ouavatar_quickload").val(),function(data){
		if ($.trim($("#ouavatar_quickload").val()) == ""){
			$("#ouavatar_quickload_info").html("번호를 입력하십시오.");
			$("#ouavatar_quickload_info").css({"background-color":"#FFA500"});
		} else {
			if (data['nickname'] == null){
				$("#ouavatar_quickload_info").html("아바타 정보가 저장되지 않은 회원입니다.<br>오유 게시판에서 일정 횟수 이상 조회될 시 아바타 정보가 자동 저장됩니다.<br>(자료창고, 신고 게시판은 제외)");
				$("#ouavatar_quickload_info").css({"background-color":"#FFA500"});
			} else {
				location.href=current_path+"/ouavatar_menu.php?"+data['getval'];
			}
		}
	})
}




// indexof 호환 모드 
if (!Array.indexOf) {
  Array.prototype.indexOf = function (obj, start) {
	for (var i = (start || 0); i < this.length; i++) {
	  if (this[i] == obj) {
		return i;
	  }
	}
	return -1;
  }
}

// 통계 새로고침
function ouavatar_refresh(){
	ouavatar_rank_refresh(); 
	ouavatar_statistics_refresh(); 
	ouavatar_ua_refresh("os");
	ouavatar_ua_refresh("browser");
}

function ouavatar_cookie_set(name,val,time,result_div_id){
	$.get(current_path + "/lib/php/ouavatar_setcookie.php",{ name: name, val:val, time:time },function(){
		$(result_div_id).html("<div style='padding:16px;'>옵션 설정이 완료되었습니다.</div>");
		$(result_div_id).slideDown("fast",function(){
			var timer = setTimeout(function() {
					 $(result_div_id).slideUp("fast");
					 clearTimeout(timer);
				}, 2000);
			});
	})
}
