function ouavatar_rank_refresh(){
	$("#ouavatar_rank_ajax").html('<span style="width:100%; display:inline-block; text-align:center; color:#BFBFBF;">순위 정보를 로딩중입니다...</span>');
	var id=0;
	var max_num = 0;
	var table_id = "ouavatar_rank_";
	$.getJSON(current_path+"/lib/php/ouavatar_rank_json.php?sort="+$("#rank_sort").val()+"&admin="+admin_get,function(data){
		is_bigger_than = "";
		json_data ="";
		
		
		json_data = json_data + '<table id="'+table_id+'" class="ouavatar_rank_table" >';
		json_data = json_data + '<thead>';
		json_data = json_data + '<tr style="background-color:#7BF77B; ">';
		json_data = json_data + '<td class="ouavatar_rank_td_num">순위</td>';
		json_data = json_data + '<td class="ouavatar_rank_td_nickname">이름</td>';
		json_data = json_data + '<td class="ouavatar_rank_td_hits">조회수</td>';
		json_data = json_data + '</tr></thead><tbody>';
		$.each(data, function(key,value) {
			if (this['nickname'] != "" ){
				json_data = json_data + "<tr>";
				json_data = json_data + "<td class='ouavatar_rank_td_num'>";
				json_data = json_data + '<div class="ouavatar_graph_line" id="graph_'+table_id+'_'+id+'"></div>';
				json_data = json_data + "<span style='position:relative;z-index:0;'>";
				json_data = json_data + this['rank'];
				json_data = json_data + "</span>";
				json_data = json_data + "</td>";
				json_data = json_data + "<td class='ouavatar_rank_td_nickname'>";
				json_data = json_data + "<span style='position:relative;z-index:0;'>";
				json_data = json_data + "<a href='"+todayhumor_member_page+this['member_num']+"' style='text-decoration:none;' target='_blank'>";
				json_data = json_data + "<span style='color:#FFA500'>";
				
				if (this['nick_icon'] != null){
					json_data = json_data + "<img src='"+current_path+"/icon/icon_"+this['nick_icon']+"_small.png' alt=''>";
				} else {
					json_data = json_data + "<img src='"+current_path+"/icon/icon_star_small.png' alt=''>";
				}
				
				json_data = json_data + "</span>";
				
				if (this['level'] == "???"){
					var level_color = 192-parseInt(999*2);
				} else {
					var level_color = 192-parseInt(this['level']*2);
				}
				
				if (level_color < 0){
					if (level_color < -255){
						level_color = -255;
					}
					level_color_blue = 255+level_color;

					level_color = 0;
				} else {
					level_color_blue = "255";
				}
				

				
				var level_bgcolor = "rgb("+level_color +","+ level_color +","+ level_color_blue+")";
				
				json_data = json_data + "<span class='ouavatar_rank_level' style='background-color:"+level_bgcolor+";'>Lv."+this['level']+"</span>";
				json_data = json_data + "<span style='color:#666666; font-weight:bold;'>";
				json_data = json_data + this['nickname'];
				json_data = json_data + "</span>";
				json_data = json_data + "</a>";
				json_data = json_data + "</span>";
				json_data = json_data + "</td>";
				json_data = json_data + "<td class='ouavatar_rank_td_hits'>";
				json_data = json_data + "<span style='position:relative;z-index:0;'>";
				json_data = json_data + "<span style='color:red;'>"+this['hits'];
				json_data = json_data + "</span> 회";
				json_data = json_data + '<input type="hidden" id="num_'+table_id+'_'+id+'" value="'+this['hits']+'">';
				if (max_num < parseInt(this['hits'])){			// 최대값을 구하는 함수
					max_num = parseInt(this['hits']);
				}
				json_data = json_data + "</span>";
				json_data = json_data + "</td></tr>";
				
				if (admin_get == admin_hash){	// 관리자 모드 
					
					json_data = json_data + "<tr><td colspan=3>";
					
					// 안쪽 테이블 시작
					json_data = json_data + "<table><tbody><tr><td style='width:64px;'>";
					
					json_data = json_data + "<img id='ouavatar_admin_img"+this['rank']+"' style='position:relative; width:64px; height:64px;'>";	
					
					json_data = json_data + "</td><td style='background-color:#E8F4FF;'>";
					
					json_data = json_data + "<span id='ouavatar_admin_about"+this['rank']+"' style='word-break:break-all; white-space:normal; color:#004589; '>";
					
					json_data = json_data + "</td></tr></tbody></table>";
					// 안쪽 테이블 끝
					
					json_data = json_data + "</td>";
					json_data = json_data + "</tr>";
					
					
					ouavatar_admin_get_about(this['rank'], this['member_num'],this['date'])
					
					function ouavatar_admin_get_about(rank, mn, date){
						$.getJSON(current_path+"/lib/php/ouavatar_preview_json.php?mn="+mn,function(){
							
						}).done(function(data) {
							$("#ouavatar_admin_about"+rank).html("<div style='color:#5C5C5C; margin-bottom:4px;'>꼬릿말 최초생성: "+date+"</div>"+ data.about1+"<br>"+data.about2);
							$("#ouavatar_admin_img"+rank).attr("src",current_path+"/lib/php/ouavatar_preview.php?resolution=100&"+data.getval);
						}).fail(function() {
							$("#ouavatar_admin_about"+rank).html("<span style='color:gray;'>(정보 획득 실패)</span>");
						 })
					}
				}
											
				if (this['rank'] == "6" || this['rank'] == "36" ){
					json_data = json_data + '</tbody>';
					json_data = json_data + '</table>';
					json_data = json_data + '<input type="button" onclick=\'$("#ouavatar_more_rank_table_'+this['rank']+'").slideToggle()\' value="순위 더 보기" style="width:100%; height:32px;">';
					json_data = json_data + '<div id="ouavatar_more_rank_table_'+this['rank']+'" style="display:none;">';
					json_data = json_data + '<table class="ouavatar_rank_table">';
					json_data = json_data + '<tbody>';
					is_bigger_than = this['rank'];
				}
			}
			id++;
		});
									
		json_data = json_data + '</tbody></table>';
		if(is_bigger_than == "6"){
			json_data = json_data + '</div>';
		} else if (is_bigger_than == "36"){
			json_data = json_data + '</div></div>';
		} 
									
		$("#ouavatar_rank_ajax").html(json_data);
		
		
		
		if ($.browser.msie){
			var time_out = 100;
		} else {
			var time_out = 0;
		}
		
		setTimeout(function() {
			 ouavatar_table_graph(table_id,id,max_num);
		}, time_out);

		$(window).resize(function(){
			ouavatar_table_graph(table_id,id,max_num);
		})
	})
}


// 배경 그래프
function ouavatar_table_graph(table_id,linenum,maxnum){
	// 테이블명, 테이블 ID, 줄 수, 최대 숫자
	var table_width = $("#"+table_id).outerWidth()-4;
	var td_height = $("#"+table_id).find("td").outerHeight();
			
	for (i=0; i<=linenum; i++){
		var graph_length = (parseInt($('#num_'+table_id+'_'+i).val()) / maxnum) * table_width;
		
		$('#graph_'+table_id+'_'+i).animate({
			"width":graph_length
		},table_width*3);
		$('#graph_'+table_id+'_'+i).height(td_height);
	}
}





// UA(브라우저/OS) 통계

function ouavatar_ua_refresh(type){
	if (type == "os"){
		var ouavatar_ua_div = "ouavatar_ua_os_ajax";
		var ouavatar_table_id = "ouavatar_ua_os_table";
	} else {
		var ouavatar_ua_div = "ouavatar_ua_browser_ajax";
		var ouavatar_table_id = "ouavatar_ua_browser_table";
	}
	
	$("#"+ouavatar_ua_div).html('<span style="width:100%; display:inline-block; text-align:center; color:#BFBFBF;">User-Agent 정보를 로딩중입니다...</span>');

	$.getJSON(current_path+"/lib/php/ouavatar_user_agent_json.php?type="+type,function(data){
		json_data ="";
		json_data = json_data + '<div class="ouavatar_ua_div_container">';
		if (type == "os"){
			json_data = json_data + 'OS별';
		} else {
			json_data = json_data + '브라우저별';
		}
		json_data = json_data + '</div>';
		json_data = json_data + '<table id="'+ouavatar_table_id+'" style="width:100%;">';
		json_data = json_data + '<thead>';
		var max_num = 0;
		var numbers = 0;
		var id = 0;
		
		$.each(data, function(key,value) {
			json_data = json_data + '<tr><td>';
			json_data = json_data + '<div class="ouavatar_graph_line" id="graph_'+ouavatar_table_id+'_'+id+'"></div>';
			json_data = json_data + '<span style="position:relative; z-index:2;">'+this['agent']+'</span>';
			json_data = json_data + '</td><td style="text-align:right;">';
			json_data = json_data + "<span style='position:relative;z-index:0;'>";
			json_data = json_data + "<span style='color:red;'>"+this['num'];
			json_data = json_data + "</span> 명";
			json_data = json_data + "</span>";
			json_data = json_data + '<input type="hidden" id="num_'+ouavatar_table_id+'_'+id+'" value="'+this['num']+'">';
			json_data = json_data + '</td></tr>';
			numbers = numbers + parseInt(this['num']);		// 합계를 구하는 함수
			if (max_num < parseInt(this['num'])){			// 최대값을 구하는 함수
				max_num = parseInt(this['num']);
			}
			id++;
		})

		json_data = json_data + '<tr style="background-color:#90EE90; font-weight:bold;"><td>';
		json_data = json_data + '합계';
		json_data = json_data + '</td><td style="text-align:right;">';
		json_data = json_data + "<span style='position:relative;z-index:0;'>";
		json_data = json_data + "<span style='color:red;'>"+ numbers;
		json_data = json_data + "</span> 명";
		json_data = json_data + "</span>";
		json_data = json_data + '</td></tr>';
		json_data = json_data + '</tbody>';
		json_data = json_data + '</table>';
		
		// 105 / 300 * 100
		// 105는 300의 몇퍼센트?
		
		$("#"+ouavatar_ua_div).html(json_data);
		
		
		// 여기서부터 그래프
		if ($.browser.msie){
			var time_out = 100;
		} else {
			var time_out = 0;
		}
		
		setTimeout(function() {
			 ouavatar_table_graph(ouavatar_table_id,id,max_num);
		}, time_out);
		
		$(window).resize(function(){
			ouavatar_table_graph(ouavatar_table_id,id,max_num);
		})
		$("#ouavatar_ua_show_button").click(function(){
			ouavatar_table_graph(ouavatar_table_id,id,max_num);
		})
		
	})
}



