


function ouavatar_random_thumbnail_refresh(){
	$.ajaxSetup({ cache: false });
	$("#ouavatar_random_thumbnail_img").attr({"src":""});
	$("#m_ouavatar_random_thumbnail_img").attr({"src":""});
	$("#ouavatar_random_thumbnail_img").hide()
	$("#m_ouavatar_random_thumbnail_img").hide()
	$.getJSON(current_path+"/lib/php/ouavatar_preview_json.php",function(data){
		if (data['nickname'] == null){
			$("#ouavatar_random_thumbnail_container").hide();
			$("#m_ouavatar_random_thumbnail_container").hide();
		} else {

				$("#ouavatar_random_thumbnail_container").show();
				$("#m_ouavatar_random_thumbnail_container").show();
				
				$("#ouavatar_random_thumbnail_link").attr({"href":todayhumor_member_page+data['mn']});
				$("#m_ouavatar_random_thumbnail_link").attr({"href":todayhumor_member_page+data['mn']});
				
				$("#ouavatar_random_thumbnail_img").attr({"src":current_path+"/lib/php/ouavatar_preview.php?"+data['getval']});
				$("#m_ouavatar_random_thumbnail_img").attr({"src":current_path+"/lib/php/ouavatar_preview.php?"+data['getval']});
				
				if (data['nick_icon'] != null && data['nick_icon'] != ""){
					$("#m_ouavatar_random_thumbnail_icon").html("<img src='"+current_path+"/icon/icon_"+data['nick_icon']+"_small.png' alt=''>");
					$("#ouavatar_random_thumbnail_icon").html("<img src='"+current_path+"/icon/icon_"+data['nick_icon']+"_small.png' alt=''>");
				} else {
					$("#m_ouavatar_random_thumbnail_icon").html("<img src='"+current_path+"/icon/icon_star_small.png' alt=''>");
					$("#ouavatar_random_thumbnail_icon").html("<img src='"+current_path+"/icon/icon_star_small.png' alt=''>");
				} 
				
				$("#ouavatar_random_thumbnail_nickname").html(data['nickname']);
				$("#m_ouavatar_random_thumbnail_nickname").html(data['nickname']);
				
				$("#ouavatar_random_thumbnail_about1").html(data['about1'].replace(/\s\s/g,"&nbsp;&nbsp;"));
				$("#m_ouavatar_random_thumbnail_about1").html(data['about1'].replace(/\s\s/g,"&nbsp;&nbsp;"));
				
				$("#ouavatar_random_thumbnail_about2").html(data['about2'].replace(/\s\s/g,"&nbsp;&nbsp;"));
				$("#m_ouavatar_random_thumbnail_about2").html(data['about2'].replace(/\s\s/g,"&nbsp;&nbsp;"));
				
				$("#ouavatar_random_thumbnail_level").html("Lv. "+data['level']);
				$("#m_ouavatar_random_thumbnail_level").html("Lv. "+data['level']);
				
				$("#ouavatar_random_thumbnail_img").show()
				$("#m_ouavatar_random_thumbnail_img").show()
			

		} // else
	}) // $.getJSON
} // function ouavatar_random_thumbnail_refresh(){
