<?php




// 오징어머리
avatar_image($directory.'avatar/squid_left_'.$_GET['squid_num']."_".$_GET['squid_left_direction'].'.png',$image,$_GET['body_color'],"n");
avatar_image($directory.'avatar/squid_right_'.$_GET['squid_num']."_".$_GET['squid_right_direction'].'.png',$image,$_GET['body_color'],"n");
 
// 피부색
avatar_image($directory.'avatar/face.png',$image,$_GET['skin_color'],"n");

// 볼(blush)
avatar_image($directory.'avatar/blush_'.$_GET['blush_num'].'.png',$image,$_GET['blush_color'],"n");
 
// 눈/눈동자
avatar_image($directory.'avatar/eyes_'.$_GET['eyes_num'].'.png',$image,$_GET['eyes_color'],"n");
avatar_image($directory.'avatar/pupil_'.$_GET['eyes_num'].'.png',$image,$_GET['pupil_color'],"n");

// 눈썹
avatar_image($directory.'avatar/brow_'.$_GET['brow_num'].'.png',$image,$_GET['brow_color'],"n");
 
// 코
avatar_image($directory.'avatar/nose_'.$_GET['nose_num'].'.png',$image,$_GET['nose_color'],"n");
 
// 입, 혀, 이빨
avatar_image($directory.'avatar/mouth_'.$_GET['mouth_num'].'.png',$image,$_GET['mouth_color'],"n");
avatar_image($directory.'avatar/tongue_'.$_GET['mouth_num'].'.png',$image,$_GET['tongue_color'],"n");
avatar_image($directory.'avatar/teeth_'.$_GET['mouth_num'].'.png',$image,$_GET['teeth_color'],"n");
 
// 헤어
avatar_image($directory.'avatar/hair_'.$_GET['hair_num'].'.png',$image,$_GET['hair_color'],"n");
 
// 몸통
avatar_image($directory.'avatar/body.png',$image,$_GET['body_color'],"n");

// 몸통 포즈 
avatar_image($directory.'avatar/body_'.$_GET['body_num'].'.png',$image,$_GET['body_color'],"n");

// 몸통 그림자
avatar_image($directory.'avatar/body_shadow.png',$image,$_GET['body_color'],"shadow");
 
// 손
avatar_image($directory.'avatar/hand_'.$_GET['body_num'].'.png',$image,$_GET['skin_color'],"n");
 
// OU마크
avatar_image($directory.'avatar/oumark.png',$image,$_GET['oumark_color'],"n");

if(empty($_GET['glasses_num']) == false && $_GET['glasses_num'] != 0){
    if(empty($_GET['glasses_lens_color']))
        $_GET['glasses_lens_color'] = 'ADD8E6';
    if(empty($_GET['glasses_border_color']))
        $_GET['glasses_border_color'] = '000000';
    if(empty($_GET['glasses_opacity']))
        $_GET['glasses_opacity'] = 60;
         
    // 안경
    avatar_image($directory.'avatar/glasses_border_'.$_GET['glasses_num'].'.png',$image,$_GET['glasses_border_color'],'n');
    avatar_image($directory.'avatar/glasses_lens_'.$_GET['glasses_num'].'.png',$image,$_GET['glasses_lens_color'],'n',$_GET['glasses_opacity']);
}
 
// 하체, 발
avatar_image($directory.'avatar/bottom_'.$_GET['bottom_num'].'.png',$image,$_GET['bottom_color'],"n");
avatar_image($directory.'avatar/foot_'.$_GET['bottom_num'].'.png',$image,$_GET['skin_color'],"n");
 
// OU마크 발광
avatar_image($directory.'avatar/oumark_gloss.png',$image,$_GET['oumark_color'],"n",$_GET['oumark_gloss_opacity']);


 ?>
