<?php
// SQL 부하를 줄이기 위해 xcache 사용
if ($_GET['type'] == "os"){
	$ua_xcache_name = "ouavatar_ua_os";
} else {
	$ua_xcache_name = "ouavatar_ua_browser";
}




if(xcache_isset($ua_xcache_name)){
	$result = xcache_get($ua_xcache_name);
} else {
	include_once "ouavatar_user_agent.php";
	// OS 통계 
	$val_array = Array();
	$count_array = Array();
	$json_result = Array();
	if ($_GET['type'] == "os"){
		$ua_type = "os";
	} else {
		$ua_type = "browser";
	}
	
	$sql_array = Array();
	foreach ($GLOBALS[$ua_type.'_array'] as $regex => $value) { 
		array_push($sql_array,"count(if(ua_".$ua_type."='".$value[0]."',1,null)) as ua_".$ua_type."_".$value[0]);
	}
	$sql = "select ".implode(",",$sql_array)." from ouavatar_user_agent";
	$data = $mysqli->query($sql)->fetch_assoc();
	foreach ($GLOBALS[$ua_type.'_array'] as $regex => $value) { 
		$data_num = $data['ua_'.$ua_type.'_'.$value[0]];
		if ($data_num > 0){
			$number = sprintf("%09d", $data_num);
			array_push($val_array,$number."|".$value[1]);
		}
	}

	rsort($val_array);
	foreach ($val_array as $key => $val){
		$piece = explode("|",$val);
		
		$json = "{";
		$json .= '"agent":';
		$json .= json_encode($piece[1]);
		$json .= ',';
		$json .= '"num":';
		$json .= json_encode(intval($piece[0]), JSON_UNESCAPED_SLASHES);
		$json .= "}";
		array_push($json_result,$json);
	}

	$result = "[".implode($json_result,",")."]";
	
	//xcache_set("캐시명","내용",초);
	xcache_set($ua_xcache_name,$result,300);
}

echo $result;
?>
