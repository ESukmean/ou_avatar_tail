<?php
// 기본 언어는 한국어

$text_nickname_null = '탈퇴/가입하지 않음';
$text_joindate_null = '알 수 없음';


// 다른 언어일 시
if ($_GET['language'] == "zh_cn"){
	$text_nickname_null = '找不到会员';
	$text_joindate_null = '未知';		
} else if ($_GET['language'] == "zh_tw"){
	$text_nickname_null = '找不到會員';
	$text_joindate_null = '未知';			
} else if ($_GET['language'] == "ja_jp"){
	$text_nickname_null = '会員情報なし';
	$text_joindate_null = '未知';		
} else if ($_GET['language'] == "en_us" || $_GET['language'] == "en_uk"){
	$text_nickname_null = 'Unknown';
	$text_joindate_null = 'Null';	
} else if ($_GET['language'] == "es_es"){
	$text_nickname_null = 'Desconocido';
	$text_joindate_null = 'Desconocido';	
} else if ($_GET['language'] == "eo_eo"){
	$text_nickname_null = 'Nekonato';
	$text_joindate_null = 'Nenio';	
} else if ($_GET['language'] == "ru_ru"){
	$text_nickname_null = 'Неизвестный Член';
	$text_joindate_null = 'Неизвестный';	
} else if ($_GET['language'] == "fr_fr"){
	$text_nickname_null = 'Nullité';
	$text_joindate_null = 'Nullité';	
}

// 월을 각국의 언어로 번역
function month_lang($obj){

	if ($_GET['language'] == "es_es"){
	$obj = str_replace("January","Enero",$obj);
	$obj = str_replace("February","Febrero",$obj);
	$obj = str_replace("March","Marzo",$obj);
	$obj = str_replace("April","Abril",$obj);
	$obj = str_replace("May","Mayo",$obj);
	$obj = str_replace("June","Junio",$obj);
	$obj = str_replace("July","Julio",$obj);
	$obj = str_replace("August","Agosto",$obj);
	$obj = str_replace("September","Septiembre",$obj);
	$obj = str_replace("October","Octubre",$obj);
	$obj = str_replace("November","Noviembre",$obj);
	$obj = str_replace("December","Diciembre",$obj);
	}
	else if ($_GET['language'] == "eo_eo"){
	$obj = str_replace("January","Januaro",$obj);
	$obj = str_replace("February","Februaro",$obj);
	$obj = str_replace("March","Marto",$obj);
	$obj = str_replace("April","Aprilo",$obj);
	$obj = str_replace("May","Majo",$obj);
	$obj = str_replace("June","Junio",$obj);
	$obj = str_replace("July","Julio",$obj);
	$obj = str_replace("August","Aŭgusto",$obj);
	$obj = str_replace("September","Septembro",$obj);
	$obj = str_replace("October","Oktobro",$obj);
	$obj = str_replace("November","Novembro",$obj);
	$obj = str_replace("December","Decembro",$obj);
	}
	else if ($_GET['language'] == "ru_ru"){
	$obj = str_replace("January","января",$obj);
	$obj = str_replace("February","февраля",$obj);
	$obj = str_replace("March","марта",$obj);
	$obj = str_replace("April","апреля",$obj);
	$obj = str_replace("May","мая",$obj);
	$obj = str_replace("June","июня",$obj);
	$obj = str_replace("July","июля",$obj);
	$obj = str_replace("August","августа",$obj);
	$obj = str_replace("September","сентября",$obj);
	$obj = str_replace("October","октября",$obj);
	$obj = str_replace("November","ноября",$obj);
	$obj = str_replace("December","декабря",$obj);
	}

	else if ($_GET['language'] == "fr_fr"){
	$obj = str_replace("January","janvier",$obj);
	$obj = str_replace("February","février",$obj);
	$obj = str_replace("March","mars",$obj);
	$obj = str_replace("April","avril",$obj);
	$obj = str_replace("May","mai",$obj);
	$obj = str_replace("June","juin",$obj);
	$obj = str_replace("July","juillet",$obj);
	$obj = str_replace("August","août",$obj);
	$obj = str_replace("September","septembre",$obj);
	$obj = str_replace("October","octobre",$obj);
	$obj = str_replace("November","novembre",$obj);
	$obj = str_replace("December","décembre",$obj);
	}

	return $obj;
}

?>
