<!-- 
용량 축소를 위한 주석, 개행, 공백 제거 기능이 작동하고 있으므로 
Script 삽입 시에는 반드시 끝에 ;를 붙여 주십시오.
-->


	<!-- 에러 알림 -->
	<div id="js_errordiv" style="display:block; padding:8px; font-size:9pt; background-color:brown; color:white; font-weight:bold; text-align:center; display:none;">
		<div style="text-align:center;">
			<div style="max-width:800px; font-size:9pt;">
			
			현재 서버에 문제가 있습니다. 꼬릿말 서비스가 제대로 작동하지 않을 수 있습니다.
			</div>
		</div>
	</div>
	<!-- 구 브라우저 전용
	IE8보다 낮을 때 알림
	 -->
	<!--[if lt IE 7]>
		<div style="display:block; padding:8px; font-size:9pt; background-color:brown; color:white; font-weight:bold; text-align:center; ">
			<div style="text-align:center;">
			<div style="max-width:800px; font-size:9pt;">현재 구버전의 브라우저 또는 OS를 사용 중입니다. 일부 기능이 작동하지 않을 수 있습니다.</div>
			</div>
		</div>
	<![endif]-->
	
		<div class="ouavatar_logo_line_container">
			<div class="ouavatar_logo_line">
			<a href='<?php echo $_SERVER['PHP_SELF'] ?>'><img src='<?php echo $current_path ?>/lib/webimages/ouavatar_toplogo.gif' alt='toplogo' id="ouavatar_toplogo_img" style='float:left;height:33px; border:0;'/></a>
			
			<a href='<?php echo $todayhumor_home; ?>'>
				<div id="ouavatar_home_button">홈으로</div>
			</a>
			</div>
		</div>

		<iframe id="ouavatar_download_iframe" style="display:none;"></iframe>
        <a href="javascript:ouavatar_source_download()"  style="text-decoration:none; " >
            <div style="width:100%; background-color:#1E90FF; padding-top:16px; padding-bottom:16px; color:white; font-weight:bold; text-align:center; font-size:12pt;" >여러분의 개발참여가 더 나은 꼬릿말 서비스를 만들어 갑니다. [소스 다운로드]<br>
            <span id='ouavatar_source_develop_count' style="font-color:white; font-size:9pt;display:none;"></span>
            </div>
            
        </a>
        <a href="http://todayhumor.com/?bestofbest_155685" target="_blank" style="text-decoration:none; ">
            <div style="width:100%; background-color:#34cd00; padding-top:16px; padding-bottom:16px; color:white; font-weight:bold; text-align:center; font-size:12pt;" >꼬릿말 사용방법 및 건의 페이지 (클릭시 이동)</div>
        </a>
		<div class="ouavatar_menu_whole_box">	
			<div class="top_shadow_container"></div>			
			<div class="ouavatar_menu_vertical_container">	

				
				<!-- ActOU 페이지 이동 -->
				
					<?php 
						if ($ismobile == false){
							echo '<a href="http://todayhumor.com/?databox_24555" target="_blank" style="text-decoration:none; font-weight:bold;"><div class="ouavatar_ad_banner"> <img src="'.$current_path.'/lib/webimages/chrome_icon.png" alt="" style="vertical-align:middle; margin-right:4px;">ActOU Google Chrome 확장 프로그램으로 개인페이지와 댓글에 아바타를 연동해 보세요!<br>소스가 공개되어 누구나 개발에 참여하실 수 있습니다!</div></a>';
						}
					?>

<a href="javascript:ouavatar_api_help_slidetoggle()" style="text-decoration:none; font-weight:bold;">
	<div style="margin:auto; font-size:12pt; text-align:center; margin-top:0px;  background-color:#FF6F00; color:white; width:100%; padding-top:8px; padding-bottom:8px;">오유앱 개발자들을 위한 아바타 연동 API를 제공합니다. [클릭]
	</div>
</a>
<script>
	function ouavatar_api_help_slidetoggle(){
		$('#ouavatar_api_help').slideToggle();
	}
</script>

<div id="ouavatar_api_help" style="background-color:#FFA55F; font-size:10pt; padding:4px; display:none;">
	1. 주어진 JSON API를 보시면,
	<div class="ouavatar_api_help_code">
		<a href="<?php echo $current_path ?>/lib/php/ouavatar_preview_json.php" target="_blank">
		<?php echo $current_path ?>/lib/php/ouavatar_preview_json.php?mn=<span style="color:red">회원번호</span>
		</a><br>
		회원번호가 없을 경우 무작위의 회원이 선택됩니다.
	</div>
	2. 다음 결과가 나옵니다.
	<div class="ouavatar_api_help_code">
	{<br>
	&emsp;"mn":<span class="ouavatar_api_help_json_code">"회원번호"</span>, <br>
	&emsp;"nickname":<span class="ouavatar_api_help_json_code">"닉네임"</span>, <br>
	&emsp;"about1":<span class="ouavatar_api_help_json_code">"자기소개 첫번째줄"</span>, <br>
	&emsp;"about2":<span class="ouavatar_api_help_json_code">"자기소개 두번째줄"</span>, <br>
	&emsp;"getval":<span class="ouavatar_api_help_json_code">"GET 값(캐릭터 형태)"</span>, <br>
	&emsp;"level":<span class="ouavatar_api_help_json_code">"레벨"</span>, <br>
	&emsp;"nick_icon":<span class="ouavatar_api_help_json_code">"닉 아이콘"</span>, <br>
	&emsp;"hyperlink":<span class="ouavatar_api_help_json_code">"하이퍼링크"</span>, <br>
	&emsp;"title":<span class="ouavatar_api_help_json_code">"하이퍼링크 제목"</span><br>
	}
	</div>
	3. 각 프로그래밍 언어에 맞게 파싱하여 API를 사용하시면 됩니다.
	<div class="ouavatar_api_help_code">
	만약 아바타 이미지를 불러온다면:<br>
		<a href="<?php echo $current_path ?>/lib/php/ouavatar_preview.php?iscustom=true&amp;background_color=ADD8E6&amp;text_color=000000&amp;about_color=0E76DC&amp;body_num=1&amp;body_color=A52A2A&amp;oumark_color=FF8C00&amp;oumark_gloss_opacity=30&amp;squid_num=1&amp;squid_left_direction=down&amp;squid_right_direction=down&amp;skin_color=ffb273&amp;hair_num=1&amp;hair_color=450000&amp;blush_num=1&amp;blush_color=ff8973&amp;brow_num=1&amp;brow_color=450000&amp;eyes_num=1&amp;eyes_color=FFFFFF&amp;pupil_color=450000&amp;glasses_num=0&amp;glasses_border_color=000000&amp;glasses_lens_color=ADD8E6&amp;glasses_opacity=50&amp;nose_num=1&amp;nose_color=450000&amp;mouth_num=1&amp;mouth_color=7B220B&amp;tongue_color=FF6B70&amp;teeth_color=FFFFFF&amp;bottom_color=FF6B70&amp;bottom_num=1" target="_blank">
		<?php echo $current_path ?>/lib/php/ouavatar_preview.php?resolution=<span style="color:blue;">[크기]</span>&amp;<span style="color:red">[파싱된 getval 값]</span>
		</a><br>
		아바타 이미지의 크기는 정사각형 픽셀 16~200 중에서 선택 가능합니다.<br>
		resolution 값이 없거나 틀린 값을 입력할 경우 기본값인 200으로 설정됩니다.
		
	</div>
</div>
				<!-- ActOU 페이지 이동 끝 -->
				<div class="ouavatar_viewSubjectDiv">
					<div class="ouavatar_viewTitleDiv">
					오늘의유머 아바타 꼬릿말 생성 프로그램
					</div>
				</div>
				<fieldset>
					<legend>통계 및 순위 
						<input type="button" id="ouavatar_rank_refresh_button" onclick="ouavatar_refresh();" value="새로고침">
						<input type="button" onclick="$('#ouavatar_statistics_help').slideToggle()" value="?">
					</legend>
					<!-- 트래픽을 분산시키기 위해 ajax를 사용해야 한다. -->
					
					
					<div id="ouavatar_statistics_help" style="font-size:10pt; margin:4px; display:none;">
					통계 및 오유앱 연동을 위하여 다음의 정보가 수집됩니다.<br>
					- 회원번호 및 닉네임<br>
					- 가입날짜<br>
					- 글, 댓글, 베스트, 베오베 갯수<br>
					- 아바타 정보 및 자기소개<br>
					- 조회수<br>
					- OS 및 브라우저 정보(정확한 브라우저명을 알지 못 할 때만 전체 User-agent 수집)<br>
					<br>
					<a href="http://todayhumor.com/?ou_1978" target="_blank">오유에서 공식 제공한 API</a> 내의 정보만 수집되며, 아이디, 이메일, 비밀번호 등의 개인정보는 절대 수집되지 않습니다.<br>
					오유 도메인 내에서만 정보가 수집되며, 테스트(자료창고) 및 신고 게시판에서는 정보가 수집되지 않습니다.

					</div>
					
					
					<div id="ouavatar_statistics_ajax" class="ouavatar_half_span">
						
					</div>
					<div id="ouavatar_rank_span" class="ouavatar_half_span">
						
						<div style="display:block;">
							<div style="display:block; padding-top:2px; padding-bottom:2px; background-color:#00A500; color:white; text-align:center; font-weight:bold;">
								순위: <select id="rank_sort" onchange="ouavatar_rank_refresh()">
									<option value="view_desc">조회수</option>
									<option value="view_asc">조회수 역순</option>
									<option value="level_desc">레벨</option>
									<option value="level_asc">레벨 역순</option>
									<option value="date_desc">가입날짜</option>
									<option value="date_asc">가입날짜 역순</option>
								</select>
							</div>
						</div>
						
						<div id="ouavatar_rank_ajax" style="display:block;">
						</div>
					</div>
					<input type="button" style="width:100%; height:32px;" id="ouavatar_ua_show_button" onclick="$('#ouavatar_ua_rank_container').slideToggle()" value="OS 및 브라우저별 사용자 보기">
					
					
					<div id="ouavatar_ua_rank_container" style="display:none;">
					<div style='padding:4px;'>
					<?php
					include_once "lib/php/ouavatar_user_agent.php";
					?>
					부하를 최소화하기 위하여 60분 주기로 수집, 5분 주기로 업데이트됩니다.<br>
					잘못 적혀 있는 경우 캡쳐 또는 복사 후 알려주세요.<br>
					User-Agent :  <span style='color:red;'><?php echo $_SERVER['HTTP_USER_AGENT']; ?></span><br>
					브라우저 : <span style='color:red;'><?php echo $GLOBALS['user_browser_name']; ?></span><br>
					OS :  <span style='color:red;'><?php echo $GLOBALS['user_os_name']; ?></span>
					
					</div>
						<div id="ouavatar_ua_os_ajax" class="ouavatar_half_span">
						</div>
						<div id="ouavatar_ua_browser_ajax" class="ouavatar_half_span">
						</div>

						
					</div>
					
				</fieldset>



				
				<fieldset>
					
					<legend>도움주신 분들 <input type="button" id="ouavatar_random_thumbnail_refresh_button" onclick="ouavatar_random_thumbnail_refresh()" value="새로고침"></legend>
					
					<!-- 모바일 -->
					<div id="m_ouavatar_random_thumbnail_container">
						<a id="m_ouavatar_random_thumbnail_link" style="text-decoration:none; color:black;" href="" target="_blank">
							<div id="m_ouavatar_random_thumbnail">
								<table style="width:100%;"><tbody>
									<tr>
										<td style="width:104px;">
											<span id="m_ouavatar_random_thumbnail_load">
											<img style="vertical-align:middle;" src="<?php echo $current_path ?>/lib/webimages/loadicon.gif" alt="">
											<!-- 로딩 이미지 -->
											</span>
											<img src="<?php echo $current_path ?>/lib/webimages/loadicon.gif" id="m_ouavatar_random_thumbnail_img" alt="">
										</td>
										<td style="vertical-align:top;">
											<span id="m_ouavatar_random_thumbnail_menu" style="padding:4px;">
												<span style="font-weight:bold;"><span id="m_ouavatar_random_thumbnail_icon" style="color:#FFA500; font-size:12pt;">★</span><span id="m_ouavatar_random_thumbnail_nickname" style="font-size:12pt; "></span></span>
												<span id="m_ouavatar_random_thumbnail_level"></span>
												<br><span id="m_ouavatar_random_thumbnail_about1" style="font-size:10pt;"></span>
												<br><span id="m_ouavatar_random_thumbnail_about2" style="font-size:10pt;"></span>
											</span>
										</td>
									</tr>
								</tbody></table>
							</div>
						</a>
					</div>
						
					<!-- PC -->
					<span id="ouavatar_random_thumbnail_container">
						<a id="ouavatar_random_thumbnail_link" href="" target="_blank">
							<span id="ouavatar_random_thumbnail">
								<span id="ouavatar_random_thumbnail_load">
								<img style="vertical-align:middle;" src="<?php echo $current_path ?>/lib/webimages/loadicon.gif" alt="">
								<!-- 로딩 이미지 -->
								</span>
								<img src="<?php echo $current_path ?>/lib/webimages/loadicon.gif" id="ouavatar_random_thumbnail_img" alt="">
								<span id="ouavatar_random_thumbnail_menu">
									<span style="font-weight:bold;"><span id="ouavatar_random_thumbnail_icon" style="color:#FFA500">★</span><span id="ouavatar_random_thumbnail_nickname"></span></span>
									<span id="ouavatar_random_thumbnail_level"></span>
									
									<br><span id="ouavatar_random_thumbnail_about1"></span>
									<br><span id="ouavatar_random_thumbnail_about2"></span>
								</span>
							</span>
						</a>
					</span>

					<script>				

					//mouseover를 사용하면 깜박임 버그가 있으므로 hover 사용
					$("#ouavatar_random_thumbnail").hover(function(){
						$('#ouavatar_random_thumbnail_menu').slideDown();
					}, function(){
						$('#ouavatar_random_thumbnail_menu').slideUp();
					});

					
					</script>
					

					<div class="ouavatar_credit_title">프로그래밍</div>
					<div class="ouavatar_credit_desc">ActiveXXX</div>
					<br>
					<div class="ouavatar_credit_title">캐릭터 디자인</div>
					<div class="ouavatar_credit_desc">i로보타 <a href="http://todayhumor.com/?humorbest_778763" target="_blank">(오유벼룩시장 캐릭터 일러스트)</a></div>
					<br>
					<div class="ouavatar_credit_title">주의사항</div>
					<div class="ouavatar_credit_desc">
					본 꼬릿말 서비스 제작자는 오유 운영팀과 관련이 없습니다.<br>
					이 프로그램 소스 및 캐릭터 디자인은 오유 외에서는 절대 사용하실 수 없으며 오유 내에서만 자유롭게 수정 재업이 가능합니다. 단, 수정 소스도 소스코드를 알기 쉽게 하여 공개해야 합니다.<br>
					<span style="font-weight:bold;">혹시 오유가 아닌 외부 사이트에서 해당 캐릭터 및 디자인이 악용되어 사용되는 걸 보시면 신고 ( <a href="mailto:irobota@daum.net">irobota@daum.net</a> / <a href="mailto:oumarket.seoul@gmail.com">oumarket.seoul@gmail.com</a> )부탁드리겠습니다~~!</span>
					</div>

				</fieldset>
				
				<fieldset>
					<legend>레벨 계산법</legend>
				레벨 = ((방문횟수*1) + (댓글수*2) + (전체글*3) + (베스트*5) + (베오베*10)) / 200 <br>
				오유 공식 레벨 계산법이 아니며, 다른 꼬릿말 서비스와 다를 수 있습니다.<br>
				레벨 계산법의 변경을 원할 시, <a href="http://todayhumor.com/?bestofbest_155685" target="_blank">건의 페이지</a>에서 건의 바랍니다.
				</fieldset>
				
				<fieldset>
					<legend>개발자용 옵션</legend>
					개발자 전용 옵션입니다. 일반 사용자는 체크하지 않으셔도 사용에 지장이 없습니다.
					<br>
					설정이 되지 않을 시 브라우저 캐시를 삭제해 주시기 바랍니다.(쿠키는 삭제하지 마세요)
					<br>
					<label>
					<input type="checkbox" name="debugmode" id="debugmode" value="true"
					<?php
						if ($_COOKIE["ouavatar_debug"] == "true"){
							echo "checked";
						}
					?>
					
					onclick="ouavatar_cookie_set('ouavatar_debug',$('#debugmode').is(':checked'),0,'#ouavatar_cookie_set_div')"
					
					 >디버그 화면 표시</label><br>
					 

					 
					<div id="ouavatar_cookie_set_div" style="text-align:center; display:none; background-color:#00FF00;"></div>
				</fieldset>
				
				<fieldset>
					<legend>아바타 설정 불러오기</legend>
					
				<!-- 즉시 불러오기 -->
				<form method="get" action="javascript:quickload()">
					<table style="width:100%;">
						<tbody>
							<tr>
								<td>
									회원번호로 즉시 불러오기(활성 사용자만):
								</td>
								<td>
									<input type="text" id="ouavatar_quickload" class="normal_text">
									</td>
									<td>
									<input type="submit" value="불러오기" style="width:100%; height:24px;">
								</td>
							</tr>
						</tbody>
					</table>
					<div id="ouavatar_quickload_info" style="font-size:10pt; display:block; padding-top:12px; padding-bottom:12px; display:none; text-align:center;"></div>
				</form>
				
				
				<input type="button" value="URL 데이터로 아바타 설정 불러오기" style="width:100%; height:64px; font-size:12pt; font-weight:bold;" onclick="$('#ouavatar_load_help').slideToggle()">
				<div id="ouavatar_load_help" style="display:none;">
					<div style="padding:8px; background-color:#90EE90; border:radius:4px;">
						<div style="padding:4px;background-color:#008000; color:white; font-weight:bold; text-align:center;">아바타 설정을 불러오는 방법</div>
						<div style="background-color:white; padding:4px;">
						<div style="border:4px solid #ADD8E6; border-radius:4px;">
							<div style="display:block; background-color:#ADD8E6; color:black; padding:4px;">오늘의유머 소스보기</div>
							<div style="padding:4px;">
							&lt;a href="<?php echo $current_path ?>/ouavatar_menu.php" target="_blank"&gt;&lt;img src="<?php echo $current_path ?>/ouavatar.php?<span style="background-color:#1E90FF; color:white; font-weight:bold;">start=true&amp; <span style="color:silver">( 중략 )</span> &amp;end=true</span>&amp;isold&amp;.png" alt="ouavatar.php?start=true&amp;text_direction=l" /&gt;&lt;/a&gt;
							</div>
						</div>
						</div>
					
					<input type="button" value="꼬릿말 소스보기">를 클릭하여 창을 열어 보이는 내용 중 start=true부터 시작하여 end=true까지 복사 후 밑의 상자에 붙여넣습니다.</div>
				

					<textarea id="load_avatar" style="width:100%;" rows=9></textarea><br>
					<input type="button" value="불러오기" onclick="avatar_load()" style="width:49%; height:32px;">
					<input type="button" value="지우기" onclick="$('#load_avatar').val('')" style="width:49%; height:32px;">
				</div><!-- id="ouavatar_load_help" -->
				</fieldset>
				<a href="new_ouavatar_menu.php">
					<button style="height:50px; width:100%;background:#5BC0DE;border-color:#46B8DA">베타 버전 아바타 메이커 가기</button>
				</a>
				<form method="get" id="ouavatar_form" action="javascript:ouavatar()">
					<input type="hidden" name="start" value="true">
						<fieldset>
							<legend>회원번호/자기소개</legend>
								<table><tbody>
									<tr>
										<td>
											언어 / Language
										</td>
										<td>
											<select name="language" id="language" style="width:100%; height:24px;">
												<option value="ko_kr">한국어</option>
												<option value="en_us">English(US)</option>
												<option value="en_uk">English(UK)</option>
												<option value="es_es">Español</option>
												<option value="eo_eo">Esperanto</option>
												<option value="fr_fr">Français</option>
												<option value="ja_jp">日本語</option>
												<option value="zh_cn">简体中文</option>
												<option value="zh_tw">繁體中文</option>
												<option value="ru_ru">Русский</option>
											</select><br>
											회원 활동정보에 표시될 언어입니다.<br>
											닉네임/자기소개에는 영향을 미치지 않습니다.
										</td>
									</tr>
									<script>
										// GET값에서 언어 가져오기
										$("#language").val("<?php echo $_GET['language'] ?>");
									</script>
									<tr>
										<td>텍스트 정렬</td>
										<td><label><input type="radio" name="text_direction" value="ltr"
										<?php 
										if ($_GET['text_direction'] != "rtl")
										echo "checked";
										?>
										>왼쪽</label>
										<label><input type="radio" name="text_direction" value="rtl"
										<?php 
										if ($_GET['text_direction'] == "rtl")
										echo "checked";
										?>
										>오른쪽</label>
										</td>
									</tr>
									<tr>
										<td>
											회원번호 (자신의 개인페이지에서 나오는 번호)<br>
											http://www.todayhumor.co.kr/board/list.php?kind=member&amp;mn=<span style="color:red">숫자</span>
										</td>
										<td>
											<input type="text" class="normal_text" name="mn" id="mn" value="<?php echo $_GET['mn'] ?>">
										</td>
									</tr>
									<tr>
										<td>
											자기소개 첫번째 줄
										</td>	
										<td>
											<input onfocus="$('#ouavatar_char_about1').fadeIn(100)" onblur="time_about1 = setTimeout(function(){ $('#ouavatar_char_about1').fadeOut(100) },100)" id="about1" type="text" class="normal_text" name="about1" size=70 value="<?php echo $_GET['about1'] ?>">
											<div class="ouavatar_preview" id="ouavatar_char_about1">
											
											</div>

										</td>
									</tr>
									<tr>
										<td>
											자기소개 두번째 줄
										</td>
										<td>
											<input onfocus="$('#ouavatar_char_about2').fadeIn(100)" onblur="time_about2 = setTimeout(function(){ $('#ouavatar_char_about2').fadeOut(100) },100)" id="about2" type="text" class="normal_text" name="about2" size=70 value="<?php echo $_GET['about2'] ?>">
											<div class="ouavatar_preview" id="ouavatar_char_about2">
											
											</div>
										</td>
									</tr>
								</tbody></table>
						</fieldset>

						<fieldset>
							<legend>글자/배경 색상</legend>
							<input type="button" value="낮" onclick="ouavatar_preset('day')" style="width:49%; height:32px;">
							<input type="button" value="밤" onclick="ouavatar_preset('night')" style="width:49%; height:32px;">
							<br>
							<div style="color:red">클릭시 팔레트가 나옵니다.<br> 나오지 않을 시 다른 브라우저를 이용하십시오.</div>
							
							<table><tbody>
								<tr>
									<td>아이콘</td>
									<td><label><input type="radio" name="nick_icon" value="star"
									<?php 
									if ($_GET['nick_icon'] == "star")
									echo "checked";
									?>
									><span style="color:#FFA500;">★</span></label>
									<label><input type="radio" name="nick_icon" value="ribbon"
									<?php 
									if ($_GET['nick_icon'] == "ribbon")
									echo "checked";
									?>
									><img src="<?php echo $current_path ?>/icon/icon_ribbon_small.png" alt=""></label><label><input type="radio" name="nick_icon" value="heart"
									<?php 
									if ($_GET['nick_icon'] == "heart")
									echo "checked";
									?>
									><span style="color:#FFA500;">♥</span></label>
									</td>
								</tr>
								<tr>
									<td>
										아이콘색상
									</td>
									<td>
										<input type="text" id="star_color" class="picker" name="star_color" value="<?php echo $_GET['star_color'] ?>">
									</td>
								</tr>
								<tr>
									<td>
										배경색상
									</td>
									<td>
										<input type="text" id="bg_color" class="picker" name="background_color" value="<?php echo $_GET['background_color'] ?>">
									</td>
								</tr>
								<tr>
									<td>
										기본 글자색상
									</td>
									<td>
										<input type="text" id="text_color" class="picker" name="text_color" value="<?php echo $_GET['text_color'] ?>">
									</td>
								</tr>
								<tr>
									<td>
										자기소개 글자색상
									</td>
									<td>
									<input type="text" id="about_color" class="picker" name="about_color" value="<?php echo $_GET['about_color'] ?>">
									</td>
								</tr>
							</tbody></table>
							 
						</fieldset>
						<fieldset>
							<legend>아바타 머리/몸통</legend>
							<table><tbody>
								<tr>
									<td>포즈</td>
									<td><input type="text" class="normal_text" id="body_num" name="body_num" value="<?php 
									 
									if ($_GET['body_num'] == "")
									echo "1";
									else
									echo $_GET['body_num'];
									 
									?>" onfocus="$('#body_preview').fadeIn()" onblur="$('#body_preview').fadeOut()">
		 
									<?php echo avatar_preview("body",$max_body_num); ?>
									 
									</td>
								</tr>
								<tr>
									<td>상의 색상</td>
									<td><input type="text" id="body_color" class="picker" name="body_color" value="<?php echo $_GET['body_color'] ?>"></td>
								</tr>
								<tr>
									<td>OU마크 색상</td>
									<td><input type="text" id="oumark_color" class="picker" name="oumark_color" value="<?php echo $_GET['oumark_color'] ?>"></td>
								</tr>
								<tr>
									<td>OU마크 발광 강도(0~100)</td>
									<td class="td_boundary"><input type="text" class="normal_text" id="oumark_gloss_opacity" name="oumark_gloss_opacity" value="<?php 
										 
										if ($_GET['oumark_gloss_opacity'] == "")
										echo "0";
										else
										echo $_GET['oumark_gloss_opacity'] 
										 
										?>" onfocus="$('#oumark_preview').fadeIn()" onblur="$('#oumark_preview').fadeOut()">
		 
									<?php echo avatar_preview("oumark",100,true); ?>
									
									</td>
								</tr>
								<tr>
									<td>오징어머리</td>
									<td><input type="text" class="normal_text" id="squid_num" name="squid_num" value="<?php 
									 
									if ($_GET['squid_num'] == "")
									echo "1";
									else
									echo $_GET['squid_num'];
									 
									?>" onfocus="$('#squid_preview').fadeIn()" onblur="$('#squid_preview').fadeOut()">
									<?php echo avatar_preview("squid",$max_squid_num); ?>
									</td>
								</tr>
								<tr>
									<td>왼쪽 오징어머리 방향</td>
									<td><label><input type="radio" name="squid_left_direction" value="up"
									<?php 
									if ($_GET['squid_left_direction'] == "up")
									echo "checked";
									?>
									>올림</label>
									<label><input type="radio" name="squid_left_direction" value="down"
									<?php 
									if ($_GET['squid_left_direction'] != "up")
									echo "checked";
									?>
									>내림</label></td>
								</tr>
								<tr>
									<td>오른쪽 오징어머리 방향</td>
									<td><label><input type="radio" name="squid_right_direction" value="up"
									<?php 
									if ($_GET['squid_right_direction'] == "up")
									echo "checked";
									?>
									>올림</label>
									<label><input type="radio" name="squid_right_direction" value="down"
									<?php 
									if ($_GET['squid_right_direction'] != "up")
									echo "checked";
									?>
									>내림</label></td>
								</tr>
							</tbody></table>
						</fieldset>
						<fieldset>
							<legend>아바타 얼굴</legend>
							<table><tbody>
									<tr>
										<td class="td_boundary">피부색</td>
										<td class="td_boundary"><input type="text" id="skin_color" class="picker" name="skin_color" value="<?php echo $_GET['skin_color'] ?>"></td>
									</tr>
									<tr>
										<td>헤어스타일</td>
										<td><input type="text" class="normal_text" id="hair_num" name="hair_num" value="<?php 
										if ($_GET['hair_num'] == "")
										echo "1";
										else
										echo $_GET['hair_num']
										 
										?>" onfocus="$('#hair_preview').fadeIn()" onblur="$('#hair_preview').fadeOut()">
										 
										<?php echo avatar_preview("hair",$max_hair_num); ?>
										</td>
									</tr>
									<tr>
										<td class="td_boundary">헤어 색상</td>
										<td class="td_boundary"><input type="text" id="hair_color" class="picker" name="hair_color" value="<?php echo $_GET['hair_color'] ?>"></td>
									</tr>
									<tr>
										<td>볼(blush)</td>
										<td><input type="text" class="normal_text" id="blush_num" name="blush_num" value="<?php 
										 
										if ($_GET['blush_num'] == "")
										echo "1";
										else
										echo $_GET['blush_num'] 
										 
										?>" onfocus="$('#blush_preview').fadeIn()" onblur="$('#blush_preview').fadeOut()">
										 
										<?php echo avatar_preview("blush",$max_blush_num);  ?>
										</td>
									</tr>
									<tr>
										<td class="td_boundary">볼 색상</td>
										<td class="td_boundary"><input type="text" id="blush_color" class="picker" name="blush_color" value="<?php echo $_GET['blush_color'] ?>"></td>
									</tr>
									<tr>
										<td>눈썹</td>
										<td><input type="text" class="normal_text" id="brow_num" name="brow_num" value="<?php 
										 
										if ($_GET['brow_num'] == "")
										echo "1";
										else
										echo $_GET['brow_num'] 
										 
										?>" onfocus="$('#brow_preview').fadeIn()" onblur="$('#brow_preview').fadeOut()">
										 
										<?php echo avatar_preview("brow",$max_brow_num);  ?>
										</td>
									</tr>
									<tr>
										<td class="td_boundary">눈썹 색상</td>
										<td class="td_boundary"><input type="text" id="brow_color" class="picker" name="brow_color" value="<?php echo $_GET['brow_color'] ?>"></td>
									</tr>
									<tr>
										<td>눈/눈동자</td>
										<td><input type="text" class="normal_text" id="eyes_num" name="eyes_num" value="<?php 
										 
										if ($_GET['eyes_num'] == "")
										echo "1";
										else
										echo $_GET['eyes_num'] 
										 
										?>" onfocus="$('#eyes_preview').fadeIn()" onblur="$('#eyes_preview').fadeOut()">
										<?php echo avatar_preview("eyes",$max_eyes_num);  ?>
										</td>
									</tr>
									<tr>
										<td>눈 색상</td>
										<td><input type="text" id="eyes_color" class="picker" name="eyes_color" value="<?php echo $_GET['eyes_color'] ?>"></td>
									</tr>
									<tr>
										<td class="td_boundary">눈동자 색상</td>
										<td class="td_boundary"><input type="text" id="pupil_color" class="picker" name="pupil_color" value="<?php echo $_GET['pupil_color'] ?>"></td>
									</tr>
									<tr>
										<td>안경 <input type="button" value="없을시 0" onclick="$('#glasses_num').val('0')"></td>
										<td><input type="text" class="normal_text" id="glasses_num" name="glasses_num" value="<?php 
										 
										if ($_GET['glasses_num'] == "" || isset($_GET['glasses_num']) == false)
										echo "0";
										else
										echo $_GET['glasses_num'] 
										 
										?>" onfocus="$('#glasses_preview').fadeIn()" onblur="$('#glasses_preview').fadeOut()">
										<?php echo avatar_preview("glasses",$max_glasses_num);  ?>
										</td>
									</tr>
									<tr>
										<td>안경 테 색상</td>
										<td><input type="text" id="glasses_border_color" class="picker" name="glasses_border_color" value="<?php echo $_GET['glasses_border_color'] ?>"></td>
									</tr>
									<tr>
										<td>안경 렌즈 색상</td>
										<td><input type="text" id="glasses_lens_color" class="picker" name="glasses_lens_color" value="<?php echo $_GET['glasses_lens_color'] ?>"></td>
									</tr>
									<tr>
										<td class="td_boundary">안경 렌즈 투명도(1 ~ 100)</td>
										<td class="td_boundary"><input type="text" class="normal_text" id="glasses_opacity" name="glasses_opacity" value="<?php 
										 
										if ($_GET['glasses_opacity'] == "")
										echo "100";
										else
										echo $_GET['glasses_opacity'] 
										 
										?>">
										</td>
									</tr>
									<tr>
										<td>코</td>
										<td><input type="text" class="normal_text" id="nose_num" name="nose_num"  value="<?php 
										 
										if ($_GET['nose_num'] == "")
										echo "1";
										else
										echo $_GET['nose_num'] 
										 
										?>" onfocus="$('#nose_preview').fadeIn()" onblur="$('#nose_preview').fadeOut()">
										 
										<?php echo avatar_preview("nose",$max_nose_num);  ?>
										</td>
									</tr>
									<tr>
										<td class="td_boundary">코 색상</td>
										<td class="td_boundary"><input type="text" id="nose_color" class="picker" name="nose_color" value="<?php echo $_GET['nose_color'] ?>"></td>
									</tr>
									<tr>
										<td>입/혀/치아</td>
										<td><input type="text" class="normal_text" id="mouth_num" name="mouth_num" value="<?php 
										 
										if ($_GET['mouth_num'] == "")
										echo "1";
										else
										echo $_GET['mouth_num']
										 
										 ?>" onfocus="$('#mouth_preview').fadeIn()" onblur="$('#mouth_preview').fadeOut()">
										 
										<?php echo avatar_preview("mouth",$max_mouth_num);  ?>
										</td>
									</tr>
									<tr>
										<td>입 색상</td>
										<td><input type="text" id="mouth_color" class="picker" name="mouth_color" value="<?php echo $_GET['mouth_color'] ?>"></td>
									</tr>
									<tr>
										<td>혀 색상</td>
										<td><input type="text" id="tongue_color" class="picker" name="tongue_color" value="<?php echo $_GET['tongue_color'] ?>"></td>
									</tr>
									<tr>
										<td class="td_boundary">치아 색상</td>
										<td class="td_boundary"><input type="text" id="teeth_color" class="picker" name="teeth_color" value="<?php echo $_GET['teeth_color'] ?>"></td>
									</tr>
									<tr>
										<td>하의 색상</td>
										<td><input type="text" id="bottom_color" class="picker" name="bottom_color" value="<?php echo $_GET['bottom_color'] ?>"></td>
									</tr>
									<tr>
										<td>하체</td>
										<td><input type="text" class="normal_text" id="bottom_num" name="bottom_num" value="<?php 
										 
										if ($_GET['bottom_num'] == "")
										echo "1";
										else
										echo $_GET['bottom_num'] 
										 
										?>" onfocus="$('#bottom_preview').fadeIn()" onblur="$('#bottom_preview').fadeOut()">
										 
										<?php echo avatar_preview("bottom",$max_bottom_num);  ?>
										</td>
									</tr>
							</tbody></table>
						</fieldset>

						<fieldset>
							<legend>아바타 회전</legend>
							<table><tbody>
								<tr>
									<td colspan=2 style="color:red;">※아직 완전하지 않습니다.<br>직각이 아닌 회전시 화질이 열화되는 현상이 있습니다.</td>
								</tr>
								<tr>
									<td>회전 각도</td>
									<td><input type="text" class="normal_text" name="rotate" value="<?php echo $_GET['rotate'] ?>"></td>
								</tr>
								<tr>
									<td>회전 오프셋<br>(직각이 아닌 회전 시에만)</td>
									<td><input type="text" class="normal_text" name="crop" value="<?php echo $_GET['crop'] ?>"></td>
								</tr>
							</tbody></table>
						</fieldset>
						<fieldset>
							<legend>하이퍼링크 및 말풍선</legend>
							
							<table><tbody>
								<tr>
									<td colspan=2>
									<span style="font-size:9pt;">꼬릿말 클릭시 이동할 주소를 자동으로 지정합니다.<br>(img src 태그와 API 연동에 적용됩니다.)</span>
									</td>
								</tr>
								<tr>
									<td>
										하이퍼링크 URL <br>
										<input type="button" value="지우기" onclick="$('#hyperlink').val('')">
										<input type="button" value="기본값" onclick="$('#hyperlink').val(current_page)">
									</td>
									<td>
										<input type="text" class="normal_text" name="hyperlink" id="hyperlink" value="<?php echo $_GET['hyperlink'] ?>">
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<hr>
									<span style="font-size:9pt;">이미지에는 보이지 않지만, 마우스를 올렸을 시 나올 말풍선을 설정합니다.<br>API에서는 하이퍼링크 제목으로 쓰입니다.</span>
									</td>
								</tr>
								<tr>
									<td>
										말풍선 <br>
										<input type="button" value="지우기" onclick="$('#title').val('')">
										<input type="button" value="기본값" onclick="$('#title').val('<?php echo $ouavatar_title_default ?>')">
									</td>
									<td>
										<input type="text" class="normal_text" name="title" id="title" value="<?php echo $_GET['title'] ?>">
									</td>
								</tr>
							</tbody></table>
						</fieldset>

						
						
					<input type="hidden" name="end" value="true">
					<input type="submit" value="아바타 생성하기" style="width:100%; height:48px; font-size:12pt;">
				</form>
				<fieldset>
					<legend>결과 보기</legend>
					<div id="result"></div>
				</fieldset>
				
				<!-- 하단 그림자가 어긋나지 않게 하기 위한 여백 div -->
				<div style="height:8px;"></div>
				
				<!-- 하단 그림자 -->
				<div class="bottom_shadow_container"></div>
			</div>	<!--<div class="ouavatar_menu_vertical_container">	-->
			<div style="background-color:#009DBC; color:white; text-align:center; padding:6px;">
				본 꼬릿말 서비스 제작자는 오유 운영팀과 관련이 없습니다.
			</div>
		</div><!--<div class="ouavatar_menu_whole_box">	-->
		

		
