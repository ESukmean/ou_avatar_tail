[
<?php

include_once "ouavatar_mobile_agent.php";
$main_path = str_replace("lib/php/".basename(__FILE__), "", realpath(__FILE__));
$main_path = str_replace(basename(__FILE__), "", $main_path);
include_once $main_path."ouavatar_config.php";



	// Turn off all error reporting
	error_reporting(0);	

	$rank_member = array();
		
	if ($_GET['admin'] == $admin_hash){
		$limit = "";
	} else {
		$limit = "LIMIT 0,132";
	}
	
		if ($_GET['sort'] == "view_asc"){
			$order = "order by ou_hits asc";
			$index = "use index(idx_ou_hits)";
		}
		else if ($_GET['sort'] == "view_desc"){
			$order = "order by ou_hits desc";
			$index = "use index(idx_ou_hits)";
		}
		else if ($_GET['sort'] == "level_asc"){
			$order = "order by ou_level asc";
			$index = "use index(idx_ou_level)";
		}
		else if ($_GET['sort'] == "level_desc"){
			$order = "order by ou_level desc";
			$index = "use index(idx_ou_level)";
		}
		else if ($_GET['sort'] == "date_asc"){
			$order = "order by ou_mn asc";
			$index = "";
		}
		else if ($_GET['sort'] == "date_desc"){
			$order = "order by ou_mn desc";
			$index = "";
		}
		else{
			$order = "order by ou_hits desc";
			$index = "use index(idx_ou_hits)";
		}
		
		$mysqli->query('SET NAMES utf8');
		$data = $mysqli->query(" select ou_hits,ou_mn,ou_nick,ou_getval,ou_level,ou_cdate from ouavatar_member_db ".$index." ".$order." ".$limit.";");
		while ($row = mysqli_fetch_array($data)){
			//echo ."\n";
			if ($row['ou_nickicon'] == "" || $row['ou_nickicon'] == null){
				$ou_nickicon = "star";
			} else {
				$ou_nickicon = $row['ou_nickicon'];
			}
			if (intval($row['ou_hits']) > 0){		// 조회수 0 이상일 때만 
				
				
				
				
				array_push($rank_member, 
				'"hits":'.intval($row['ou_hits']).
				',"member_num":'.intval($row['ou_mn']).
				',"nickname":'.json_encode($row['ou_nick'], JSON_UNESCAPED_SLASHES).
				',"nick_icon":'.json_encode(ouavatar_parse_getval($row['ou_getval'],"nick_icon"), JSON_UNESCAPED_SLASHES).
				',"level":'.intval($row['ou_level']).
				',"date":'.json_encode($row['ou_cdate'], JSON_UNESCAPED_SLASHES).
				'}');
			}
		}

	
	

	
	foreach ($rank_member as $key => $val) {

			if ($key > 0){
				echo ",";
			}
			echo '{"rank":"'.($key+1).'",'.$val;
			//rank:순위, hits:조회수, member_num:회원번호, nickname:닉네임
		
	}
	
	// Report all PHP errors
	error_reporting(-1);
?>
]
