<!-- 
용량 축소를 위한 주석, 개행, 공백 제거 기능이 작동하고 있으므로 
Script 삽입 시에는 반드시 끝에 ;를 붙여 주십시오.
-->    
        <title>오늘의유머 아바타 꼬릿말 생성 프로그램</title>
         <link rel="stylesheet" type="text/css" href="<?php echo $current_path ?>/lib/css/ouavatar.css">
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <script type="text/javascript" src="<?php echo $current_path ?>/lib/js/jquery-1.7.1.min.js"></script>
        <script src="<?php echo $current_path ?>/lib/colpick/js/colpick.js" type="text/javascript"></script>

        <link rel="stylesheet" href="<?php echo $current_path ?>/lib/colpick/css/colpick.css" type="text/css"/>

        <script type="text/javascript">
            <?php
                // php/esm 확장자 자동설정
                if (preg_match("/\/ouavatar_menu\.esm/", $_SERVER['PHP_SELF']))
                    $extension = "esm";
                else
                    $extension = "php";
                    
            $admin_get = myhash($_GET['admin']);
            if($admin_get != $admin_hash){
				$admin_get = "";
			}
            ?>
            //
            extension = "<?php echo $extension ?>";			
            current_path = "<?php echo $current_path ?>";
			current_page = "<?php echo $current_page ?>";
			todayhumor_member_page = "<?php echo $todayhumor_member_page ?>";
			admin_hash = "<?php echo $admin_hash ?>";
			admin_get = "<?php echo $admin_get ?>";
			ouavatar_title_default = "<?php echo $ouavatar_title_default ?>";
			$(document).ready(function(){
				ouavatar_refresh();
				ouavatar_random_thumbnail_refresh();
			})
			


		</script>

	<script type="text/javascript" src="<?php echo $current_path ?>/lib/js/ouavatar_menu_default.js" ></script>
        <script type="text/javascript" src="<?php echo $current_path ?>/lib/js/ouavatar_rank_json.js"></script>

        <script type="text/javascript" src="<?php echo $current_path ?>/lib/js/ouavatar_statistics_json.js" ></script>
        <script type="text/javascript" src="<?php echo $current_path ?>/lib/js/ouavatar_random_thumbnail.js" ></script>
        
        <!-- 
        IE 전용 CSS3 지원 스크립트 
        lt는 lower than의 약자
        -->
        
		<!--[if lt IE 10]>
		<script src="<?php echo $current_path ?>/lib/js/css3-mediaqueries.js"></script>
		<script src="<?php echo $current_path ?>/lib/js/PIE.js"></script>
		<style>.ouavatar_preview{max-width:400px;}</style>
		<![endif]-->


		<script src="<?php echo $current_path ?>/lib/js/ouavatar_style_response.js"></script>

        <?php		
			// 아바타 미리보기 및 클릭 시 숫자 반환
			function avatar_preview($name,$num,$oumark_light)
			{
				$result = "<div class='ouavatar_preview' id='".$name."_preview'>";
				
				if ($oumark_light == true){
					for ($i=0 ; $i<=100 ; $i=$i+25)
					{
						// OU마크 발광 선택 메뉴 전용
						$result .= "<span style='display:inline-block;'>".$i." <img class='ouavatar_preview_img' alt='".$i."' src='".$GLOBALS['current_path']."/lib/php/ouavatar_preview.php?body_num=1&body_color=1E90FF&oumark_color=FFFFFF&oumark_gloss_opacity=".$i."&end=true&isold&.png' onclick='$(\"#oumark_gloss_opacity\").val(\"".$i."\")'></span>";
					}
				} else {
					for ($i=1;$i<=$num;$i++)
					{
						$result .= "<span style='display:inline-block;'>".$i." <img class='ouavatar_preview_img' alt='".$i."' src='".$GLOBALS['current_path']."/lib/php/ouavatar_preview.php?".$name."_num=".$i."' onclick='$(\"#".$name."_num\").val(\"".$i."\")'></span>";
					}
				}
				$result .= "</div>";
				echo $result;
			}

			?>

        
