<?php
$file_server_path = realpath(__FILE__);
$server_path = str_replace(basename(__FILE__), "", $file_server_path);
//include_once $server_path."lib/php/ouavatar_layer.php";
$uploaded_path = str_replace("/lib/php/","/uploaded/",$server_path);
$fwcache_path = str_replace("/lib/php/","/fwcache/",$server_path);
// 파일 저장/로드 기능이므로 반드시 파일/폴더 모두 포함하여 퍼미션을 777로 해 주세요.
// fopen 저장/로드 기능


// 램 캐시			vs	디스크 캐시
// xcache_set		vs	fwcache_set
// xcache_get		vs	fwcache_get
// xcache_isset		vs	fwcache_isset
// 시간제한 설정가능 vs 시간제한 설정불가
// Array 가능		vs	Array 불가

// 램 캐시를 우선으로 사용, 그 다음 디스크 캐시 사용
// 자주 재부팅되는 서버에 적합하다.

// fwxcache_get
// fwxcache_set
// fwxcache_isset
// fw캐시와 x캐시를 합체한 것이다.
// 이 기능은 한 번 디스크에서 불러오면 램에 상주시켜 디스크 성능 저하를 줄이는 기능이다.


// 중복 충돌 방지를 위해 이 문자를 넣기 전 url encode 과정을 거친다.
$array_explode = "|";


function fwcache_isset($name){
	$filename =  $GLOBALS['fwcache_path'].urlencode($name).".txt";
	$fwcache_isset = fopen($filename, 'r');
	fclose($fwcache_isset);
	return $fwcache_isset;
}

function fwxcache_isset($name){
	// xcache가 우선, 그 다음이 fwcache
	// 캐시가 존재하나 확인 
	if (xcache_isset($name)){
		return xcache_isset($name);
	} else {
		if (fwcache_isset($name)){
			return fwcache_isset($name);
		} else {
			return false;
		}
	}
}

function fwxcache_get($name){
	if (xcache_isset($name)){
		fwcache_set($name,xcache_get($name));
		return xcache_get($name);
	} else {
		if (fwcache_isset($name)){
			xcache_set($name,fwcache_get($name)); // 디스크에서 불러와 램에 상주시켜 성능 저하 방지
			return fwcache_get($name);
		} else {
			return false;
		}
	}
}

function fwxcache_set($name,$str){
	xcache_set($name,$str,0);
	fwcache_set($name,$str);
}

function fwctime($name){
	if (file_exists ($GLOBALS['fwcache_path'].urlencode($name).".txt")){
		return filectime($GLOBALS['fwcache_path'].urlencode($name).".txt");
	} else {
		return false;
	}
}

function fwcache_get($name){
	$filename =  $GLOBALS['fwcache_path'].urlencode($name).".txt";
	$fwcache_isset = fopen($filename, 'r');
	$fwcache_get = fread($fwcache_isset, filesize($filename));
	fclose($fwcache_isset);
	if (preg_match("/\|/",$fwcache_get)){
		
		$array = explode($GLOBALS['array_explode'],$fwcache_get);
		    $out_array = array();
			foreach($array as $key => $num){
				array_push($out_array,urldecode($array[$key]));
			}
		return $out_array;

	} else {
		return urldecode($fwcache_get);
	}
}

function fwcache_set($name,$str){
	if (is_array($str)){
		$out_array = array();
		foreach($str as $key => $num){
			array_push($out_array,urlencode($str[$key]));
		}
		
		$str = implode($GLOBALS['array_explode'],$out_array);
	} else {
		$str = urlencode($str);
	}
	$filename =  $GLOBALS['fwcache_path'].urlencode($name).".txt";
	$fwcache_set = fopen($filename, 'w+');
	fwrite($fwcache_set, $str, strlen($str));
	fclose($fwcache_set);
}


// xcache_inc(string name [, int value [, int ttl]])

function fwxcache_inc($name,$num){
	if (fwxcache_isset($name)){
		$cache = intval(fwxcache_get($name));
	} else {
		$cache = intval(0);
	}
	
	fwxcache_set($name,$cache+1);
	return fwxcache_get($name);
}

function fwxcache_dec($name,$num){
	if (fwxcache_isset($name)){
		$cache = intval(fwxcache_get($name));
	} else {
		$cache = intval(0);
	}
	
	fwxcache_set($name,$cache-1);
	return fwxcache_get($name);
}

function ouavatar_fwrite($filename){
	// 이렇게 따로 해줘야 fread를 할 수 있다.
	$fhr = fopen($GLOBALS['fwcache_path'].$filename.'.txt', 'r');
							
	//$fhr_value = fread($fhr, filesize('uploaded/'.$filename.'.txt'));
	$fhr_value = fread($fhr, 100);
							
	fclose($fhr);
							
	// txt 파일 불러오기
	$fh = fopen($GLOBALS['fwcache_path'].$filename.'.txt', 'w+');
	if ($fhr_value > xcache_get($filename) || xcache_get($filename) == null || xcache_get($filename) == ""){
		// 만약 불러오는 파일이 더 클 경우(xcache가 초기화되었을 경우)
		xcache_set($filename,$fhr_value,0);	
		return $fhr_value;
	} else {
		// 만약 불러오는 파일이 더 작을 경우(평상시)
		// fwrite를 그대로 리턴하면 값이 제대로 나오지 않는다.
		fwrite($fh, xcache_get($filename));	
		return xcache_get($filename);
	}
	fclose($fh);
}

// 이것은 IP 카운트
// is_up true시 IP 기록
// is_up false시 IP를 카운트만 하기
// ip는 sha512 해시로 수집되어 관리자도 열람 불가.
function ipcount($id,$is_up){
	$ip = hash('sha512',$_SERVER['REMOTE_ADDR']);
	$file = $GLOBALS['fwcache_path'].$id.".txt";
	$ipcount_open = fopen($file, 'r');
	if (!$ipcount_open){
		$ipcount_write = fopen($file, 'w+');
		if ($is_up == true){
			fwrite($ipcount_write, $ip."\n", strlen($ip."\n"));
		} else {
			fwrite($ipcount_write, "", strlen(""));
		}
	} else {
		$data = fread($ipcount_open, filesize($file));
		// IP가 목록에 없을 때 그리고 기록 활성화시
		if (!preg_match("/".$ip."/",$data) && $is_up == true){
			$data = $data.$ip."\n";
			$ipcount_write = fopen($file, 'w+');
			fwrite($ipcount_write, $data, strlen($data));
		}
	}
	fclose($ipcount_write);
	fclose($ipcount_open);
	
	return substr_count($data,"\n");
}



?>
