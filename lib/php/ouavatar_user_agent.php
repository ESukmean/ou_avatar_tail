<?php
$ouavatar_main_path = str_replace("lib/php/".basename(__FILE__), "", realpath(__FILE__));
include_once $ouavatar_main_path."ouavatar_config.php";

	function iphex(){
		$ip = explode(".",$_SERVER['REMOTE_ADDR']);
		return sprintf('%02x%02x%02x%02x', $ip[0], $ip[1], $ip[2], $ip[3]);
	}

	// 이것은 같은 IP 안의 다기기 사용자를 배려
	$GLOBALS['device_array']       =   array(
							'/unknown/i'            =>  array(0,'정보없음/우회접속'),
                            '/iphone/i'             =>  array(1,'iPhone'),
                            '/ipod/i'               =>  array(2,'iPod'),
                            '/ipad/i'               =>  array(3,'iPad'),
                            '/android/i'            =>  array(4,'Android'),
                            '/blackberry/i'         =>  array(5,'BlackBerry'),
                            '/windows/i'    		=>  array(6,'Windows'),
                            '/mac/i'    			=>  array(7,'Mac'),
                            '/linux/i'    			=>  array(8,'GNU/Linux')
						);

    $GLOBALS['os_array']       =   array(
							'/unknown/i'            =>  array(0,'정보없음/우회접속'),
                            '/windows nt 6.3/i'     =>  array(1,'Windows 8.1'),
                            '/windows nt 6.2/i'     =>  array(2,'Windows 8'),
                            '/windows nt 6.1/i'     =>  array(3,'Windows 7'),
                            '/windows nt 6.0/i'     =>  array(4,'Windows Vista'),
                            '/windows nt 5.2/i'     =>  array(5,'Windows Server 2003/XP x64'),
                            '/windows nt 5.1/i'     =>  array(6,'Windows XP'),
                            '/windows xp/i'         =>  array(7,'Windows XP'),
                            '/windows nt 5.0/i'     =>  array(8,'Windows 2000'),
                            '/windows me/i'         =>  array(9,'Windows ME'),
                            '/win98/i'              =>  array(10,'Windows 98'),
                            '/win95/i'              =>  array(11,'Windows 95'),
                            '/win16/i'              =>  array(12,'Windows 3.11'),
                            '/macintosh|mac os x/i' =>  array(13,'Mac OS X'),
                            '/mac_powerpc/i'        =>  array(14,'Mac OS 9'),
                            '/linux/i'              =>  array(15,'GNU/Linux'),
                            '/ubuntu/i'             =>  array(16,'Ubuntu'),
                            '/iphone/i'             =>  array(17,'iPhone'),
                            '/ipod/i'               =>  array(18,'iPod'),
                            '/ipad/i'               =>  array(19,'iPad'),
                            '/android/i'            =>  array(20,'Android'),
                            '/blackberry/i'         =>  array(21,'BlackBerry'),
                            '/bb10/i'				=>	array(22,'BlackBerry 10'),
                            '/iphone os 1_/i'	=> array(23,'iOS 1'),
                            '/iphone os 2_/i'	=> array(24,'iOS 2'),
                            '/iphone os 3_/i'	=> array(25,'iOS 3'),
                            '/iphone os 4_/i'	=> array(26,'iOS 4'),
                            '/iphone os 5_/i'	=> array(27,'iOS 5'),
                            '/iphone os 6_/i'	=> array(28,'iOS 6'),
                            '/iphone os 7_/i'	=> array(29,'iOS 7'),
                            '/iphone os 8_/i'	=> array(30,'iOS 8'),
                            '/iphone os 9_/i'	=> array(31,'iOS 9'),
                            '/iphone os 10_/i'	=> array(32,'iOS 10'),
                            '/facebook/i'	=> array(33,'[BOT]Facebook'),
                            '/cros/i'			=> array(34,'Chrome OS'),
                            '/Image2play/i'	=> array(35,'[BOT]Image2play'),
                            '/playstation 4/i'	=> array(36,'PlayStation 4'),
                            '/Windows phone 8\.1/i'	=> array(37,'Windows Phone 8.1'),
                            '/freebsd/i'	=> array(38,'FreeBSD'),
                            '/Google\-Adwords\-DisplayAds\-WebRender/i'	=> array(39,'[BOT]Google-Adwords-DisplayAds-WebRender'),
                            '/Google Wireless Transcoder/i'	=> array(40,'[BOT]Google Wireless Transcoder'),
                            '/archive\.org/i' => array(41,'[BOT]WayBack Machine')
                            
                            
                            
                        );
                        
                      // 잘못된 측정 때문에 우선순위 변경
    $GLOBALS['browser_array']  =   array(
							'/unknown/i'            =>  array(0,'정보없음/우회접속'),
							'/applewebkit[^>]+safari/i'            =>  array(34,'Safari'),
							'/mozilla\/4\.0/i'            =>  array(35,'Mozilla 1.9'),
							'/AppleWebKit\/537/i'	=> array(1,'Safari 7'),
                            '/AppleWebKit\/536/i'	=> array(2,'Safari 6'),
                            '/AppleWebKit\/(534\.|6537\.)/i'	=> array(3,'Safari 5'),
                            '/AppleWebKit\/531/i'	=> array(4,'Safari 4'),
							'/msie/i'       		=>  array(5,'Internet Explorer Older than 6'),
                            '/msie 6/i'      		=>  array(6,'Internet Explorer 6'),
                            '/msie 7/i'       		=>  array(7,'Internet Explorer 7'),
                            '/msie 8/i'       		=>  array(8,'Internet Explorer 8'),
                            '/msie 9/i'       		=>  array(9,'Internet Explorer 9'),
                            '/msie 10/i'       		=>  array(10,'Internet Explorer 10'),
                            '/trident\/7\.0/i'      =>  array(11,'Internet Explorer 11 (Trident 7.0)'),
                            '/firefox/i'    		=>  array(12,'Mozilla Firefox'),
                            '/chrome/i'     		=>  array(13,'Google Chrome'),
                            '/qq/i'     			=>  array(14,'QQ Browser'),
                            '/baidu/i'     			=>  array(15,'Baidu Browser'),
                            '/swing/i'     			=>  array(16,'Swing Browser'),
                            '/opera/i'      		=>  array(17,'Opera'),
                            '/dolphin/i'      		=>  array(18,'Dolphin'),
                            '/netscape/i'   		=>  array(19,'Netscape'),
                            '/maxthon/i'    		=>  array(20,'Maxthon'),
                            '/konqueror/i'  		=>  array(21,'Konqueror'),
                            //'/wow64/i'				=> array('14','WOW64'),
                            '/bb10/i'				=>	array(22,'BlackBerry 10'),
                            '/mercury/i'			=> array(23,'Mercury'),
                            '/puffin/i'				=> array(24,'puffin'),
                            '/ucweb/i'				=> array(25,'UC Browser'),
                            '/coast\/2/i'				=> array(26,'Coast 2'),
                            '/AppleWebKit\/600/i'	=> array(27,'Safari 8'),
                            '/AppleWebKit\/533/i'	=> array(28,'Safari 5.0.x'),
                            '/facebook/i'			=> array(29,'[BOT]Facebook'),
                             '/Image2play/i'	=> array(30,'[BOT]Image2play'),
                             '/android[^>]+applewebkit\/530/i'	=> array(31,'Android Browser 4'),
                             '/AppleWebKit\/538/i'	=> array(32,'Safari 8'),
                             '/webglance/i'	=> array(33,'Web Glance'),
                              '/Google\-Adwords\-DisplayAds\-WebRender/i'	=> array(34,'[BOT]Google-Adwords-DisplayAds-WebRender'),
                            '/Google Wireless Transcoder/i'	=> array(35,'[BOT]Google Wireless Transcoder'),
                            '/archive\.org/i' => array(36,'[BOT]WayBack Machine')
                             
                        );
                        
                        
function getOS() { 
    $os_platform    =   0;
    foreach ($GLOBALS['os_array'] as $regex => $value) { 
        if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
           $os_platform    =   $value[0];
        }
    }   
    return $os_platform;
}

function getBrowser() {
    $browser        =   0;
    foreach ($GLOBALS['browser_array'] as $regex => $value) { 
        if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
            $browser    =   $value[0];
        }
    }
    return $browser;
}

function getDevice() {	
    $device        =   0;
    foreach ($GLOBALS['device_array'] as $regex => $value) { 
        if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
            $device    =   $value[0];
        }
    }
    return $device;
}


function getOSName() { 
    $os_platform    =   "정보없음/우회접속";
    foreach ($GLOBALS['os_array'] as $regex => $value) { 
        if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
           $os_platform    =   $value[1];
        }
    }   
    return $os_platform;
}

function getBrowserName() {
    $browser        =   "정보없음/우회접속";
    foreach ($GLOBALS['browser_array'] as $regex => $value) { 
        if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
            $browser    =   $value[1];
        }
    }
    return $browser;
}


$GLOBALS['iphex'] = mysql_escape_string(iphex().getDevice());
$GLOBALS['user_os']        =   mysql_escape_string(getOS());
$GLOBALS['user_browser']  =   mysql_escape_string(getBrowser());
$GLOBALS['user_device']	=	mysql_escape_string(getDevice());		// 하나의 IP 밑에서 여러 기기를 쓰는 사람을 배려.
$GLOBALS['user_browser_name'] = getBrowserName();
$GLOBALS['user_os_name'] = getOSName();











?>
