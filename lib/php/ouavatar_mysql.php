<?php
	include_once('../activexxx_mysql_config.php');
	function timestamp(){
		date_default_timezone_set("Asia/Seoul");
		return strtotime(date('Y-m-d'));
	}

	 class ouavatar_mysqli
	 {
		private $query = '';
		public function __construct(){
			timestamp();
			$this->query = '';	
		}
		public function escape_str($str){
			return addslashes($str);
		}
		public function add_query($str){
			$this->query=$this->query.$str.';';
		}
		
		public function query(){
			if($this->query != NULL && $this->query != ''){
				include_once('../activexxx_mysql_config.php');
				global $mysqli_host;
				global $mysqli_id;
				global $mysqli_pw;
				global $mysqli_db;
				$mysqli = new mysqli($mysqli_host, $mysqli_id, $mysqli_pw, $mysqli_db);
				$mysqli->multi_query($this->query);
				$mysqli->close();
			}
		}

		public function hit(){
			$this->add_query('INSERT INTO ouavatar_hits (ou_datestamp,ou_hits) VALUES (\''.timestamp().'\',\'1\') ON duplicate KEY UPDATE ou_hits = ou_hits+1');
		}
		
		public function refresh(){
			$this->add_query('INSERT INTO ouavatar_refresh (ou_datestamp,ou_refresh) VALUES (\''.timestamp().'\',\'0\') ON duplicate KEY UPDATE ou_refresh = ou_refresh+1');
		}
		public function active_users($mn)
		{
			if(xcache_isset('ou|ouavatar|'.$mn)) return;

			xcache_set('ou|ouavatar|'.$mn, '', 86400);
			$this->add_query('INSERT INTO ouavatar_active_users (ou_datestamp,ou_users) VALUES (\''.timestamp().'\',\'0\') ON duplicate KEY UPDATE ou_users = ou_users+1');
		}
	 } 