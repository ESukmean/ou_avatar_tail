<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	$file_server_path = realpath(__FILE__);
	$server_path = str_replace(basename(__FILE__), "", $file_server_path);
	//include_once $server_path."lib/php/ouavatar_mobile_agent.php";
	//include_once $server_path."lib/php/ouavatar_fwrite.php";
	include_once "ouavatar_fwrite.php";
	include_once "ouavatar_mobile_agent.php";
	$ouavatar_main_path = str_replace("lib/php/".basename(__FILE__), "", realpath(__FILE__));
	include_once $ouavatar_main_path."ouavatar_config.php";
		
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');  // 캐쉬  업데이트  다시 저장
	header("Pragma: no-cache");   
	header("Cache-Control: no-cache,must-revalidate");   

	$mysqli->query("delete from `ouavatar_hits`, `ouavatar_refresh`, `ouavatar_active_users` where ou_datestamp < '".strtotime(date("Y-m-d",strtotime("-31 days")))."';");
	$to_return = array();
	$query = $mysqli->query("SELECT A.`ou_datestamp`, A.`ou_hits`, B.`ou_refresh`, C.`ou_users` FROM `ouavatar_hits` AS A LEFT JOIN `ouavatar_refresh` AS B ON B.`ou_datestamp` = A.`ou_datestamp` LEFT JOIN `ouavatar_active_users` AS C ON C.`ou_datestamp` = A.`ou_datestamp` ORDER BY A.`ou_datestamp` DESC LIMIT 31;");
	while ($data = $query->fetch_array()){
		$row_array['datetime'] = $data['ou_datestamp'];
		$row_array['hits'] = $data['ou_hits'];
		$row_array['refresh'] = $data['ou_refresh'];
		$row_array['users'] = $data['ou_users'];

		array_push($to_return,$row_array);
	}

	?>[<?php
	echo '{"days":"0",';
	echo '"date":"'.date("Y-m-d",$to_return[0]['datetime']).'",'; 
	echo '"users_num":"'.$to_return[0]['users'].'",';
	echo '"users_arrow":"';									
	echo '","refresh_num":"'.$to_return[0]['refresh'].'","refresh_arrow":"';
	echo '","view_num":"'.$to_return[0]['hits'].'","view_arrow":"';
	echo '"},';

	for ($i=1, $j=count($to_return), $k = $j-1; $i < $j; $i++) { 
		echo '{"days":"'.$i.'",';
		echo '"date":"'.date("Y-m-d",$to_return[$i]['datetime']).'",'; 
		echo '"users_num":"'.$to_return[$i]['users'].'",';

		echo '"users_arrow":"';									
		if ($to_return[$i - 1]['users'] < $to_return[$i]['users']) {
			echo 'up';
		}else if ($to_return[$i - 1]['users'] > $to_return[$i]['users']){
			echo 'down';
		}

		echo '","refresh_num":"'.$to_return[$i]['refresh'].'","refresh_arrow":"';
		if ($to_return[$i - 1]['refresh'] < $to_return[$i]['refresh']) {
			echo 'up';
		}else if ($to_return[$i - 1]['refresh'] > $to_return[$i]['refresh']){
			echo 'down';
		} 

		echo '","view_num":"'.$to_return[$i]['hits'].'","view_arrow":"';
		if ($to_return[$i - 1]['hits'] < $to_return[$i]['hits']) {
			echo 'up';
		}else if ($to_return[$i - 1]['hits'] > $to_return[$i]['hits']){
			echo 'down';
		}

		echo '"}';
		if($i != $k){
			echo ',';
		}
	}

	?>]<?php
