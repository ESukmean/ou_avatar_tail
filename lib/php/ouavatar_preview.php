<?php
error_reporting (0);

// php 설치 후 설치 항목 메모
// 설치하지 않으면 작동하지 않습니다.
// 빠진 항목 있다면 추가 바랍니다.
// php5-gd 			이미지 표시에 필요
// php5-xcache		xcache 서버 캐시에 필요
// php5-curl		웹 페이지 가져오기에 필요

header('Content-Type: image/png');
header('Cache-Control: max-age=3600');   

// 이미지의 가로와 세로
$im_width = 200;
$im_height = 200;

if ($_GET['iscustom'] != "true"){
	if(empty($_GET['body_num'])){
		$_GET['body_num'] = '1';
		$_GET['body_color'] = 'cccccc';
	} else {
		$_GET['body_color'] = '1E90FF';
		$_GET['squid_num'] = 0;
	}
	if($_GET['squid_num']){
		$_GET['body_color'] = '1E90FF';
	}
	if(empty($_GET['hair_num'])){
		$_GET['hair_num'] = 0; 
		
	} else {
		$_GET['hair_color'] = '450000'; 
	}

	if(empty($_GET['bottom_num'])){
		$_GET['bottom_num'] = 1;
		$_GET['bottom_color'] = 'cccccc';
	} else {
		$_GET['bottom_color'] = '1E90FF';
	}

	if($_GET['nose_num']){
		$_GET['nose_color'] = 450000;
	}


		
	if(empty($_GET['oumark_color']))
		$_GET['oumark_color'] = 'cccccc';
		
	if(empty($_GET['skin_color']))
		$_GET['skin_color'] = 'ffb273';
		
	if(empty($_GET['squid_left_direction']))
		$_GET['squid_left_direction'] = 'down';
	if(empty($_GET['squid_right_direction']))
		$_GET['squid_right_direction'] = 'down';
		
		
	if(empty($_GET['hair_color']))
		$_GET['hair_color'] = '450000';
	if(empty($_GET['pupil_color']))
		$_GET['pupil_color'] = '450000';
	if(empty($_GET['eyes_color']))
		$_GET['eyes_color'] = 'FFFFFF';
	if(empty($_GET['brow_color']))
		$_GET['brow_color'] = '450000';

		
	if(empty($_GET['text_color']))
		$_GET['text_color'] = '000000';

	if(empty($_GET['background_color']))
		$_GET['background_color'] = 'ADD8E6';
	if(empty($_GET['star_color']))
		$_GET['star_color'] = 'FFA500';

	if(empty($_GET['about_color']))
		$_GET['about_color'] = '0E76DC';
	if(empty($_GET['about1']))
		$_GET['about1'] = '';
	if(empty($_GET['about2']))
		$_GET['about2'] = '';

	if(empty($_GET['rotate']))
		$_GET['rotate'] = 0;
	if(empty($_GET['crop']))
		$_GET['crop'] = 0;
	if(empty($_GET['glasses_lens_color']))
		$_GET['glasses_lens_color'] = 'ADD8E6';
	if(empty($_GET['glasses_border_color']))
		$_GET['glasses_border_color'] = '000000';
	if(empty($_GET['glasses_opacity']))
		$_GET['glasses_opacity'] = 50;


	if(empty($_GET['blush_color']))
		$_GET['blush_color'] = 'ff8973';
		
	$_GET['mouth_color'] = '7B220B';
	$_GET['tongue_color'] = 'FF6B70';
	$_GET['teeth_color'] = 'FFFFFF';	
	$_GET['background_color'] = 'FFFFFF';	
} //if ($_GET['iscustom'] != "true"){
		
// 한 쪽에만 인클루드 시켜야 할 경우 여기로 빼둔다.

if((isset($_GET['oumark_gloss_opacity']) == false || $_GET['oumark_gloss_opacity'] == "" || isset($_GET['isold']) == false) || (isset($_GET['oumark_gloss_opacity']) && $_GET['oumark_gloss_opacity'] == 0)){
    $_GET['oumark_gloss_opacity'] = 1;
}




$image = imagecreatetruecolor($im_width, $im_height);
imagealphablending($image, 0);
imagesavealpha($image, 1);
imagefilledrectangle($image, 0, 0, 200, 200, imagecolorallocatehex($image,'#'.$_GET['background_color']));

 
imagealphablending($image, 1);
 

// 경로 설정
$directory = $_SERVER['DOCUMENT_ROOT'] . "/ouavatar/";

// 모든 아바타 요소는 200x200 픽셀 투명+검은색 PNG 이어야 한다.
// 좌측 아바타 레이어 겹치기 기능. 아래로 갈수록 레이어는 위로 올라온다.
// 그림자, 입, 혀, 치아 모두 따로 PNG로 만들어두어야 한다.
$file_server_path = realpath(__FILE__);
 $server_path = str_replace(basename(__FILE__), "", $file_server_path);
 //include_once $server_path."lib/php/ouavatar_layer.php";
 include_once "ouavatar_layer.php";

// 아바타 레이어를 그리는 함수
function avatar_image($layer,$image,$color_hex,$isshadow,$opacity,$type,$x_pos,$y_pos,$iscolor){
    $layer_png = imagecreatefrompng($layer); 
    
    // 이 두 줄의 코드가 없으면 윤곽선 현상이 생긴다.
    imagealphablending($layer_png, false);
    imagesavealpha($layer_png, false);
    
    if ($iscolor != true){
		// 이미지 컬러라이징 시작
		imagefilter($layer_png, IMG_FILTER_GRAYSCALE);
		 
		// 그림자 png 레이어를 위한 기능.
		// 그림자가 n이 아니면, RGB에 50을 마이너스한 다음 만약 음수가 된다면 양수로 치환하여 그림자 효과를 구현한다.
		if ($isshadow == 'n')
		{
			$colorize_red = intval(hexdec(substr($color_hex,0,2)));
			$colorize_green = intval(hexdec(substr($color_hex,2,2)));
			$colorize_blue = intval(hexdec(substr($color_hex,4,2)));
		} else {
			$colorize_red = abs(intval(hexdec(substr($color_hex,0,2)))-50);
			$colorize_green = abs(intval(hexdec(substr($color_hex,2,2)))-50);
			$colorize_blue = abs(intval(hexdec(substr($color_hex,4,2)))-50);
		} 
		imagefilter($layer_png, IMG_FILTER_COLORIZE, $colorize_red,$colorize_green,$colorize_blue);
		// 이미지 컬러라이징 끝
	}
	
    if ($opacity == null || $opacity > 100)
        $opacity = 100;
     
    if ($type != "icon"){
		$x_pos = $_GET['avatar_x'];
		$y_pos = 0;
		$avatar_width = 199;
		$avatar_height = 200;
		$avatar_crop = $_GET['crop'];
		$avatar_rotate = $_GET['rotate'];
	} else {
		$avatar_width = 72;
		$avatar_height = 72;
		$avatar_crop = 0;
		$avatar_rotate = 0;
	}
	
     
     
    
        imagecopymerge_alpha(
        $image,         // 원본 이미지 
        imagerotate($layer_png,intval($avatar_rotate),imagecolorallocate($image,192,192,192)),             // 위에 겹칠 이미지
        $x_pos,$y_pos,            // 겹칠 이미지의 위치
        1+intval($avatar_crop),1+intval($avatar_crop),        // 겹칠 이미지 위치
        $avatar_width, $avatar_height,       // 겹칠 이미지 자르기. 겹칠 이미지의 캔버스 크기만큼 권장한다.
        $opacity                // 투명도. 0이 투명, 100이 불투명
        );
}
 
function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
    $src_w -= 3;
    $src_h -= 3;
    $cut = imagecreatetruecolor($src_w, $src_h);
    imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
    imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
    imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
}
 
// 이미지 컬러를 헥스로
function imagecolorallocatehex($im,$s){
    if ($s[0]=='#') $s=substr($s,1);
    $bg_dec=hexdec($s);
    return imagecolorallocate($im,
                ($bg_dec & 0xFF0000) >> 16,
                ($bg_dec & 0x00FF00) >>  8,
                ($bg_dec & 0x0000FF)
                );
}


// 해상도 설정 
if (!isset($_GET['resolution']) || $_GET['resolution'] == ""){
	$_GET['resolution'] = 200;
}


if (intval($_GET['resolution']) >= 200){
	imagepng($image);
	imagedestroy($image);
} else {
	$resize = intval($_GET['resolution']);
	if ($resize < 16){
		$resize = 16;
	}
	
	$image_p = imagecreatetruecolor($resize,$resize);
	imagecopyresampled ( $image_p, $image, 0, 0, 0, 0, $resize, $resize, $im_width, $im_height);
	imagepng($image_p);
	imagedestroy($image_p);
}


?>
