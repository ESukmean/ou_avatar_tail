<?php
function ouavatar_char_select($id){
	$sel = ""; // 맨 처음 이것을 남기지 않으면 에러를 뿜는다. 사용에 지장은 없지만.
	//$sel = '<div class="ouavatar_preview" id="ouavatar_char_'.$id.'" style="/*padding:8px;*/">';
		
	$sel .= '<span class="ouavatar_char_select_close_button" onclick="$(\'#ouavatar_char_'.$id.'\').slideUp(100)">닫기</span>';
	
	$tmp_class = ".ouavatar_charpad_1";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\'); " >:)</span>';
	
	$tmp_class = ".ouavatar_charpad_8";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >Ä</span>';
	
	$tmp_class = ".ouavatar_charpad_9";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >ä</span>';
	
	$tmp_class = ".ouavatar_charpad_13";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');">Ĉĉ</span>';

	$tmp_class = ".ouavatar_charpad_2";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >あ</span>';
	
	$tmp_class = ".ouavatar_charpad_3";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >ぁ</span>';
	
	$tmp_class = ".ouavatar_charpad_4";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >ア</span>';
	
	$tmp_class = ".ouavatar_charpad_5";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >ァ</span>';
	
	$tmp_class = ".ouavatar_charpad_6";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >Я</span>';
	
	$tmp_class = ".ouavatar_charpad_7";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');">я</span>';
	
	$tmp_class = ".ouavatar_charpad_10";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');" >ية</span>';

	$tmp_class = ".ouavatar_charpad_11";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');">ע</span>';
	
	$tmp_class = ".ouavatar_charpad_12";
	$sel .= '<span class="ouavatar_char_select_category_button" onclick="charpad_change(\''.$tmp_class.'\',\''.$id.'\');">ย</span>';
	
	// 콤플렉스 문자 호환을 위하여 2개의 array를 준비한다.
	
	// 이모티콘
	$array = array("-_-","^_^","囧","ಠ_ಠ","ಥ_ಥ","( ͡° ͜ʖ ͡°)","◕ ◡ ◕","ヘ(◕。◕ヘ)","(づ｡◕‿‿◕｡)づ","٩(̾●̮̮̃̾•̃̾)۶","(/◔ ◡ ◔)/","⊂(◉‿◉)つ","↙(◕ ‿‿ ◕)↘");
	$sel .= charpad($sel, $array,$array, $id, "1", "이모티콘");
	
	
	// 히라가나
	$array = array("あ", "い", "う", "え", "お","か","き","く","け","こ","が","ぎ","ぐ","げ","ご","さ","し","す","せ","そ","ざ","じ","ず","ぜ","ぞ","た","ち","つ","て","と","だ","ぢ","づ","で","ど","な","に","ぬ","ね","の","は","ひ","ふ","へ","ほ","ば","び","ぶ","べ","ぼ","ぱ","ぴ","ぷ","ぺ","ぽ","ま","み","む","め","も","や","ゆ","よ","ら","り","る","れ","ろ","わ","ゐ","ゔ","ゑ","を","ん");
	$sel .= charpad($sel, $array,$array, $id, "2", "히라가나");

	// 작은 히라가나
	$array = array("ぁ","ぃ","ぅ","ぇ","ぉ","っ","ゃ","ゅ","ょ","ゎ");
	$sel .= charpad($sel, $array,$array, $id, "3", "히라가나");

	// 카타카나
	$array = array("ア","イ","ウ","エ","オ","カ","キ","ク","ケ","コ","ガ","ギ","グ","ゲ","ゴ","サ","シ","ス","セ","ソ","ザ","ジ","ズ","ゼ","ゾ","タ","チ","ツ","テ","ト","ダ","ヂ","ヅ","デ","ド","ナ","ニ","ヌ","ネ","ノ","ハ","ヒ","フ","ヘ","ホ","バ","ビ","ブ","ベ","ボ","パ","ピ","プ","ペ","ポ","マ","ミ","ム","メ","モ","ヤ","ユ","ヨ","ラ","リ","ル","レ","ロ","ワ","ヰ","ヴ","ヱ","ヲ","ン");
	$sel .= charpad($sel, $array,$array, $id, "4", "가타카나");

	// 작은 카타카나
	$array = array("ァ","ィ","ゥ","ェ","ォ","ッ","ャ","ュ","ョ","ヮ","ヵ","ヶ");
	$sel .= charpad($sel, $array,$array, $id, "5", "가타카나");

	// 키릴 대문자
	$array = array("Ѐ","Ё","Ђ","Ѓ","Є","Ѕ","І","Ї","Ј","Љ","Њ","Ћ","Ќ","Ѝ","Ў","Џ","<br>","А","Б","В","Г","Д","Е","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я");
	$sel .= charpad($sel, $array,$array, $id, "6", "키릴 대문자");

	// 키릴 소문자
	$array = array("ѐ","ё","ђ","ѓ","є","ѕ","і","ї","ј","љ","њ","ћ","ќ","ѝ","ў","џ","<br>","а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","ъ","ы","ь","э","ю","я");
	$sel .= charpad($sel, $array,$array, $id, "7", "키릴 소문자");

	// 라틴 대문자
	$array = array("À","Á","Â","Ã","Ä","Å","Æ","Ç","È","É","Ê","Ë","Ì","Í","Î","Ï","Ð","Ñ","Ò","Ó","Ô","Õ","Ö","Ø","Ù","Ú","Û","Ü","Ý","Þ","ß");
	$sel .= charpad($sel, $array,$array, $id, "8", "라틴 대문자");

	// 라틴 소문자
	$array = array("à","á","â","ã","ä","å","æ","ç","è","é","ê","ë","ì","í","î","ï","ð","ñ","ò","ó","ô","õ","ö","ø","ù","ú","û","ü","ý","þ");
	$sel .= charpad($sel, $array,$array, $id, "9", "라틴 소문자");
	
	// 에스페란토
	$array = array("Ĉ","Ĝ","Ĥ","Ĵ","Ŝ","Ŭ","<br>","ĉ","ĝ","ĥ","ĵ","ŝ","ŭ");
	$sel .= charpad($sel, $array,$array, $id, "13", "에스페란토");
	

	// 아랍어
	$array = array('ا','ب','ت','ث','ج','ح','خ','د','ذ','ر','ز','س','ش','ص','ض','ط','ظ','ع','غ','ف','ق','ك','ل','م','ن','ه','و','ي','ة');
	$sel .= charpad($sel, $array,$array, $id, "10", "<span style='font-size:12pt; font-weight:bold; color:red'>아랍어 (불안정, 글자 뒤섞임 주의)</span><div style='font-size:9pt;'>RTL 언어 표시를 위한 시스템을 구현하고 있지만 아직 완전하지 않습니다. ouavatar/lib/php/ouavatar_rtltoltr.php 를 수정해 주시기 바랍니다.</div>");

	// 히브리어
	$array = array('א','ב','ג','ד','ה','ו','ז','ח','ט','י','ך','כ','ל','ם','מ','ן','נ','ס','ע','ף','פ','ץ','צ','ק','ר','ש','ת','װ','ױ','ײ','׳','״');
	$sel .= charpad($sel, $array,$array, $id, "11", "<span style='font-size:12pt; font-weight:bold; color:red'>히브리어 (불안정, 글자 뒤섞임 주의)</span><div style='font-size:9pt;'>RTL 언어 표시를 위한 시스템을 구현하고 있지만 아직 완전하지 않습니다. ouavatar/lib/php/ouavatar_rtltoltr.php 를 수정해 주시기 바랍니다.</div>");
	

	// 태국어는 콤플렉스 문자이므로 array를 따로 주어야 한다.
	$complex_char = "<span class='ouavatar_emoticon_button_complex'>ก</span>ั";
	$array = array("ก","ข","ฃ","ค","ฅ","ฆ","ง","จ","ฉ","ช","ซ","ฌ","ญ","ฎ","ฏ","ฐ","ฑ","ฒ","ณ","ด","ต","ถ","ท","ธ","น","บ","ป","ผ","ฝ","พ","ฟ","ภ","ม","ย","ร","ฤ","ล","ฦ","ว","ศ","ษ","ส","ห","ฬ","อ","ฮ","ฯ","ะ","ั","า","ำ","ิ","ี","ึ","ื","ุ","ู","ฺ","<br>","฿","เ","แ","โ","ใ","ไ","ๅ","ๆ","็","่","้","๊","๋","์","ํ","๎","<br>","๏","๐","๑","๒","๓","๔","๕","๖","๗","๘","๙","๚","๛");
	$array_button = array("ก","ข","ฃ","ค","ฅ","ฆ","ง","จ","ฉ","ช","ซ","ฌ","ญ","ฎ","ฏ","ฐ","ฑ","ฒ","ณ","ด","ต","ถ","ท","ธ","น","บ","ป","ผ","ฝ","พ","ฟ","ภ","ม","ย","ร","ฤ","ล","ฦ","ว","ศ","ษ","ส","ห","ฬ","อ","ฮ","ฯ","ะ","<span></span>ั","า","<span></span>ำ","<span></span>ิ","<span></span>ี","<span></span>ึ","<span></span>ื","<span></span>ุ","<span></span>ู","<span></span>ฺ","<br>","฿","เ","แ","โ","ใ","ไ","ๅ","ๆ","<span></span>็","<span></span>่","<span></span>้","<span></span>๊","<span></span>๋","<span></span>์","<span></span>ํ","<span></span>๎","<br>","๏","๐","๑","๒","๓","๔","๕","๖","๗","๘","๙","๚","๛");
	
	$sel .= charpad($sel, $array,$array_button, $id, "12", "태국어");
	

	$sel .= '</div>'; // 이모티콘 창 전체를 닫는 div
	return $sel;
}

function charpad($sel, $array,$array_button, $id, $num, $k_name){
	$sel = "";
	if ($num != "1"){
		$style = "style='display:none;'";
	} else {
		$style = "";
	}
	$sel .= "<div class='ouavatar_charpad_".$num."' ".$style."><div style='background-color:#C6FFC6; font-size:11pt; padding:4px; font-weight:bold;'>".$k_name."</div>";
	for($i=0;$i<count($array);$i++){
		if ($array[$i] == "<br>"){
		$sel .= "<br>";
		} else{
		$sel .= '<span class="ouavatar_emoticon_button" 
					onclick="$(\'#'.$id.'\').focus(); 
					ouavatar_char_click(this,\''.$id.'\'); 
					" char="'.$array[$i].'">'
					.$array_button[$i].
				'</span>';
		}
	}
	$sel .= "</div>"; // charpad 닫음 
	
	// 데이터 절약을 위해 필요없는 공간 제거
	$sel = str_replace(array("\r","\n","\t")," ",$sel);
	return preg_replace("/\s+/", " ", $sel);
}		

echo ouavatar_char_select($_GET['id']);
?>
