<?php
// 모바일 에이전트 감지
// 모바일 에이전트 목록
$mobile_agent = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/';

// preg_match() 함수를 이용해 모바일 기기로 접속하였는지 확인
if(preg_match($mobile_agent, $_SERVER['HTTP_USER_AGENT'])) {
	// 모바일
	$ismobile = true;
	$todayhumor_home = 'http://m.todayhumor.co.kr';
	$todayhumor_member_page = "http://m.todayhumor.co.kr/list.php?kind=member&mn=";
} else {
	// PC
	$ismobile = false;
	$todayhumor_home = 'http://www.todayhumor.co.kr';
	$todayhumor_member_page = "http://www.todayhumor.co.kr/board/list.php?kind=member&mn=";
}
// 모바일 에이전트 감지 끝

if (preg_match("/MSIE (4|5|6|7)\./",$_SERVER['HTTP_USER_AGENT'],$matches)){
	$islegacy = true;
} else {
	$islegacy = false;
}


$current_path = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER["REQUEST_URI"]);
$current_page = "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
// 경로 설정 끝

// 관리자 모드 해시
$admin_hash = myhash("oumyadmin");

function myhash($str){
	$str_plain = $str;
	$str_salt = md5($str);
	return hash("sha512",$str_plain.$str_salt);
}
?>
