<?php

include_once "ouavatar_user_agent.php";
$datestamp = strtotime(date('Y-m-d'));

// 시간대 설정
date_default_timezone_set("Asia/Seoul");

$mysqli->multi_query("CREATE INDEX idx_ua_os ON ouavatar_user_agent(ua_os);
					CREATE INDEX idx_ua_ip ON ouavatar_user_agent(ua_ip);
					CREATE INDEX idx_ua_browser ON ouavatar_user_agent(ua_browser);
					CREATE INDEX idx_ou_hits ON ouavatar_member_db(ou_hits);
					CREATE INDEX idx_ou_level ON ouavatar_member_db(ou_level);");

if (!isset($_COOKIE["ouavatar_ua_write"]) && $is_todayhumor_domain == true && $is_testboard == false){
	$yesterday = date("Y-m-d H:i:s",strtotime("-1 day"));
	$insert_query = "CREATE TABLE IF NOT EXISTS ouavatar_user_agent
					(
					ua_id			INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
					ua_ip			CHAR(10) UNIQUE KEY,
					ua_os			TINYINT,
					ua_browser		TINYINT,
					ua_date			DATETIME DEFAULT '0000-00-00 00:00:00'
					)CHARACTER SET ascii COLLATE ascii_general_ci;
		CREATE TABLE IF NOT EXISTS ouavatar_full_ua
					(
					ua_id			INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
					ua_ip			CHAR(10) UNIQUE KEY,
					ua_unkn_type	CHAR(8),
					ua_if_unknown	TEXT
					);
		 
		DELETE FROM ouavatar_user_agent where ua_date < '".$yesterday."';

		insert into ouavatar_user_agent (ua_ip,ua_os,ua_browser,ua_date)
					values ('".$GLOBALS['iphex']."',
					'".$GLOBALS['user_os']."',
					'".$GLOBALS['user_browser']."',
					'".date("Y\/m\/d H:i:s")."')	
					on duplicate key update ua_ip = '".$GLOBALS['iphex']."';		


						";
						
						
						
	// user-agent를 모를 때만
	if ($GLOBALS['user_os'] == 0 || $GLOBALS['user_browser'] == 0 ){	
		
		if ($GLOBALS['user_os'] == 0){
			$ua_unkn_type = "OS";
		} if ($GLOBALS['user_browser'] == 0){
			$ua_unkn_type = "Browser";
		} if ($GLOBALS['user_browser'] == 0 && $GLOBALS['user_os'] == 0){
			$ua_unkn_type = "BOTH";
		} 
		
		
		$insert_query .= "
			delete from ouavatar_full_ua 
				where ua_if_unknown='".mysql_escape_string($_SERVER['HTTP_USER_AGENT'])."';
			insert into ouavatar_full_ua
						(ua_ip, 
						ua_unkn_type,
						ua_if_unknown)
					values 
						('".$GLOBALS['iphex']."',
						'".$ua_unkn_type."',
						'".mysql_escape_string($_SERVER['HTTP_USER_AGENT'])."')";
	}
	setcookie("ouavatar_ua_write", "true", time()+3600); 			
} else {
	$insert_query = "";
}			

?>
