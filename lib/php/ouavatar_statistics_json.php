<?php

$file_server_path = realpath(__FILE__);
 $server_path = str_replace(basename(__FILE__), "", $file_server_path);
//include_once $server_path."lib/php/ouavatar_mobile_agent.php";
//include_once $server_path."lib/php/ouavatar_fwrite.php";
include_once "ouavatar_fwrite.php";
include_once "ouavatar_mobile_agent.php";
$ouavatar_main_path = str_replace("lib/php/".basename(__FILE__), "", realpath(__FILE__));
include_once $ouavatar_main_path."ouavatar_config.php";
	
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');  // 캐쉬  업데이트  다시 저장
header("Pragma: no-cache");   
header("Cache-Control: no-cache,must-revalidate");   

$hits_num = array();
$hits_datestamp = array();
$refresh_num = array();
$refresh_datestamp = array();
$active_num = array();
$active_datestamp = array();


$mysqli->query("delete from ouavatar_hits where
		ou_datestamp < '".strtotime(date("Y-m-d",strtotime("-31 days")))."';");
		
$mysqli->query("delete from ouavatar_refresh where
		ou_datestamp < '".strtotime(date("Y-m-d",strtotime("-31 days")))."';");
		
$mysqli->query("delete from ouavatar_active_users where
		ou_datestamp < '".strtotime(date("Y-m-d",strtotime("-31 days")))."';");




$query = $mysqli->query("select * from ouavatar_hits order by ou_datestamp desc limit 31");
while ($row = mysqli_fetch_array($query,MYSQLI_ASSOC)){
	array_unshift($hits_num, $row['ou_hits']);
	array_unshift($hits_datestamp,$row['ou_datestamp']);
}

$query = $mysqli->query("select * from ouavatar_refresh order by ou_datestamp desc limit 31");
while ($row = mysqli_fetch_array($query,MYSQLI_ASSOC)){
	array_unshift($refresh_num, $row['ou_refresh']);
	array_unshift($refresh_datestamp,$row['ou_datestamp']);
}

$query = $mysqli->query("select * from ouavatar_active_users order by ou_datestamp desc limit 31");
while ($row = mysqli_fetch_array($query,MYSQLI_ASSOC)){
	array_unshift($active_num, $row['ou_users']);
	array_unshift($active_datestamp,$row['ou_datestamp']);
}

$i = count($active_num)-1;


echo "[";			

	foreach($hits_datestamp as $key => $val){
		 echo '{"days":"'.$i.'",';
		 echo '"date":"'.date("Y-m-d",$hits_datestamp[$key]).'",'; 
		 
		 /* 활성 사용자 수 */
		 echo '"users_num":"'.$active_num[$key].'",';
		 
		 
		 /* 활성 사용자 수 화살표 */
		 echo '"users_arrow":"';
		 
				// 어제/오늘 꼬릿말 비교
				$statistics_yesterday = $active_num[$key+1];
				$statistics_today = $active_num[$key];
														
				if ( $statistics_yesterday > $statistics_today){
					echo "down";
				} else if ( $statistics_yesterday < $statistics_today ) {
					echo "up";
				}
		/* 갱신된 꼬릿말 */
		echo '","refresh_num":"'.$refresh_num[$key].'",';

		/* 갱신된 꼬릿말 화살표 */
		echo '"refresh_arrow":"';
				// 어제/오늘 꼬릿말 비교
				$statistics_yesterday = $refresh_num[$key+1];
				$statistics_today = $refresh_num[$key];
														
				if ( $statistics_yesterday > $statistics_today){
					echo "down";
				} else if ( $statistics_yesterday < $statistics_today ) {
					echo "up";
				}
				
		/* 조회된 꼬릿말 */
		echo '","view_num":"'.$hits_num[$key].'",';
		
		/* 조회된 꼬릿말 화살표 */
		echo '"view_arrow":"';
				// 어제/오늘 꼬릿말 비교
				$statistics_yesterday = $hits_num[$key+1];
				$statistics_today = $hits_num[$key];
		
				if ( $statistics_yesterday > $statistics_today){
					echo "down";
				} else if ( $statistics_yesterday < $statistics_today ) {
					echo "up";
				}
		echo '"}';
				
		if ($key < count($hits_datestamp)-1){
			echo ",";
		}
		$i--;
	}
echo "]";
