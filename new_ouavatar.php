<?php 
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	date_default_timezone_set("Asia/Seoul");

	header('Content-Type: image/png');
	header('Cache-Control: max-age=600');

	// 중복함수를 인클루드시킨다.
	$file_server_path = realpath(__FILE__);
	$server_path = str_replace(basename(__FILE__), "", $file_server_path);
	if (isset($_GET['language']) == false) $_GET['language'] = '';
	include_once "lib/php/ouavatar_mysql.php";		// MySQL 관련
	include_once "lib/php/ouavatar_lang.php";		// 다국어 지원
	include_once "lib/php/ouavatar_if.php";		// if 변수 모아둔것
	include_once "lib/php/ouavatar_fwrite.php";	// fwrite 변수 모아둔것 
	include_once "lib/php/ouavatar_rtltoltr.php";	// RTL 지원 

	$query = '';
	$avatar_mysqli = new ouavatar_mysqli();
	$init_setting = array(
		'im_width' => 806,
		'im_height' => 206,
		'directory' => $_SERVER['DOCUMENT_ROOT'].'/ouavatar/',
		'font' => $_SERVER['DOCUMENT_ROOT'].'/ouavatar/hangangm.ttf',
		'nbsp_md5' => (xcache_isset('ou|avatar|&nbsp')) ? (xcache_get('ou|avatar|&nbsp')) : (xcache_set('ou|avatar|&nbsp', md5("&nbsp;"), 600) == false || xcache_get('ou|avatar|&nbsp')),
		'uinfo_1' => '오유 회원 API가 맛이간 관계로 회원정보를 표시할 수 없습니다.',
		'uinfo_2' => 'http://www.todayhumor.co.kr/member/api.php?mn=36 ···',
		'about1' => $_GET['about1'],
		'about2' => $_GET['about2'],
		'color' => '#'.$_GET['text_color'],
		'background_color' => '#'.$_GET['background_color'],
		'rtl' => $_GET['text_direction'] == 'rtl' ? true : false,
		'nick_icon' => ($_GET['nick_icon'] == 'star' || $_GET['nick_icon'] == 'ribbon' || $_GET['nick_icon'] == 'heart') ? $_GET['nick_icon'] : 'star',
		'star_color' => '#'.$_GET['star_color'],
		'about_color' => '#'.$_GET['about_color'],
		'join_text' => '',
		'gen_text' => ''
	);
	$user_info = array(
		'nickname' => '알 수 없음',
		'joindate' => '알 수 없음',
		'times' => 0,
		'replytimes' => 0,
		'bobtimes' => 0,
		'besttimes' => 0,
		'totaltimes' => 0,
		'level' => 0
	);

	//var_dump($init_setting);
	//var_dump($user_info);

	function curl_page($str){
		if(xcache_isset('ou|page|'.$str))
			return xcache_get('ou|page|'.$str);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $str);
		curl_setopt($ch, CURLOPT_USERAGENT,'OU_Tail_Maker (ActiveXXX, irobota, humorbest_862108)');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		xcache_set('ou|page|'.$str, str_replace(array("\t","\n","\r"),'',curl_exec($ch)), 600);
		curl_close($ch);
		return xcache_get('ou|page|'.$str);
	}
	function rtl_text($size, $font, $text, $x){
		$dimensions = imagettfbbox($size, 0, $font, $text);
		$textWidth = abs($dimensions[4] - $dimensions[0]);
		return $x+(imagesx($im) - $textWidth);
	}
	function avatar_image($layer,$image,$color_hex,$isshadow,$opacity,$type,$x_pos,$y_pos,$iscolor){
		$layer_png = imagecreatefrompng($layer); 
		
		// 이 두 줄의 코드가 없으면 윤곽선 현상이 생긴다.
		imagealphablending($layer_png, false);
		imagesavealpha($layer_png, false);
		
		if ($iscolor != true){
			// 이미지 컬러라이징 시작
			imagefilter($layer_png, IMG_FILTER_GRAYSCALE);
			 
			// 그림자 png 레이어를 위한 기능.
			// 그림자가 n이 아니면, RGB에 50을 마이너스한 다음 만약 음수가 된다면 양수로 치환하여 그림자 효과를 구현한다.
			if ($isshadow == 'n')
			{
				$colorize_red = intval(hexdec(substr($color_hex,0,2)));
				$colorize_green = intval(hexdec(substr($color_hex,2,2)));
				$colorize_blue = intval(hexdec(substr($color_hex,4,2)));
			} else {
				$colorize_red = abs(intval(hexdec(substr($color_hex,0,2)))-50);
				$colorize_green = abs(intval(hexdec(substr($color_hex,2,2)))-50);
				$colorize_blue = abs(intval(hexdec(substr($color_hex,4,2)))-50);
			} 
			imagefilter($layer_png, IMG_FILTER_COLORIZE, $colorize_red,$colorize_green,$colorize_blue);
			// 이미지 컬러라이징 끝
		}
		
		if ($opacity == null || $opacity > 100)
			$opacity = 100;
		 
		if ($type != "icon"){
			$x_pos = $_GET['avatar_x'];
			$y_pos = 0;
			$avatar_width = 199;
			$avatar_height = 200;
			$avatar_crop = $_GET['crop'];
			$avatar_rotate = $_GET['rotate'];
		} else {
			$avatar_width = 72;
			$avatar_height = 72;
			$avatar_crop = 0;
			$avatar_rotate = 0;
		}

		imagecopymerge_alpha(
			$image,         // 원본 이미지 
			imagerotate($layer_png,intval($avatar_rotate),imagecolorallocate($image,192,192,192)),             // 위에 겹칠 이미지
			$x_pos + 10,$y_pos,            // 겹칠 이미지의 위치
			1+intval($avatar_crop),1+intval($avatar_crop),        // 겹칠 이미지 위치
			$avatar_width, $avatar_height,       // 겹칠 이미지 자르기. 겹칠 이미지의 캔버스 크기만큼 권장한다.
			$opacity                // 투명도. 0이 투명, 100이 불투명
		);
	}
	 
	function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
		$src_w -= 3;
		$src_h -= 3;
		$cut = imagecreatetruecolor($src_w, $src_h);
		imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
		imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
		imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
	}
	 
	// 이미지 컬러를 헥스로
	function imagecolorallocatehex($im,$s){
		if ($s[0]=='#') $s=substr($s,1);
		$bg_dec=hexdec($s);
		return imagecolorallocate($im,
					($bg_dec & 0xFF0000) >> 16,
					($bg_dec & 0x00FF00) >>  8,
					($bg_dec & 0x0000FF)
					);
	}

	$image = imagecreatetruecolor($init_setting['im_width'], $init_setting['im_height']);
	imagealphablending($image, 0);
	imagesavealpha($image, 1);
	imagefilledrectangle($image, 0, 0, $init_setting['im_width'], $init_setting['im_height'], imagecolorallocatehex($image, $init_setting['background_color']));
	imagealphablending($image, 1);

	$init_setting['color'] = imagecolorallocatehex($image, $init_setting['color']);
	$init_setting['background_color'] = imagecolorallocatehex($image, $init_setting['background_color']);
	$init_setting['star_color'] = imagecolorallocatehex($image, $init_setting['star_color']);
	$init_setting['about_color'] = imagecolorallocatehex($image, $init_setting['about_color']);

	//var_dump($init_setting);

	if(xcache_isset('ou|new|minfo|'.$_GET['mn'])){
		$user_info = xcache_get('ou|new|minfo|'.$_GET['mn']);
	}else{
		$ouapi = curl_page('http://www.todayhumor.co.kr/board/list.php?kind=member&mn='.$_GET['mn'], true);

	 	$tmp;
		preg_match("/<strong>(.*)<\/strong>님의/", $ouapi, $tmp);
		$user_info['nickname'] =  $tmp[1];
		preg_match("/회원가입 : ((\d\d-)(\d\d-)(\d\d))/", $ouapi, $tmp);
		$user_info['joindate'] = '20'.$tmp[1];   // 가입날짜
		preg_match("/방문횟수 : (\d*)/", $ouapi, $tmp);
		$user_info['times'] = $tmp[1]; // 방문횟수
		preg_match_all("/>([0-9]+)<\/a>/", $ouapi, $tmp);
		$user_info['totaltimes'] = $tmp[1][0]; // 전체글 수

		if($user_info['times'] == ''){
			$user_info['nickname'] = $text_nickname_null;
			$user_info['joindate'] = $text_joindate_null;	
		}

		xcache_set('ou|new|minfo|'.$_GET['mn'], $user_info, 600);

		$avatar_mysqli->refresh();
		$avatar_mysqli->active_users($_GET['mn']);
	}

	switch ($_GET['language']) {
		case NULL:
		case '':
			$init_setting['join_text'] = '회원가입 : '.$user_info['joindate'].'  방문횟수 : '.$user_info['times'].'회';
			$init_setting['gen_text'] = '전체글 : '.$user_info['totaltimes'].'  ※공식 API의 문제로 데이터가 불안정 및 미비합니다.';
			break;
		
		case 'zh_cn':
			$init_setting['join_text'] = '会员加入 : '.$user_info['joindate'].'  访问次数 : '.$user_info['times'].'次';
			$init_setting['gen_text'] = '布告个数:'.$user_info['totaltimes'];	
			break;

		case 'zh_tw':
			$init_setting['join_text'] = '會員加入 : '.$user_info['joindate'].'  訪問次數 : '.$user_info['times'].'次';
			$init_setting['gen_text'] = '布告個數:'.$user_info['totaltimes'];
			break;

		case 'ja_jp':
			$init_setting['join_text'] = '会員加入 : '.$user_info['joindate'].'  訪問回数 : '.$user_info['times'].'回';
			$init_setting['gen_text'] = '全体スレッド:'.$user_info['totaltimes'];		
			break;

		case 'en_us':
			if ($user_info['joindate'] != $text_joindate_null){
				$user_info['joindate'] = date("F j, Y",strtotime($user_info['joindate']));
			}
			
			$init_setting['join_text'] = 'Join : '.$user_info['joindate'].'  Visit : '.$user_info['times'].'';
			$init_setting['gen_text'] = 'Posts: '.$user_info['totaltimes'];			
			break;

		case 'en_uk':
			if ($user_info['joindate'] != $text_joindate_null){
				$user_info['joindate'] = date("j F Y",strtotime($user_info['joindate']));
			}
			
			$init_setting['join_text'] = 'Join : '.$user_info['joindate'].'  Visit : '.$user_info['times'].'';
			$init_setting['gen_text'] = '  Posts: '.$user_info['totaltimes'];		
			break;

		case 'es_es':
			if ($user_info['joindate'] != $text_joindate_null){
				$user_info['joindate'] = month_lang(date("j \d\\e F, Y",strtotime($user_info['joindate'])));
			}
			
			$init_setting['join_text'] = 'Registro : '.$user_info['joindate'].'  Visita : '.$user_info['times'].'';
			$init_setting['gen_text'] = 'Poste:'.$user_info['totaltimes'];
			break;

		case 'eo_eo':
			if ($user_info['joindate'] != $text_joindate_null){
				$user_info['joindate'] = month_lang(date("j \d\\e F, Y",strtotime($user_info['joindate'])));
			}
			
			$init_setting['join_text'] = 'Enskribis : '.$user_info['joindate'].'  Visitoj : '.$user_info['times'].'';
			$init_setting['gen_text'] = 'Poŝtoj:'.$user_info['totaltimes'];
			break;

		case 'ru_ru':
			if ($user_info['joindate'] != $text_joindate_null){
				$user_info['joindate'] = month_lang(date("d F Y г.",strtotime($user_info['joindate'])));
			}
			
			$init_setting['join_text'] = 'Вступать : '.$user_info['joindate'].'  Визиты : '.$user_info['times'].'';
			$init_setting['gen_text'] = 'Сообщений:'.$user_info['totaltimes'];
			break;

		case 'fr_fr':
			if ($user_info['joindate'] != $text_joindate_null){
				$user_info['joindate'] = month_lang(date("j F Y",strtotime($user_info['joindate'])));
			}
			
			$init_setting['join_text'] = 'Inscription : '.$user_info['joindate'].' Visites : '.$user_info['times'].'';
			$init_setting['gen_text'] = 'Entiers:'.$user_info['totaltimes'];
			break;


		default:
			$init_setting['join_text'] = '회원가입 : '.$user_info['joindate'].'  방문횟수 : '.$user_info['times'].'회';
			$init_setting['gen_text'] = '전체글 : '.$user_info['totaltimes'].'  ※공식 API의 문제로 데이터가 불안정 및 미비합니다.';
			break;
	}

	if($init_setting['rtl'] == false){
		$_GET['avatar_x'] = 0;
		$star_x = 200;
		$nickname_x = 290;
		$lv_text_x = 210;
		$level_x = rtl_text(14,$init_setting['font'],$level,260);
		$join_text_x = 220;
		$gen_text_x = 220;
		$about1_text_x = 220;
		$about2_text_x = 220;
	} else {
		$nickname_text = str_replace($init_setting['nbsp_md5']," ",trim(rtl_ltr(str_replace(" ",$init_setting['nbsp_md5'],$user_info['nickname']))));
		$about1_text = str_replace($init_setting['nbsp_md5']," ",trim(rtl_ltr(str_replace(" ",$init_setting['nbsp_md5'],$init_setting['about1']))));
		$about2_text = str_replace($init_setting['nbsp_md5']," ",trim(rtl_ltr(str_replace(" ",$init_setting['nbsp_md5'],$init_setting['about2']))));

		$_GET['avatar_x'] = 606;
		$star_x = rtl_text(70,$init_setting['font'],'★',596);
		$nickname_x = rtl_text(40,$init_setting['font'],$user_info['nickname'],516);
		$lv_text_x = rtl_text(24,$init_setting['font'],"Lv.",566);
		$level_x = rtl_text(24,$init_setting['font'],$user_info['lv'],586);
		$join_text_x = rtl_text(18,$init_setting['font'],$init_setting['join_text'],586);
		$gen_text_x = rtl_text(15,$init_setting['font'],$init_setting['gen_text'],586);
		$about1_text_x = rtl_text(14,$init_setting['font'],$about1_text,586);
		$about2_text_x = rtl_text(14,$init_setting['font'],$about2_text,586);
	}

	//var_dump($user_info);
	//var_dump($init_setting);
	//리본, 별 아이콘 ★
	avatar_image($directory.'icon/icon_'.$_GET['nick_icon'].'.png',$image,$_GET['star_color'],"n",100,"icon",$star_x,5);

	for ($i=0; $i<2; $i++){
		// $star = 별표
		//imagettftext($image, 70, 0, $star_x, 68, $init_setting['star_color'], $init_setting['font'], '★');
		// $nickname = 회원 닉네임
		imagettftext($image, 40, 0, $nickname_x, 58, $init_setting['color'], $init_setting['font'], $user_info['nickname']);
		// $lv_text = 회원 레벨 Lv
		//imagettftext($image, 24, 0, $lv_text_x, 35, $init_setting['color']), $init_setting['font'], 'Lv.');
		// $level = 회원 레벨 숫자 (레벨숫자 오른쪽 정렬을 위해)
		//imagettftext($image, 24, 0, $level_x, 65, $init_setting['color']), $init_setting['font'], $user_info['level']);
		// $join_text = 회원가입
		imagettftext($image, 18, 0, $join_text_x, 100, $init_setting['color'], $init_setting['font'], $init_setting['join_text']);
		// $gen_text = 댓글수, 베오베, 베스트, 전체글 
		imagettftext($image, 15, 0, $gen_text_x, 127, $init_setting['color'], $init_setting['font'], $init_setting['gen_text']);  
		// $about1 = 자기소개 첫번째 줄	
		imagettftext($image, 14, 0, $about1_text_x, 155, $init_setting['about_color'], $init_setting['font'], $init_setting['about1']);
		// $about2 = 자기소개 두번째 줄
		imagettftext($image, 14, 0, $about2_text_x, 180, $init_setting['about_color'], $init_setting['font'], $init_setting['about2']);
	}

	include_once $server_path."ouavatar_layer.php";
	include_once "ouavatar_layer.php";
	
	//최종 이미지 생성
	imagepng($image);
	imagedestroy($image);

	fastcgi_finish_request();
	$avatar_mysqli->hit();
	$avatar_mysqli->query();
?>