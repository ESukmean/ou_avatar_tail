<?php
// 에러를 표시한다. 개발자가 아닐 시 0 권장.
error_reporting (0);


$ouavatar_main_path = str_replace("lib/php/".basename(__FILE__), "", realpath(__FILE__));
$ouavatar_main_path = str_replace(basename(__FILE__), "", realpath(__FILE__));
include_once $ouavatar_main_path."ouavatar_config.php";






$file_server_path = realpath(__FILE__);
$server_path = str_replace(basename(__FILE__), "", $file_server_path);
$server_path = str_replace("/lib/php", "", $server_path);
include_once $server_path."lib/php/ouavatar_mobile_agent.php";
include_once $server_path."lib/php/ouavatar_if.php";
include_once $server_path."lib/php/ouavatar_max_avatar_num.php";
include_once $server_path."lib/php/ouavatar_fwrite.php";

// fwrite 에서 fcache 파일 청소


// 파일 청소 명령어
$files = glob("{".$fwcache_path."*}",GLOB_BRACE);


foreach($files as $file){ // iterate files
  if(is_file($file)){
		if (fileatime($file) < strtotime(date(DATE_RFC822, strtotime("-32 day")))){
			unlink($file);
		}
	}
}
// 캐시 디렉토리 안의 1달 동안 접근하지 않은 파일 청소.

// \r, \n, \t, 주석 등의 필요없는 것을 없애 용량을 줄여 최소한의 태그만 인클루드한다.
function include_once_min($buffer){
	if ($GLOBALS['ouavatar_html_minify'] == 1 || $GLOBALS['ouavatar_html_minify'] == true ){
		$buffer = preg_replace('/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\)\/\/.*))/','',$buffer); 
		$buffer = str_replace(array("\r","\t","\n")," ",$buffer);
		$buffer = preg_replace('/<!--[^\[][^>](.*?)-->/si','',$buffer); 
		$buffer = preg_replace("/\s+/", " ", $buffer);
	}
	return $buffer;
}
//

if(empty($_GET['mn']))
	$_GET['mn'] = '';

// 한 쪽에만 인클루드 시켜야 할 경우 여기로 빼둔다.
if(empty($_GET['oumark_gloss_opacity']))
	$_GET['oumark_gloss_opacity'] = 25; 
if(empty($_GET['glasses_num']))
	$_GET['glasses_num'] = 0; 
?>
<?php header('Cache-Control: max-age=600');   ?>


<?php

ob_start("include_once_min"); // 개행, 주석, 공백 등을 제거하여 용량 축소
?>
<!DOCTYPE html>
<html>
	<head><?php 		
		// head 부분 include
		include_once $server_path."ouavatar_menu_head.php";
	?>
	</head>
	<body onerror="document.getElementById('js_errordiv').style.display='';">
		<?php 			
		// body 부분 include
		include_once $server_path."ouavatar_menu_body.php";
		
	?>
	</body>
</html>
<?php
ob_end_flush(); //용량 축소 끝

?>
